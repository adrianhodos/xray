#include <gtest/gtest.h>
#include "Xray/Math/Constants.hpp"
#include "Xray/Math/Vector2.hpp"
#include "tests_config.hpp"

using namespace xray::math;

TEST(Vector2Tests, Basics)
{
    const vector2F v0{2.0f, -3.0f};

    EXPECT_NEAR(13.0f, v0.squared_lenght(), EpsilonLow);
    EXPECT_NEAR(3.6055f, v0.length(), EpsilonLow);
    EXPECT_FALSE(v0.is_unit_length());
    EXPECT_FALSE(v0.is_zero_length());

    const vector2F v1{make_normal_vector(v0)};
    EXPECT_TRUE(v1.is_unit_length());

    const vector2F v2{5.0f, 2.0f};

    EXPECT_NEAR(4.0f, dot_product(v0, v2), EpsilonLow);
    EXPECT_FALSE(perpendicular_vectors(v0, v2));

    const vector2F v3{make_orthogonal_vector_to_vector(v0)};
    const vector2F v4{make_orthogonal_vector_to_vector(v0, rotation_type::kClockwise)};

    EXPECT_EQ(v3, (vector2F{3.0f, 2.0f}));
    EXPECT_TRUE(perpendicular_vectors(v0, v3));
    EXPECT_TRUE(perpendicular_vectors(v0, v4));

    EXPECT_FALSE(collinear_points(v0, v2, v3));
}

TEST(Vector2Tests, RelationalOps)
{
    vector2F v0{4.0f, 1.0f};
    vector2F v1{-v0};

    EXPECT_FALSE(v0 == v1);
    EXPECT_TRUE(v0 != v1);
    EXPECT_TRUE(v0 == -v1);

    v0.negate();
    EXPECT_TRUE(v0 == v1);
}

TEST(Vector2Tests, Misc)
{
    const vector2F p0{1.0f, 3.0f};
    const vector2F p1{4.0f, 7.0f};

    EXPECT_NEAR(25.0f, points_squared_distance(p0, p1), EpsilonLow);
    EXPECT_NEAR(5.0f, points_distance(p0, p1), EpsilonLow);

    const vector2F p2{-2.0f, -1.0f};

    EXPECT_TRUE(collinear_points(p0, p1, p2));
    EXPECT_FALSE(collinear_points(p0, p1, vector2F::Zero));

    const vector2F v0{p1 - p0};
    const vector2F v1{6.0f, 8.0f};

    EXPECT_TRUE(parallel_vectors(v0, v1));
}

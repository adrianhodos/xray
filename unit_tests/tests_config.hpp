#pragma once

#include "xray/xray.hpp"

const float EpsilonLow = 1.0e-3f;
const float EpsilonMid = 1.0e-6f;
const float EpsilonHigh = 1.0e-9f;
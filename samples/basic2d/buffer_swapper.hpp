#pragma once

#include <windows.h>
#include <dxgi.h>

#include "xray/xray.hpp"
#include "xray/base/windows/com_pointer.hpp"


struct ID3D11Texture2D;
struct ID3D11Device;

class buffer_swapper {
public :
    buffer_swapper() NOEXCEPT;

    ~buffer_swapper();

    void initialize(HWND output_window, ID3D11Device* device) NOEXCEPT;

    explicit operator bool() const NOEXCEPT {
        return swap_chain_ != nullptr;
    }

    xray::base::com_scoped_pointer<ID3D11Texture2D>
    backbuffer_reference() const NOEXCEPT;

    void resize(const UINT new_width, const UINT new_height) NOEXCEPT;

    void swap_buffers() NOEXCEPT;

private :
    HWND                                                        output_window_;
    xray::base::com_scoped_pointer<IDXGISwapChain>              swap_chain_;

private :
    NO_CC_ASSIGN(buffer_swapper);
};

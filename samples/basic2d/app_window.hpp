#pragma once

#include <cstdint>
#include <functional>
#include <windows.h>

#include "xray/xray.hpp"
#include "xray/base/timer.hpp"
#include "xray/base/fast_delegate.hpp"
#include "buffer_swapper.hpp"

struct window_init_params_t;

class application_window {
public :
    typedef xray::base::fast_delegate<void (const float)> update_event_t;
    typedef xray::base::fast_delegate<void (const xr_int32_t, const xr_int32_t)> resize_event_t;
    typedef xray::base::fast_delegate<void (void)> draw_event_t;
    typedef xray::base::fast_delegate<void (ID3D11Texture2D*)>  backbuffer_reset_event_t;

public :
    application_window(const window_init_params_t& init_params);

    void run() NOEXCEPT;

    HWND window_handle() const NOEXCEPT { return window_; }

    explicit operator bool() const NOEXCEPT { return is_valid(); }

public :

    update_event_t              UpdateEvent;
    resize_event_t              ResizeEvent;
    draw_event_t                DrawEvent;
    backbuffer_reset_event_t    BackBufferResetEvent;

private :

    void wmsize_event(
        const xr_int32_t size_request,
        const xr_int32_t width,
        const xr_int32_t height) NOEXCEPT;

    bool is_valid() const NOEXCEPT { return window_ != nullptr && buff_swap_; }

    void initialize(const window_init_params_t& init_params) NOEXCEPT;

    void draw_frame() NOEXCEPT;

    void update_frame(const float) NOEXCEPT;

    static LRESULT CALLBACK window_proc_stub(HWND, UINT, WPARAM, LPARAM);

    LRESULT window_procedure(UINT msg, WPARAM w_param, LPARAM l_param);

private :
    HWND                                            window_;
    HINSTANCE                                       instance_;
    xr_int32_t                                      width_;
    xr_int32_t                                      height_;
    xray::base::high_resolution_timer<float>        timer_;
    buffer_swapper                                  buff_swap_;

private :
    NO_CC_ASSIGN(application_window);
};

struct window_init_params_t {
    ID3D11Device*                                   GraphicsDevice;
    xr_int32_t                                      Width;
    xr_int32_t                                      Height;
    application_window::backbuffer_reset_event_t    Event_BackbufferReset;
    application_window::update_event_t              Event_Update;
    application_window::draw_event_t                Event_Draw;
    application_window::resize_event_t              Event_Resize;
};

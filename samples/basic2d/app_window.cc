#include <cassert>
#include <cstdint>
#include <memory>
#include <string>
#include <tchar.h>
#include <d3d11.h>
#include <Windows.h>

#include "xray/base/shims/pointer/scoped_pointer.hpp"
#include "app_window.hpp"

using namespace xray::base;

const TCHAR* const kWinClassName = _T("__##@@%%%_!!!_OpenGLES_##@@__");

application_window::application_window(const window_init_params_t &init_params)
    :       UpdateEvent{init_params.Event_Update},
            ResizeEvent{init_params.Event_Resize},
            DrawEvent{init_params.Event_Draw},
            BackBufferResetEvent{init_params.Event_BackbufferReset},
            window_{nullptr},
            instance_{nullptr},
            width_{init_params.Width},
            height_{init_params.Height}
{
    initialize(init_params);
}

void
application_window::initialize(
    const window_init_params_t &init_params) NOEXCEPT {
    if (is_valid())
        return;

    instance_ = GetModuleHandle(nullptr);

    WNDCLASSEX wc;
    HINSTANCE instance_handle = GetModuleHandle(0);
    ZeroMemory(&wc, sizeof(wc));

    wc.cbSize = sizeof(wc);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = &application_window::window_proc_stub;
    wc.hInstance = instance_handle;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
    wc.lpszClassName = kWinClassName;

    if (!RegisterClassEx(&wc)) {
        return;
    }

    window_ = CreateWindowEx(
        0L, kWinClassName, _T("OpenGL ES window"),
        WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
        width_, height_, nullptr, nullptr, instance_handle,
        this);
    if (!window_) {
        return;
    }

    buff_swap_.initialize(window_, init_params.GraphicsDevice);

    ShowWindow(window_, SW_SHOWNORMAL);
    UpdateWindow(window_);
}

LRESULT WINAPI 
application_window::window_proc_stub(
    HWND wnd,
    UINT msg,
    WPARAM w_param,
    LPARAM l_param)
{
    if (msg == WM_CREATE) {
        application_window* ptr = (application_window*) ((CREATESTRUCT*) l_param)->lpCreateParams;
        SetWindowLongPtr(wnd, GWLP_USERDATA, (LONG_PTR) ptr);
        return true;
    }

    application_window* wptr = (application_window*) (LONG_PTR) GetWindowLong(wnd, GWLP_USERDATA);
    if (wptr) {
        return wptr->window_procedure(msg, w_param, l_param);
    }

    return DefWindowProc(wnd, msg, w_param, l_param);
}

LRESULT
application_window::window_procedure(
    UINT msg, WPARAM w_param, LPARAM l_param)
{
    switch (msg) {
    case WM_CLOSE :
        DestroyWindow(window_);
        return 0L;
        break;

    case WM_DESTROY :
        PostQuitMessage(0);
        return 0L;
        break;

    case WM_SIZE :
        wmsize_event(w_param, LOWORD(l_param), HIWORD(l_param));
        return 0L;
        break;

    default :
        break;
    }

    return DefWindowProc(window_, msg, w_param, l_param);
}

void
application_window::wmsize_event(
    const int32_t size_request,
    const int32_t width,
    const int32_t height) NOEXCEPT
{
    if (size_request == SIZE_MAXIMIZED || size_request == SIZE_RESTORED) {
        if ((width <= 0) || (height <= 0))
            return;

        if (ResizeEvent)
            ResizeEvent(width, height);

        buff_swap_.resize(static_cast<UINT>(width), static_cast<UINT>(height));

        if (BackBufferResetEvent) {
            auto tex_ref = buff_swap_.backbuffer_reference();
            if (tex_ref)
                BackBufferResetEvent(raw_ptr(tex_ref));
        }
    }
}

void
application_window::draw_frame() NOEXCEPT {
    assert(is_valid());

    if (DrawEvent) {
        DrawEvent();
    }

    buff_swap_.swap_buffers();
}

void
application_window::update_frame(const float delta_tm) NOEXCEPT {
    assert(is_valid());

    if (UpdateEvent)
        UpdateEvent(delta_tm);
}

void
application_window::run() NOEXCEPT {
    if (!is_valid()) {
        return;
    }

    if (BackBufferResetEvent) {
        auto tex_ref = buff_swap_.backbuffer_reference();
        if (tex_ref)
            BackBufferResetEvent(raw_ptr(tex_ref));
    }

    timer_.start();

    MSG message;
    for (bool quit_flag = false; quit_flag != true;) {
        while (PeekMessage(&message, nullptr, 0, 0, PM_NOREMOVE) == FALSE) {
            const float delta_tm = timer_.tick();
            update_frame(delta_tm);
            draw_frame();

            Sleep(50);
        }

        do {
            const int ret_code = GetMessage(&message, nullptr, 0, 0);
            if (ret_code == -1) {
                //
                // error
                quit_flag = true;
                break;
            }

            if (ret_code == 0) {
                //
                // WM_QUIT
                quit_flag = true;
                break;
            }

            TranslateMessage(&message);
            DispatchMessage(&message);
        } while (PeekMessage(&message, nullptr, 0, 0, PM_NOREMOVE) == TRUE);
    }
}

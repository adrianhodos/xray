#include <tchar.h>
#include <random>
#include <Windows.h>

#include "xray/math/color.hpp"
#include "xray/rendering/directx/renderer_directx.hpp"
#include "app_window.hpp"
#include "basic_geometry.hpp"

using namespace xray::base;
using namespace xray::rendering;

class simple_scene {
public :
    simple_scene(renderer_directx* rdev)
        : renderer_{rdev} {}

    void init();

    void update(const float delta_tm) NOEXCEPT;

    void draw() NOEXCEPT;

private :
    std::mt19937            engine_;
    float                   elapsed_time_ {0.0f};
    renderer_directx*       renderer_;
    basic_geometry          simple_mesh_;
};

void simple_scene::init() {
    assert(renderer_);

    simple_mesh_.initialize(*renderer_);
}

void simple_scene::update(const float delta_tm) NOEXCEPT {
    elapsed_time_ += delta_tm;

    if (elapsed_time_ < 200.0f)
        return;

    elapsed_time_ = 0.0;
    
    std::uniform_real_distribution<float> dist{0.0f, 1.0f};
    
    xray::math::rgb_color new_color{dist(engine_), dist(engine_), dist(engine_), 1.0f};
    renderer_->set_clear_color(new_color);
}

void simple_scene::draw() NOEXCEPT {
    renderer_->clear();
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPTSTR, int) {
    renderer_directx renderer;
    if (!renderer)
        return EXIT_FAILURE;

    simple_scene scene{&renderer};

    window_init_params_t window_params{
        renderer.device(), 1024, 1024, 
        make_delegate(renderer, &renderer_directx::restore_after_resize),
        make_delegate(scene, &simple_scene::update),
        make_delegate(scene, &simple_scene::draw),
        make_delegate(renderer, &renderer_directx::handle_resize)
    };

    application_window app_wnd{window_params};
    if (!app_wnd)
        return EXIT_FAILURE;

    scene.init();
    app_wnd.run();

    //app_wnd.BackBufferResetEvent = xray::base::make_delegate(renderer, &renderer_directx::restore_after_resize);
    //app_wnd.ResizeEvent = xray::base::make_delegate(renderer, &renderer_directx::handle_resize);

    return 0;
}
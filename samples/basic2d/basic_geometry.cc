#include <DirectXMath.h>

#include "xray/base/array_dimension.hpp"
#include "xray/rendering/directx/resource_usage.hpp"
#include "xray/rendering/directx/renderer_directx.hpp"
#include "xray/rendering/directx/gpu_buffer.hpp"
#include "xray/rendering/directx/shader_uniform_block.hpp"
#include "xray/rendering/directx/shader_uniform.hpp"
#include "xray/rendering/directx/shader_common_core.hpp"
#include "basic_geometry.hpp"

using namespace xray::rendering;

struct vertex_pc {
    DirectX::XMFLOAT3   position;
    DirectX::XMFLOAT4   color;
};

struct vertex_pnt {
    DirectX::XMFLOAT3   position;
    DirectX::XMFLOAT3   normal;
    DirectX::XMFLOAT4   color;
};

basic_geometry::basic_geometry(const renderer_directx& renderer)
{
    initialize(renderer);
}

basic_geometry::~basic_geometry() {}

void basic_geometry::initialize(const renderer_directx& renderer) {
    const vertex_pc vertices[] {
        {{0.0f, 0.0f, 1.0f}, {1.0f, 0.0f, 0.0f, 1.0f}},
        {{10.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 0.0f, 1.0f}},
        {{0.0f, 10.0f, 1.0f}, {0.0f, 0.0f, 1.0f, 1.0f}}
    };

    //vertex_buffer_ = renderer.make_vertex_buffer(vertices, resource_usage::immutable);
    vertex_buffer_.initialize(renderer, xr_array_size__(vertices), sizeof(vertices[0]), resource_usage::immutable, vertices);

    const xr_uint16_t indices[] { 0, 1, 2 };

    //index_buffer_ = renderer.make_index_buffer(indices, resource_usage::immutable);
    index_buffer_.initialize(renderer, xr_array_size__(indices), sizeof(indices[0]), resource_usage::immutable, indices);

    constant_buffer cb{renderer, 1, 32, resource_usage::cpu_writable};

    const vertex_pnt test_vertices[] = {{{1.0f, 1.0f, 1.0f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 1.0f, 1.0f}}};
    vertex_buffer vb2;
    vb2.initialize(renderer, test_vertices, resource_usage::cpu_writable);

    const xr_uint32_t indices2[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    index_buffer ib2{renderer, indices2, resource_usage::immutable};

    if (!cb)
        OutputDebugString("Muie, sloboz!");

    dx_detail::shader_uniform_block ubt;
    dx_detail::shader_uniform unf{{}, &ubt};
    dx_detail::shader_common_core scb;
}
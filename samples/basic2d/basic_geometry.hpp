#pragma once

#include "xray/xray.hpp"
#include "xray/base/windows/com_pointer.hpp"
#include "xray/rendering/directx/gpu_buffer.hpp"

namespace xray { namespace rendering {
    class renderer_directx;
}
}

struct ID3D11Buffer;

class basic_geometry {
public :
    basic_geometry() {}

    explicit basic_geometry(const xray::rendering::renderer_directx& renderer);

    ~basic_geometry();

    bool is_valid() const NOEXCEPT {
        return vertex_buffer_ && index_buffer_;
    }

public :
    void initialize(const xray::rendering::renderer_directx& renderer);

private :
    //xray::base::com_scoped_pointer<ID3D11Buffer>    vertex_buffer_;
    //xray::base::com_scoped_pointer<ID3D11Buffer>    index_buffer_;
    xray::rendering::vertex_buffer                      vertex_buffer_;
    xray::rendering::index_buffer                       index_buffer_;

private :
    NO_CC_ASSIGN(basic_geometry);
};

#include <d3d11.h>

#include "buffer_swapper.hpp"
#include "xray/base/shims/pointer/scoped_pointer.hpp"

using namespace xray::base;

buffer_swapper::buffer_swapper() NOEXCEPT
    :       output_window_{nullptr}
{}

buffer_swapper::~buffer_swapper() {}

void
buffer_swapper::initialize(
    HWND output_window,
    ID3D11Device *device) NOEXCEPT
{
    output_window_ = output_window;
    if (!output_window)
        return;

    RECT window_geometry;
    GetWindowRect(output_window_, &window_geometry);

    com_scoped_pointer<IDXGIFactory> dxgi_factory;

    HRESULT ret_code = CreateDXGIFactory(
                __uuidof(IDXGIFactory), (void**) raw_ptr_ptr(dxgi_factory));

    if (FAILED(ret_code))
        return;

    DXGI_SWAP_CHAIN_DESC swap_chain_desc;
    swap_chain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swap_chain_desc.BufferDesc.Height = window_geometry.right - window_geometry.left;
    swap_chain_desc.BufferDesc.Width = window_geometry.bottom - window_geometry.top;
    swap_chain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    swap_chain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swap_chain_desc.BufferDesc.RefreshRate.Numerator = 60;
    swap_chain_desc.BufferDesc.RefreshRate.Denominator = 1;
    swap_chain_desc.BufferCount = 1;
    swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swap_chain_desc.Flags = 0;
    swap_chain_desc.OutputWindow = output_window_;
    swap_chain_desc.SampleDesc.Count = 1;
    swap_chain_desc.SampleDesc.Quality = 0;
    swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    swap_chain_desc.Windowed = true;

    ret_code = dxgi_factory->CreateSwapChain(device,
                                             &swap_chain_desc,
                                             raw_ptr_ptr(swap_chain_));
}

xray::base::com_scoped_pointer<ID3D11Texture2D>
buffer_swapper::backbuffer_reference() const NOEXCEPT
{
    com_scoped_pointer<ID3D11Texture2D> backbuffer_ref;

    if (!swap_chain_)
        return backbuffer_ref;

    swap_chain_->GetBuffer(
        0, __uuidof(ID3D11Texture2D), (void**) raw_ptr_ptr(backbuffer_ref));

    return backbuffer_ref;
}

void
buffer_swapper::resize(const UINT new_width, const UINT new_height) NOEXCEPT {
    if (!swap_chain_)
        return;

    swap_chain_->ResizeBuffers(1, new_width, new_height, DXGI_FORMAT_UNKNOWN, 0);
}

void
buffer_swapper::swap_buffers() NOEXCEPT
{
    if (!swap_chain_)
        return;

    swap_chain_->Present(0, 0);
}

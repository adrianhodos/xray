//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <Skia/SkCanvas.h>

#include "Xray/Xray.hpp"
#include "Xray/Drawing/IDrawingBackend.hpp"

namespace Xray {
namespace Drawing {

    class SkiaDrawingBackend : public IDrawingBackend {
    public:
        typedef IDrawingBackend base_class;

    public:
        SkiaDrawingBackend(const DrawingBackendParams_t& initParams);

        bool CanDraw2D() const NOEXCEPT
        {
            return true;
        }

        bool CanDraw3D() const NOEXCEPT
        {
            return false;
        }

        const void* CanvasPixels() const NOEXCEPT;

        void SaveCanvasToFile(const char* fileName);

    public:
        void BeginDraw();

        void EndDraw();
        
        void
        DrawPoint(
            Math::xrVector2F const& pointToDraw,
            Math::xrRGBColor const& drawColor,
            float const pointSize = 6.0f) NOEXCEPT;

        void
        DrawArrow(
            Math::xrVector2F const& arrowOrigin,
            Math::xrVector2F const& arrowDirection,
            Math::xrRGBColor const& drawColor,
            float const arrowLength = 4.0f) NOEXCEPT;

        void
        DrawLine(
            Math::xrLine2F const& lineToDraw,
            Math::xrRGBColor const& drawColor,
            float const lineWidth = 4.0f) NOEXCEPT;

        void
        DrawLine(
            Math::xrVector2F const& lineStart,
            Math::xrVector2F const& lineEnd,
            Math::xrRGBColor const& drawColor,
            float const lineWidth) NOEXCEPT;

        void
        DrawPolygon(
            const Xray::Base::ArrayProxy<const Math::xrVector2F>& polyVertices,
            const Xray::Math::xrRGBColor& fillColor) NOEXCEPT;

        void
        DrawTriangle(
            const Math::xrTriangle2F& triangleToDraw,
            const Math::xrRGBColor& fillColor,
            const Math::xrRGBColor& sideNormalsColor) NOEXCEPT;

        void
        DrawCircle(
            const Math::xrCircle2F& circleToDraw,
            const Math::xrRGBColor& fillColor) NOEXCEPT;

        void
        DrawArc(
            const Math::xrArc2F& arcToDraw,
            const Math::xrRGBColor& fillColor) NOEXCEPT;

    private:
        SkBitmap mCanvasBackingStore;
        SkCanvas mDrawingCanvas;
        SkPaint mDrawingPaint;

    private:
        NO_CC_ASSIGN(SkiaDrawingBackend);
    };

} // namespace Drawing
} // namespace Xray

//
// Copyright (c) 2011, 2012, Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "xray/xray.hpp"
#include "xray/base/array_proxy.hpp"
#include "xray/math/color.hpp"
#include "xray/math/objects/arc2.hpp"
#include "xray/math/objects/circle2.hpp"
#include "xray/math/objects/line2.hpp"
#include "xray/math/objects/triangle2.hpp"
#include "xray/math/vector2.hpp"

namespace xray {
namespace drawing {

    struct drawing_backend_params_t {
        xr_uint32_t CanvasWidth;
        xr_uint32_t CanvasHeight;
        xr_uint32_t BitsPerPixel;
    };

    class IDrawingBackend {
    public:
        virtual ~IDrawingBackend();

        virtual bool CanDraw2D() const NOEXCEPT = 0;

        virtual bool CanDraw3D() const NOEXCEPT = 0;

        virtual const void* CanvasPixels() const NOEXCEPT = 0;

        virtual void SaveCanvasToFile(const char* fileName) = 0;

    public:
        virtual void BeginDraw() = 0;

        virtual void EndDraw() = 0;

        virtual void
        DrawPoint(
            math::vector2F const& pointToDraw,
            math::rgb_color const& drawColor,
            float const pointSize = 6.0f) NOEXCEPT = 0;

        virtual void
        DrawArrow(
            math::vector2F const& arrowOrigin,
            math::vector2F const& arrowDirection,
            math::rgb_color const& drawColor,
            float const arrowLength = 4.0f) NOEXCEPT = 0;

        virtual void
        DrawLine(
            math::line2F const& lineToDraw,
            math::rgb_color const& drawColor,
            float const lineWidth = 4.0f) NOEXCEPT = 0;

        virtual void
        DrawLine(
            math::vector2F const& lineStart,
            math::vector2F const& lineEnd,
            math::rgb_color const& drawColor,
            float const lineWidth) NOEXCEPT = 0;

        virtual void
        DrawPolygon(
            const base::array_proxy<const math::vector2F>& polyVertices,
            const math::rgb_color& fillColor) NOEXCEPT = 0;

        virtual void
        DrawTriangle(
            const math::triangle2F& triangleToDraw,
            const math::rgb_color& fillColor,
            const math::rgb_color& sideNormalsColor) NOEXCEPT = 0;

        virtual void
        DrawCircle(
            const math::circle2F& circleToDraw,
            const math::rgb_color& fillColor) NOEXCEPT = 0;

        virtual void
        DrawArc(
            const math::arc2F& arcToDraw,
            const math::rgb_color& fillColor) NOEXCEPT = 0;
    };

} // namespace drawing
} // namespace xray

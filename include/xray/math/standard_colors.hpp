//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

///
/// \file   standard_colors.hpp

#pragma once

#include "xray/xray.hpp"

namespace xray {
namespace math {

    /// \addtogroup __GroupXrayMath_Utility
    /// @{

    /// Standard web color values.
    enum class standard_colors : xr_uint32_t {
        kAliceBlue = 0xf0f8ffff,
        kAntiqueWhite = 0xfaebd7ff,
        kAqua = 0x00ffffff,
        kAquamarine = 0x7fffd4ff,
        kAzure = 0xf0ffffff,
        kBeige = 0xf5f5dcff,
        kBisque = 0xffe4c4ff,
        kBlack = 0x000000ff,
        kBlanchedAlmond = 0xffebcdff,
        kBlue = 0x0000ffff,
        kBlueViolet = 0x8a2be2ff,
        kBrown = 0xa52a2aff,
        kBurlyWood = 0xdeb887ff,
        kCadetBlue = 0x5f9ea0ff,
        kChartreuse = 0x7fff00ff,
        kChocolate = 0xd2691eff,
        kCoral = 0xff7f50ff,
        kCornflowerBlue = 0x6495edff,
        kCornsilk = 0xfff8dcff,
        kCrimson = 0xdc143cff,
        kDarkBlue = 0x00008bff,
        kDarkCyan = 0x008b8bff,
        kDarkGoldenrod = 0xb8860bff,
        kDarkGray = 0xa9a9a9ff,
        kDarkGreen = 0x006400ff,
        kDarkKhaki = 0xbdb76bff,
        kDarkMagenta = 0x8b008bff,
        kDarkOliveGreen = 0x556b2fff,
        kDarkOrange = 0xff8c00ff,
        kDarkOrchid = 0x9932ccff,
        kDarkRed = 0x8b0000ff,
        kDarkSalmon = 0xe9967aff,
        kDarkSeaGreen = 0x8fbc8fff,
        kDarkSlateBlue = 0x483d8bff,
        kDarkSlateGray = 0x2f4f4fff,
        kDarkTurquoise = 0x00ced1ff,
        kDarkViolet = 0x9400d3ff,
        kDeepPink = 0xff1493ff,
        kDeepSkyBlue = 0x00bfffff,
        kDimGray = 0x696969ff,
        kDodgerBlue = 0x1e90ffff,
        kFirebrick = 0xb22222ff,
        kFloralWhite = 0xfffaf0ff,
        kForestGreen = 0x228b22ff,
        kFuchsia = 0xff00ffff,
        kGainsboro = 0xdcdcdcff,
        kGhostWhite = 0xf8f8ffff,
        kGold = 0xffd700ff,
        kGoldenrod = 0xdaa520ff,
        kGray = 0x808080ff,
        kGreen = 0x008000ff,
        kGreenYellow = 0xadff2fff,
        kHoneydew = 0xf0fff0ff,
        kHotPink = 0xff69b4ff,
        kIndianRed = 0xcd5c5cff,
        kIndigo = 0x4b0082ff,
        kIvory = 0xfffff0ff,
        kKhaki = 0xf0e68cff,
        kLavender = 0xe6e6faff,
        kLavenderBlush = 0xfff0f5ff,
        kLawnGreen = 0x7cfc00ff,
        kLemonChiffon = 0xfffacdff,
        kLightBlue = 0xadd8e6ff,
        kLightCoral = 0xf08080ff,
        kLightCyan = 0xe0ffffff,
        kLightGoldenrodYellow = 0xfafad2ff,
        kLightGray = 0xd3d3d3ff,
        kLightGreen = 0x90ee90ff,
        kLightPink = 0xffb6c1ff,
        kLightSalmon = 0xffa07aff,
        kLightSeaGreen = 0x20b2aaff,
        kLightSkyBlue = 0x87cefaff,
        kLightSlateGray = 0x778899ff,
        kLightSteelBlue = 0xb0c4deff,
        kLightYellow = 0xffffe0ff,
        kLime = 0x00ff00ff,
        kLimeGreen = 0x32cd32ff,
        kLinen = 0xfaf0e6ff,
        kMaroon = 0x800000ff,
        kMediumAquamarine = 0x66cdaaff,
        kMediumBlue = 0x0000cdff,
        kMediumOrchid = 0xba55d3ff,
        kMediumPurple = 0x9370dbff,
        kMediumSeaGreen = 0x3cb371ff,
        kMediumSlateBlue = 0x7b68eeff,
        kMediumSpringGreen = 0x00fa9aff,
        kMediumTurquoise = 0x48d1ccff,
        kMediumVioletRed = 0xc71585ff,
        kMidnightBlue = 0x191970ff,
        kMintCream = 0xf5fffaff,
        kMistyRose = 0xffe4e1ff,
        kMoccasin = 0xffe4b5ff,
        kNavajoWhite = 0xffdeadff,
        kNavy = 0x000080ff,
        kOldLace = 0xfdf5e6ff,
        kOlive = 0x808000ff,
        kOliveDrab = 0x6b8e23ff,
        kOrange = 0xffa500ff,
        kOrangeRed = 0xff4500ff,
        kOrchid = 0xda70d6ff,
        kPaleGoldenrod = 0xeee8aaff,
        kPaleGreen = 0x98fb98ff,
        kPaleTurquoise = 0xafeeeeff,
        kPaleVioletRed = 0xdb7093ff,
        kPapayaWhip = 0xffefd5ff,
        kPeachPuff = 0xffdab9ff,
        kPeru = 0xcd853fff,
        kPink = 0xffc0cbff,
        kPlum = 0xdda0ddff,
        kPowderBlue = 0xb0e0e6ff,
        kPurple = 0x800080ff,
        kRed = 0xff0000ff,
        kRosyBrown = 0xbc8f8fff,
        kRoyalBlue = 0x4169e1ff,
        kSaddleBrown = 0x8b4513ff,
        kSalmon = 0xfa8072ff,
        kSandyBrown = 0xf4a460ff,
        kSeaGreen = 0x2e8b57ff,
        kSeaShell = 0xfff5eeff,
        kSienna = 0xa0522dff,
        kSilver = 0xc0c0c0ff,
        kSkyBlue = 0x87ceebff,
        kSlateBlue = 0x6a5acdff,
        kSlateGray = 0x708090ff,
        kSnow = 0xfffafaff,
        kSpringGreen = 0x00ff7fff,
        kSteelBlue = 0x4682b4ff,
        kTan = 0xd2b48cff,
        kTeal = 0x008080ff,
        kThistle = 0xd8bfd8ff,
        kTomato = 0xff6347ff,
        kTurquoise = 0x40e0d0ff,
        kViolet = 0xee82eeff,
        kWheat = 0xf5deb3ff,
        kWhite = 0xffffffff,
        kWhiteSmoke = 0xf5f5f5ff,
        kYellow = 0xffff00ff
    };

    /// @}

} // namespace math
} // namespace xray

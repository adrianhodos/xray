//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <cassert>
#include <cmath>

#include "xray/xray.hpp"
#include "xray/math/utility.hpp"

namespace xray {
namespace math {

    /// \addtogroup __GroupXrayMath_Geometry
    /// @{

    ///
    /// A two component vector.
    template <typename RealType>
    class vector2 {

        /// \name   Defined types.
        /// @{

    public:
        enum {
            is_floating_point = details::is_floating_point_type<RealType>::Yes
        };

        typedef RealType element_type;
        typedef RealType& reference;
        typedef const RealType& const_reference;
        typedef vector2<RealType> class_type;

        /// @}

        /// \name   Static members and class constants.
        /// @{

    public:
        static const class_type Zero; ///< The Zero vector (0, 0)

        static const class_type UnitX; ///< The unit vector along the X axis (1, 0)

        static const class_type UnitY; ///< The unit vector along the Y axis (0, 1)

        /// @}

        /// \name Data members.
        /// @{

    public:
        union {

            struct {
                RealType X;
                RealType Y;
            };

            RealType Elements[2]; ///< Used for array like access to elements.
        };

        /// @}

        /// \name   Construction/init.
        /// @{

    public:
        vector2() NOEXCEPT
        {
        }

        vector2(RealType const x, RealType const y) NOEXCEPT
            : X{x},
              Y{y}
        {
        }

        vector2(RealType const (&arrayRef)[2]) NOEXCEPT
            : X{arrayRef[0]},
              Y{arrayRef[1]}
        {
        }

        template <typename ConvertibleType>
        vector2(vector2<ConvertibleType> const& other) NOEXCEPT
            : X{other.X},
              Y{other.Y}
        {
        }

        ///
        /// Constructs a vector from polar coordinates.
        static class_type
        MakeFromPolarCoordinates(
            RealType const& angleToXAxis,
            RealType const& radius) NOEXCEPT
        {
            const RealType sinAngle = sin(angleToXAxis);
            const RealType cosAngle = cos(angleToXAxis);

            return {radius * cosAngle, radius * sinAngle};
        }

        /// @}

        /// \name   Assignment operators.
        /// @{

    public:
        template <typename ConvertibleType>
        class_type&
        operator=(vector2<ConvertibleType> const& other) NOEXCEPT
        {
            X = other.X;
            Y = other.Y;
            return *this;
        }

        template <typename ConvertibleType>
        class_type&
        operator+=(vector2<ConvertibleType> const& rhs) NOEXCEPT
        {
            X += rhs.X;
            Y += rhs.Y;
            return *this;
        }

        template <typename ConvertibleType>
        class_type&
        operator-=(vector2<ConvertibleType> const& rhs) NOEXCEPT
        {
            X -= rhs.X;
            Y -= rhs.Y;
            return *this;
        }

        template <typename ConvertibleType>
        class_type&
        operator*=(ConvertibleType const scalar) NOEXCEPT
        {
            X *= scalar;
            Y *= scalar;
            return *this;
        }

        template <typename ConvertibleType>
        class_type&
        operator/=(ConvertibleType const scalar) NOEXCEPT
        {
            X /= scalar;
            Y /= scalar;
            return *this;
        }

        /// @}

        /// \name   State.
        /// @{

    public:
        /// Returns the sum of the square of the components.
        RealType
        squared_lenght() const NOEXCEPT
        {
            return X * X + Y * Y;
        }

        /// Returns the length of the vector.
        RealType
        length() const NOEXCEPT
        {
            return std::sqrt(squared_lenght());
        }

        /// Makes the vector unit length (v = v / ||v||).
        class_type&
        normalize() NOEXCEPT
        {
            const RealType vector_length{length()};

            if (math::is_zero(vector_length)) {
                X = Y = RealType{0};
            } else {
                *this /= vector_length;
            }
            return *this;
        }

        /// Test if vector is unit length.
        xr_bool_t
        is_unit_length() const NOEXCEPT
        {
            return compare_eq(RealType{1}, squared_lenght());
        }

        /// Test if the vector is null.
        xr_bool_t
        is_zero_length() const NOEXCEPT
        {
            return is_zero(squared_lenght());
        }

        /// Set the vector to the null vector.
        class_type&
        make_zero() NOEXCEPT
        {
            X = Y = RealType{0};
            return *this;
        }

        /// Negates the vector.
        class_type&
        negate() NOEXCEPT
        {
            X = -X;
            Y = -Y;
            return *this;
        }

        /// @}
    };

    typedef vector2<float> vector2F;

    typedef vector2<double> vector2D;

    template <typename RealType>
    const vector2<RealType> vector2<RealType>::Zero(RealType(0), RealType(0));

    template <typename RealType>
    const vector2<RealType> vector2<RealType>::UnitX(RealType(1), RealType(0));

    template <typename RealType>
    const vector2<RealType> vector2<RealType>::UnitY(RealType(0), RealType(1));

    template <typename RealType>
    inline bool
    operator==(
        vector2<RealType> const& lhs,
        vector2<RealType> const& rhs) NOEXCEPT
    {
        return compare_eq(lhs.X, rhs.X) && compare_eq(lhs.Y, rhs.Y);
    }

    template <typename RealType>
    inline bool
    operator!=(
        vector2<RealType> const& lhs,
        vector2<RealType> const& rhs) NOEXCEPT
    {
        return !(lhs == rhs);
    }

    template <typename RealType>
    inline vector2<RealType>
    operator+(
        vector2<RealType> const& lhs,
        vector2<RealType> const& rhs) NOEXCEPT
    {
        math::vector2<RealType> res{lhs};
        res += rhs;
        return res;
    }

    template <typename RealType>
    inline vector2<RealType>
    operator-(
        vector2<RealType> const& lhs,
        vector2<RealType> const& rhs) NOEXCEPT
    {
        vector2<RealType> res{lhs};
        res -= rhs;
        return res;
    }

    template <typename RealType>
    inline vector2<RealType>
    operator-(
        vector2<RealType> const& vec) NOEXCEPT
    {
        vector2<RealType> newVec{vec.X, vec.Y};
        return newVec.negate();
    }

    template <typename RealType, typename ConvertibleType>
    inline vector2<RealType>
    operator*(
        vector2<RealType> const& vec,
        ConvertibleType const scalar)NOEXCEPT
    {
        vector2<RealType> result{vec};
        result *= scalar;
        return result;
    }

    template <typename RealType, typename ConvertibleType>
    inline vector2<RealType>
    operator*(
        ConvertibleType const scalar,
        vector2<RealType> const& vec)NOEXCEPT
    {
        return vec * scalar;
    }

    template <typename RealType, typename ConvertibleType>
    inline vector2<RealType>
    operator/(
        vector2<RealType> const& vec,
        ConvertibleType const scalar) NOEXCEPT
    {
        vector2<RealType> result{vec};
        result /= scalar;

        return result;
    }

    /// Returns the dot product of two vectors.
    template <typename RealType>
    inline RealType
    dot_product(
        vector2<RealType> const& lhs,
        vector2<RealType> const& rhs) NOEXCEPT
    {
        return lhs.X * rhs.X + lhs.Y * rhs.Y;
    }

    /// Tests if two vectors are orthogonal (v1 * v2 = 0).
    template <typename RealType>
    inline bool
    perpendicular_vectors(
        vector2<RealType> const& v0,
        vector2<RealType> const& v1) NOEXCEPT
    {
        return is_zero(dot_product(v0, v1));
    }

    /// Returns the angle of two vectors.
    template <typename RealType>
    inline RealType
    vectors_angle(
        vector2<RealType> const& v0,
        vector2<RealType> const& v1) NOEXCEPT
    {
        return std::acos(dot_product(v0, v1) / (v0.Length() * v1.Length()));
    }

    ///
    /// Returns the angle of two unit length vectors.
    template <typename RealType>
    inline RealType
    unit_vectors_angle(
        vector2<RealType> const& v0,
        vector2<RealType> const& v1) NOEXCEPT
    {
        assert(v0.is_unit_length());
        assert(v1.is_unit_length());

        return std::acos(dot_product(v0, v1));
    }

    /// Returns the projection of a vector on another vector.
    /// Let P, Q be two vectors. The projection of P on Q
    ///     proj (Q, P) = [(P * Q) / (|| Q || ^2)] * Q.
    template <typename RealType>
    inline vector2<RealType>
    project_vector_on_vector(
        vector2<RealType> const& v0,
        vector2<RealType> const& v1) NOEXCEPT
    {
        return (dot_product(v0, v1) / v1.SumSquaredElements()) * v1;
    }

    /// Returns the normal of the input vector.
    template <typename RealType>
    inline vector2<RealType>
    make_normal_vector(
        vector2<RealType> const& vec) NOEXCEPT
    {
        vector2<RealType> res{vec};
        res.normalize();
        return res;
    }

    enum class rotation_type {
        kClockwise,
        kCounterClockwise
    };

    /// Returns a vector that is orthogonal to the input vector.
    /// \param[in]  vec                 The input vector.
    /// \param[in]  counter_clockwise   (optional) True if the vector is computed using
    /// a counter clockwise rotation, false if using a clockwise rotation.
    template <typename RealType>
    inline vector2<RealType>
    make_orthogonal_vector_to_vector(
        const vector2<RealType>& vec,
        const rotation_type rot_type = rotation_type::kCounterClockwise) NOEXCEPT
    {
        vector2<RealType> perpVector;
        if (rot_type == rotation_type::kCounterClockwise) {
            perpVector.X = -vec.Y;
            perpVector.Y = vec.X;
        } else {
            perpVector.X = vec.Y;
            perpVector.Y = -vec.X;
        }
        return perpVector;
    }

    /// Returns the square of the distance between two points.
    template <typename RealType>
    inline RealType
    points_squared_distance(
        vector2<RealType> const& point1,
        vector2<RealType> const& point2) NOEXCEPT
    {
        RealType px = point2.X - point1.X;
        RealType py = point2.Y - point1.Y;
        return px * px + py * py;
    }

    /// Returns the distance between two points.
    template <typename RealType>
    inline RealType
    points_distance(
        vector2<RealType> const& point1,
        vector2<RealType> const& point2) NOEXCEPT
    {
        return sqrt(points_squared_distance(point1, point2));
    }

    /// Test if two vectors are parallel.
    template <typename RealType>
    inline bool
    parallel_vectors(
        vector2<RealType> const& v0,
        vector2<RealType> const& v1) NOEXCEPT
    {
        return is_zero(v0.X * v1.Y - v0.Y * v1.X);
    }

    /// Returns the signed area of the triangle spanned by three points.
    template <typename RealType>
    inline RealType
    triangle_signed_area(
        vector2<RealType> const& p0,
        vector2<RealType> const& p1,
        vector2<RealType> const& p2) NOEXCEPT
    {
        const RealType triangleArea = p0.X * (p1.Y - p2.Y)
                                      + p1.X * (p2.Y - p0.Y)
                                      + p2.X * (p0.Y - p1.Y);
        return triangleArea / RealType{2};
    }

    /// Test if three points are collinear. Three points are collinear
    /// if the area of the triangle spanned by the points is zero.
    template <typename RealType>
    inline bool
    collinear_points(
        vector2<RealType> const& p0,
        vector2<RealType> const& p1,
        vector2<RealType> const& p2) NOEXCEPT
    {
        return is_zero(triangle_signed_area(p0, p1, p2));
    }

    /// Test if the points spanning a triangle are ordered counter clockwise.
    template <typename RealType>
    inline bool
    ccw_winded_triangle(
        vector2<RealType> const& p0,
        vector2<RealType> const& p1,
        vector2<RealType> const& p2) NOEXCEPT
    {
        return triangle_signed_area(p0, p1, p2) > RealType{0};
    }

    /// @}

} // namespace math
} // namespace xray

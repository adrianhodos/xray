//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

///
/// \file    radians.hpp

#include <cassert>

#include "xray/xray.hpp"
#include "xray/math/utility.hpp"

namespace xray {
namespace math {

    /// \addtogroup __GroupXrayMath_Geometry
    /// @{

    /// A class for expressing the measure of an angle in radians.
    template <typename RealType>
    class radians {
        /// \name   Defined types.
        /// @{

    public:
        typedef radians<RealType> class_type;

        /// @}

        /// \name   Constructors.
        /// @{

    public:
        static class_type make_from_value(const RealType rad_val) NOEXCEPT
        {
            return radians{rad_val};
        }

        static radians make_from_degrees(const RealType deg_val) NOEXCEPT
        {
            return radians{deg_val * numerics<RealType>::PIOver180()};
        }

    private:
        radians(const RealType radians_val) NOEXCEPT
            : radians_value_{radians_val}
        {
        }

        /// @}

        /// \name   Attributes.
        /// @{

    public:
        RealType to_degrees() const NOEXCEPT
        {
            return math::to_degrees(radians_value_);
        }

        RealType value() const NOEXCEPT
        {
            return radians_value_;
        }

        /// @}

        /// \name   Data members.
        /// @{

    private:
        RealType radians_value_;

        /// @}
    };

    typedef radians<float> radiansF;
    typedef radians<double> radiansD;

    /// @}

} // namespace xray
} // namespace math

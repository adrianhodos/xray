//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

///
/// \file   segment2.hpp

#include "xray/xray.hpp"
#include "xray/math/vector2.hpp"

namespace xray {
namespace math {

    /// \addtogroup __GroupXrayMath_Geometry
    /// @{

    /// \brief  Abstracts a line segment. The segment is stored using a
    ///			centre-direction-extent representation. Given a segment S with
    ///			the centre at point P, direction vector D and extent e, any point
    ///			on the segment verifies the inequality :
    ///			P + (-e) * D <= Q <= P + e * D.
    template <typename RealType>
    class segment2 {

        /// \name   Defined types.
        /// @{

    public:
        typedef vector2<RealType> point_type;
        typedef vector2<RealType> vector_type;

        /// @}

        /// \name   Data members.
        /// @{

    public:
        point_type Center; ///< The centre point.
        vector_type Direction; ///< The direction vector (unit-length).
        RealType Extent; ///< The extent of the segment2.

        /// @}

        /// \name   Constructors.
        /// @{

    public:
        segment2() NOEXCEPT
        {
        }

        segment2(
            const point_type& segmentCenter,
            const vector_type& direction,
            const RealType extent) NOEXCEPT
            : Center{segmentCenter},
              Direction{direction},
              Extent{extent}
        {
        }

        /// \brief Construct a segment, given the two end_points.
        /// \param end_point0 First end_point.
        /// \param end_point1 Second end_point.
        /// \remarks Let E0 and E1 be the end_points. The segment S with center C,
        ///          direction D and extent e is derived with the following :
        ///          C = (E0 + E1) / 2
        ///          D = (E1 - E0) / ||E1 - E0||
        ///          e = (E1 - E0).length() / 2
        segment2(
            const point_type& end_point0,
            const point_type& end_point1) NOEXCEPT
        {
            Center = (end_point0 + end_point1) / RealType{2};
            const vector_type directionVector = (end_point1 - end_point0);
            Direction = GetNormalOfVector(directionVector);
            Extent = directionVector.length() / RealType{2};
        }

        /// @}

        /// \name   Attributes.
        /// @{

    public:
        /// Returns the start point (head) of the segment
        /// (the point P = C + (-e) * D).
        point_type start_point() const NOEXCEPT
        {
            return Center + (-Extent) * Direction;
        }

        /// Returns the end point (tail) of the segment
        /// (the point P = C + e * D).
        point_type end_point() const NOEXCEPT
        {
            return Center + Extent * Direction;
        }

        /// Returns the length of the segment.
        RealType length() const NOEXCEPT
        {
            return Extent * RealType{2};
        }

        /// @}
    };

    typedef segment2<float> segment2F;

    typedef segment2<double> segment2D;

    /// @}

} // namespace Math
} // namespace Xray

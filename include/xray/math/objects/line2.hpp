//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

///
/// \file   line2.hpp   line2 class definition.

#pragma once

#include "xray/xray.hpp"
#include "xray/math/vector2.hpp"
#include "xray/math/utility.hpp"

namespace xray {
namespace math {

    /// \addtogroup __GroupXrayMath_Geometry
    /// @{

    ///
    /// \brief  A line in 2D, stored in parametric form.
    ///         Given a line with origin point P and direction vector D,
    ///         any point on the line verifies the equation Q = P + t * D,
    ///         where t is a real number in the (-inf, +inf) range.
    template <typename RealType>
    class line2 {
    public:
        typedef vector2<RealType> vector_type;
        typedef vector2<RealType> point_type;
        typedef line2<RealType> class_type;

    public:
        ///<    Line origin point.
        point_type Origin;

        ///<    Line direction vector.
        vector_type Direction;

    public:
        ///
        /// \brief  Construct a line from an origin point and a direction vector.
        line2(
            RealType const originX,
            RealType const originY,
            RealType const directionX,
            RealType const directionY) NOEXCEPT
            : Origin{originX, originY},
              Direction{directionX, directionY}
        {
        }

        ///
        /// \brief  Construct a line from an origin point and a direction vector.
        line2(
            point_type const& originPoint,
            vector_type const& directionVector) NOEXCEPT
            : line2{originPoint.X, originPoint.Y, directionVector.X, directionVector.Y}
        {
        }

        ///
        /// \brief  Construct a line from a normal vector and origin point.
        static class_type
        make_from_normal_and_origin(
            RealType const originX,
            RealType const originY,
            RealType const normalX,
            RealType const normalY) NOEXCEPT
        {
            return {originX, originY, normalY, -normalX};
        }

        ///
        /// \brief  Construct a line passing through two points.
        static class_type
        make_from_points(
            RealType const p0x,
            RealType const p0y,
            RealType const p1x,
            RealType const p1y) NOEXCEPT
        {
            return {p0x, p0y, p1x - p0x, p1y - p0y};
        }

        ///
        /// \brief  Returns the normal vector to the line (the vector perpendicular
        ///         to the line's direction vector).
        vector_type
        normal() const NOEXCEPT
        {
            return {-Direction.Y, Direction.X};
        }

        ///
        /// \brief  Get the point on the line corresponding to the passed parameter
        ///         value.
        point_type
        evaluate_param(RealType const paramValue) const NOEXCEPT
        {
            return Origin + paramValue * Direction;
        }

        ///
        /// \brief  Test if a point is on the line. A point is on the line
        ///         if the dot product between the vector from the line's origin
        ///         to that point is perpendicular to the line's normal.
        bool
        contains_point(
            RealType const pointX,
            RealType const pointY) const NOEXCEPT
        {
            return is_zero(dot_product({pointX - Origin.X, pointY - Origin.y},
                                       normal()));
        }

        ///
        /// \brief  Test if a point is on the line.
        bool
        contains_point(
            point_type const& point) const NOEXCEPT
        {
            return contains_point(point.X, point.Y);
        }
    };

    typedef line2<float> line2F;

    typedef line2<double> line2D;

    ///
    /// \brief  Let L be a line with origin at point O and direction vector V and
    ///         P be a point. Let N(-V.y, Vx) be the normal vector to the line.
    ///         Then the signed distance from the point P to the line L is
    ///         given by the formula :
    ///             <b> D(P, L) = [(N . P) - (N . O)] / || N || </b> .
    template <typename RealType>
    inline RealType
    signed_distance_point_line(
        RealType const pointX,
        RealType const pointY,
        line2<RealType> const& line) NOEXCEPT
    {
        const vector2<RealType> lineNormal{line.normal()};
        const RealType signedDist = dot_product(lineNormal, {pointX, pointY})
                                    - dot_product(lineNormal, line.Origin);

        return signedDist / lineNormal.Length();
    }

    ///
    /// \brief  Returns the signed distance from a point to a line.
    template <typename RealType>
    inline RealType
    signed_distance_point_line(
        vector2<RealType> const& point,
        line2<RealType> const& line) NOEXCEPT
    {
        return signed_distance_point_line(point.X, point.Y, line);
    }

    ///
    /// \brief  Let L be a line and P a point. Let N be the normal to the line L.
    ///         Let <b>s</b> be the signed distance from the point P to the line L.
    ///         Then the projection of point P onto the line L is given by the formula :
    ///             <b> Proj(P, L) = P - s * N </b>.
    template <typename RealType>
    inline vector2<RealType>
    point_line_projection(
        RealType const pointX,
        RealType const pointY,
        line2<RealType> const& line) NOEXCEPT
    {
        const vector2<RealType> lineNormal{line.normal()};
        const RealType signedDistToLine = signed_distance_point_line(pointX, pointY, line);

        return {pointX - signedDistToLine * lineNormal.X,
                pointY - signedDistToLine * lineNormal.Y};
    }

    ///
    /// \brief  Returns the projection of a point on a line.
    template <typename RealType>
    inline vector2<RealType>
    point_line_projection(
        vector2<RealType> const& point,
        line2<RealType> const& line) NOEXCEPT
    {
        return point_line_projection(point.X, point.Y, line);
    }

    /// @}

} // namespace math
} // namespace xray

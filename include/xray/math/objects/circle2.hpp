//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

///
/// \file    circle2.hpp

#include <cassert>
#include "xray/xray.hpp"
#include "xray/math/utility.hpp"
#include "xray/math/vector2.hpp"

namespace xray {
namespace math {

    /// \addtogroup __GroupXrayMath_Geometry
    /// @{

    /// A circle.
    template <typename RealType>
    class circle2 {

        /// \name   Defined types.
        /// @{

    public:
        typedef vector2<RealType> point_type;
        typedef circle2<RealType> class_type;

        /// @}

        /// \name Data members.
        /// @{

    public:
        point_type Center;
        RealType Radius;

        /// @}

        /// \name   Constructors.
        /// @{

    public:
        circle2() NOEXCEPT
        {
        }

        circle2(
            const point_type& circleCenter,
            const RealType circleRadius) NOEXCEPT
            : Center{circleCenter},
              Radius{circleRadius}
        {
        }

        /// Create a circle passing through three non collinear points.
        static class_type
        make_circle_through_points(
            const point_type& p0,
            const point_type& p1,
            const point_type& p2) NOEXCEPT;

        /// Create a circle tangent to three non collinear points.
        static class_type
        make_circle_tangent_points(
            const point_type& p0,
            const point_type& p1,
            const point_type& p2) NOEXCEPT;

        /// @}

        /// \name Attributes.
        /// @{

        RealType length() const NOEXCEPT
        {
            return numerics<RealType>::TwoPI() * Radius;
        }

        RealType diameter() const NOEXCEPT
        {
            return RealType{2} * Radius;
        }

        RealType area() const NOEXCEPT
        {
            return numerics<RealType>::TwoPI() * Radius * Radius;
        }

        /// @}
    };

    typedef circle2<float> circle2F;
    typedef circle2<double> circle2D;

    /// @}

} // namespace xray
} // namespace math

//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

///
/// \file    arc2.hpp

#include <cassert>
#include <cmath>

#include "xray/xray.hpp"
#include "xray/math/utility.hpp"
#include "xray/math/radians.hpp"
#include "xray/math/vector2.hpp"

namespace xray {
namespace math {

    /// \addtogroup __GroupXrayMath_Geometry
    /// @{

    /// A portion of a circle, spanning an angle of less than 2PI radians.
    template <typename RealType>
    class arc2 {

        /// \name   Defined types.
        /// @{

    public:
        typedef vector2<RealType> point_type;
        typedef vector2<RealType> vector_type;
        typedef radians<RealType> angle_type;
        typedef arc2<RealType> class_type;

        /// @}

        /// \name Data members.
        /// @{

    public:
        point_type Center;
        angle_type StartAngle;
        angle_type EndAngle;
        RealType Radius;

        /// @}

        /// \name   Constructors.
        /// @{

    public:
        arc2() NOEXCEPT
        {
        }

        arc2(
            const point_type& arcCenter,
            const angle_type startAngle,
            const angle_type endAngle,
            const RealType arcRadius) NOEXCEPT
            : Center{arcCenter},
              StartAngle{startAngle},
              EndAngle{endAngle},
              Radius{arcRadius}
        {
        }

        ///
        /// Construct an arc passing through three non collinear points.
        /// The distance from the start point to the middle point must be
        /// equal to the distance from the middle point to the end point.
        arc2(
            const point_type& start_point,
            const point_type& middlePoint,
            const point_type& end_point) NOEXCEPT
        {
            assert(!collinear_points(start_point, middlePoint, end_point));
            assert(compare_eq(points_squared_distance(start_point, middlePoint),
                              points_squared_distance(middlePoint, end_point)));
        }

        /// @}

        /// \name Attributes
        /// @{

    public:
        RealType length() const NOEXCEPT
        {
            return spanned_angle().value()
                   / (numerics<RealType>::TwoPI() * Radius);
        }

        RealType area() const NOEXCEPT
        {
            return (spanned_angle().value() * Radius * Radius) / RealType{2};
        }

        /// Returns the height of the arc subtended by the chord.
        RealType height_subtended_arc() const NOEXCEPT
        {
            return Radius * (RealType{1} - cos(spanned_angle().value() / RealType{2}));
        }

        /// Returns the height of the isosceles triangle spanned
        /// by the arc's center and its end_points.
        RealType height_triangle() const NOEXCEPT
        {
            return Radius * cos(spanned_angle().value() / RealType{2});
        }

        /// Returns the length of the arc's chord.
        RealType segment_length() const NOEXCEPT
        {
            return RealType{2} * sin(spanned_angle().value() / RealType{2});
        }

        /// Returns the area of the isosceles triangle spanned by the
        /// arc's center and its end_points.
        RealType sector_area() const NOEXCEPT
        {
            const RealType halfSpannedAngle = spanned_angle().value() / RealType{2};
            return Radius * Radius * sin(halfSpannedAngle) * cos(halfSpannedAngle);
        }

        /// Returns the area of the surface between the chord and the
        /// subtended arc.
        RealType segment_area() const NOEXCEPT
        {
            return (Radius * Radius * (spanned_angle().value()
                                       - sin(spanned_angle().value())))
                   / RealType{2};
        }

        /// Returns the angle spanned by the arc.
        angle_type spanned_angle() const NOEXCEPT
        {
            return angle_type::MakeFromValue(EndAngle.Value() - StartAngle.Value());
        }

        point_type start_point() const NOEXCEPT
        {
            const RealType dirXVal = cos(StartAngle.Value());
            const RealType dirYVal = sin(StartAngle.Value());

            const vector_type dirVector{
                GetNormalOfVector(vector_type{dirXVal, dirYVal})};

            return Center + Radius * dirVector;
        }

        point_type end_point() const NOEXCEPT
        {
            const RealType dirXVal = cos(EndAngle.Value());
            const RealType dirYVal = sin(EndAngle.Value());

            const vector_type dirVector{
                GetNormalOfVector(vector_type{dirXVal, dirYVal})};

            return Center + Radius * dirVector;
        }

        /// Returns the middle point of the subtended chord.
        point_type chord_middle_point() const NOEXCEPT
        {
            return (start_point() + end_point()) / RealType{2};
        }

        /// Test if arc is counter clockwise oriented.
        bool counter_clockwise() const NOEXCEPT
        {
            return StartAngle.Value() < EndAngle.Value();
        }

        /// @}
    };

    typedef arc2<float> arc2F;

    typedef arc2<double> arc2D;

    /// @}

} // namespace xray
} // namespace math

//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

///
/// \file    triangle2.hpp

#include <cassert>
#include <utility>

#include "xray/xray.hpp"
#include "xray/math/utility.hpp"
#include "xray/math/vector2.hpp"

namespace xray {
namespace math {

    /// \addtogroup __GroupXrayMath_Geometry
    /// @{

    enum class triangle_side {
        kA,
        kB,
        kC
    };

    /// \brief   A triangle, stored by its three vertices.
    template <typename RealType>
    class triangle2 {

        /// \name   Defined types.
        /// @{

    public:
        typedef vector2<RealType> point_type;
        typedef vector2<RealType> vector_type;

        /// @}

        /// \name Data members.
        /// @{

    public:
        union {

            struct {
                point_type V0;
                point_type V1;
                point_type V2;
            };

            point_type Vertices[3]; ///< Convenient array access to the vertices.
        };

        /// @}

        /// \name   Constructors.
        /// @{

    public:
        triangle2() NOEXCEPT
        {
        }

        triangle2(
            const point_type& v0,
            const point_type& v1,
            const point_type& v2) NOEXCEPT
            : V0{v0},
              V1{v1},
              V2{v2}
        {
        }

        /// @}

        /// \name   Attributes.
        /// @{

    public:
        /// Returns the signed area of the triangle. It will be a negative value
        /// if the vertices are winded clockwise. If the area is zero, the triangle
        /// is degenerate (the vertices represent collinear points).
        RealType signed_area() const NOEXCEPT
        {
            return triangle_signed_area(V0, V1, V2);
        }

        /// Test if the vertices are winded counter clockwise.
        bool ccw_winded() const NOEXCEPT
        {
            return ccw_winded_triangle(V0, V1, V2);
        }

        /// Test if the triangle is degenerate (the vertices are collinear points).
        bool degenerate() const NOEXCEPT
        {
            return is_zero(signed_area());
        }

        /// Returns the mediator line's origin and direction, corresponding
        /// to the requested side. The origin is the first tuple element and
        /// the direction vector is the second tuple element. The direction
        /// vector is not normalized.
        void side_mediator(
            const triangle_side triSide,
            std::pair<point_type, vector_type>& mediatorData) const NOEXCEPT;

        /// Returns a point inside the triangle, computed using the barycentric
        /// equation.
        /// \remarks The following constraints must be verified :
        ///     1) c0, c1, c2 must be in the [0, 1] range
        ///     2) c0 + c1 + c2 = 1
        point_type evaluate_barycentric(
            const RealType c0,
            const RealType c1,
            const RealType c2) const NOEXCEPT
        {
            return V0 * c0 + V1 * c1 + V2 * c2;
        }

        /// @}
    };

    typedef triangle2<float> triangle2F;

    typedef triangle2<double> triangle2D;

    template <typename RealType>
    void triangle2<RealType>::side_mediator(
        const triangle_side triSide,
        std::pair<point_type, vector_type>& mediatorData) const NOEXCEPT
    {
        const xr_int32_t vertexIndices[][2] = {
            {1, 0}, {0, 2}, {2, 1}};

        const xr_int32_t idx = static_cast<xr_int32_t>(triSide);

        mediatorData.first = (Vertices[vertexIndices[idx][0]]
                              + Vertices[vertexIndices[idx][1]]) / RealType{2};
        mediatorData.second = make_orthogonal_vector_to_vector(
            Vertices[vertexIndices[idx][1]] - Vertices[vertexIndices[idx][0]]);
    }

    /// @}

} // namespace Xray
} // namespace Math

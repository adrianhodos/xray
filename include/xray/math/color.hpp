//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <cmath>

#include "xray/xray.hpp"
#include "xray/math/standard_colors.hpp"

namespace xray {
namespace math {

    class rgb_color;
    struct hsv_color;
    struct hsl_color;
    struct xyz_color;
    struct lab_color;
    struct hcl_color;

    void
    rgb_to_hsv(
        const rgb_color* rgb,
        hsv_color* hsv) NOEXCEPT;

    void
    hsv_to_rgb(
        const hsv_color* hsv,
        rgb_color* rgb) NOEXCEPT;

    void
    rgb_to_hsl(
        const rgb_color& rgb, hsl_color* hls) NOEXCEPT;

    void
    hsl_to_rgb(
        const hsl_color& hls, rgb_color* rgb) NOEXCEPT;

    void
    rgb_to_xyz(
        const rgb_color* rgb,
        xyz_color* xyz) NOEXCEPT;

    void
    rgb_to_lab(
        const rgb_color* rgb,
        lab_color* lab) NOEXCEPT;

    void
    rgb_to_hcl(
        const rgb_color& rgb,
        hcl_color* lab) NOEXCEPT;

    void
    xyz_to_rgb(
        const xyz_color& xyz,
        rgb_color* rgb) NOEXCEPT;

    void
    xyz_to_lab(
        const xyz_color* xyz,
        lab_color* lab) NOEXCEPT;

    void
    lab_to_xyz(
        const lab_color* lab,
        xyz_color* xyz) NOEXCEPT;

    void
    lab_to_rgb(
        const lab_color& lab,
        rgb_color* rgb) NOEXCEPT;

    void
    hcl_to_lab(
        const hcl_color& hcl,
        lab_color* lab) NOEXCEPT;

    void
    lab_to_hcl(
        const lab_color& lab,
        hcl_color* hcl) NOEXCEPT;

    struct cmy_color {
        float Cyan;
        float Magenta;
        float Yellow;

        cmy_color() NOEXCEPT
        {
        }

        cmy_color(
            const float cyan,
            const float magenta,
            const float yellow) NOEXCEPT
            : Cyan(cyan),
              Magenta(magenta),
              Yellow(yellow)
        {
        }
    };

    struct yiq_color {
        float Y;
        float I;
        float Q;

        yiq_color() NOEXCEPT
        {
        }

        yiq_color(
            const float yval,
            const float ival,
            const float qval) NOEXCEPT
            : Y(yval),
              I(ival),
              Q(qval)
        {
        }
    };

    struct hsv_color {
        float Hue; //! Hue.
        float Saturation; //! Saturation.
        float Value; //! Value.

        hsv_color() NOEXCEPT
        {
        }

        hsv_color(
            const float hue,
            const float saturation,
            const float value) NOEXCEPT
            : Hue(hue),
              Saturation(saturation),
              Value(value)
        {
        }
    };

    struct hsl_color {
        float Hue; //! Hue.
        float Lightness; //! Lightness.
        float Saturation; //! Saturation.

        hsl_color() NOEXCEPT
        {
        }

        hsl_color(
            const float hue,
            const float lightness,
            const float saturation) NOEXCEPT
            : Hue(hue),
              Lightness(lightness),
              Saturation(saturation)
        {
        }

        hsl_color(const rgb_color& rgb) NOEXCEPT
        {
            rgb_to_hsl(rgb, this);
        }
    };

    struct xyz_color {
        float X;
        float Y;
        float Z;

        xyz_color() NOEXCEPT
        {
        }

        xyz_color(const float x, const float y, const float z) NOEXCEPT
            : X(x),
              Y(y),
              Z(z)
        {
        }
    };

    struct lab_color {
        union {
            struct {
                float L;
                float A;
                float B;
            };

            float Elements[3];
        };

        lab_color() NOEXCEPT
        {
        }

        lab_color(const float l, const float a, const float b) NOEXCEPT
            : L(l),
              A(a),
              B(b)
        {
        }
    };

    struct hcl_color {
        union {
            struct {
                float C;
                float S;
                float L;
            };

            float Elements[3];
        };

        hcl_color() NOEXCEPT
        {
        }

        hcl_color(const float chroma, const float sat, const float lum) NOEXCEPT
            : C(chroma),
              S(sat),
              L(lum)
        {
        }

        hcl_color(const rgb_color& rgb) NOEXCEPT
        {
            from_rgb(rgb);
        }

        hcl_color& from_rgb(const rgb_color& rgb) NOEXCEPT
        {
            rgb_to_hcl(rgb, this);
            return *this;
        }
    };

    ///
    /// \brief Represents a 4 component (red, green, blue, alpha)
    ///       normalized color vector (128 bits).
    /// \remarks Color operations (addition, substraction,
    ///         component-wise multiplication, scalar multiplication) can result
    ///         in individual components having values out of the [0, 1] range, so
    ///         some form of normalization should be used, to correct those situations.
    class rgb_color {
    public:
        union {

            struct {
                float Red; ///< Red component intensity
                float Green; ///< Green component intensity
                float Blue; ///< Blue component intensity
                float Alpha; ///< Alpha component (opacity)
            };

            float Elements[4];
        };

        rgb_color(
            const float r = 0.0f, 
            const float g = 0.0f, 
            const float b = 0.0f, 
            const float a = 1.0f) NOEXCEPT
            : Red{r},
              Green{g},
              Blue{b},
              Alpha{a}
        {
        }

        /// Construct from 32 bit value representing an RGBA color.
        explicit rgb_color(const xr_uint32_t u32color) NOEXCEPT
        {
            unsigned char red = (u32color >> 24) & 0xFF;
            unsigned char green = (u32color >> 16) & 0xFF;
            unsigned char blue = (u32color >> 8) & 0xFF;
            unsigned char alpha = u32color & 0xFF;

            Red = static_cast<float>(red) / 255.0f;
            Green = static_cast<float>(green) / 255.0f;
            Blue = static_cast<float>(blue) / 255.0f;
            Alpha = static_cast<float>(alpha) / 255.0f;
        }

        explicit rgb_color(const standard_colors stdColor) NOEXCEPT
            : rgb_color{static_cast<xr_uint32_t>(stdColor)}
        {}

        explicit rgb_color(const hsl_color& hls) NOEXCEPT
        {
            make_from_hsl(hls);
        }

        explicit rgb_color(const xyz_color& xyz) NOEXCEPT
        {
            make_from_xyz(xyz);
        }

        explicit rgb_color(const lab_color& lab) NOEXCEPT
        {
            make_from_lab(lab);
        }

        rgb_color& make_from_hsl(const hsl_color& hsl) NOEXCEPT
        {
            hsl_to_rgb(hsl, this);
            return *this;
        }

        rgb_color& make_from_xyz(const xyz_color& xyz) NOEXCEPT
        {
            xyz_to_rgb(xyz, this);
            return *this;
        }

        rgb_color& make_from_lab(const lab_color& lab) NOEXCEPT
        {
            lab_to_rgb(lab, this);
            return *this;
        }

        static rgb_color
        make_from_bgra(const xr_uint32_t u32bgra) NOEXCEPT
        {
            unsigned char blue = (u32bgra >> 24) & 0xFF;
            unsigned char green = (u32bgra >> 16) & 0xFF;
            unsigned char red = (u32bgra >> 8) & 0xFF;
            unsigned char alpha = u32bgra & 0xFF;
            return rgb_color(
                static_cast<float>(red) / 255.0f,
                static_cast<float>(green) / 255.0f,
                static_cast<float>(blue) / 255.0f,
                static_cast<float>(alpha) / 255.0f);
        }

        static rgb_color 
        make_from_argb(const xr_uint32_t u32argb) NOEXCEPT
        {
            unsigned char alpha = (u32argb >> 24) & 0xFF;
            unsigned char red = (u32argb >> 16) & 0xFF;
            unsigned char green = (u32argb >> 8) & 0xFF;
            unsigned char blue = u32argb & 0xFF;
            return rgb_color(
                static_cast<float>(red) / 255.0f,
                static_cast<float>(green) / 255.0f,
                static_cast<float>(blue) / 255.0f,
                static_cast<float>(alpha) / 255.0f);
        }

        xr_uint32_t
        to_rgba() const NOEXCEPT
        {
            unsigned int red = static_cast<unsigned int>(ceil(255 * Red)) << 24;
            unsigned int green = static_cast<unsigned int>(ceil(255 * Green)) << 16;
            unsigned int blue = static_cast<unsigned int>(ceil(255 * Blue)) << 8;
            unsigned int alpha = static_cast<unsigned int>(ceil(255 * Alpha));
            return red | green | blue | alpha;
        }

        xr_uint32_t
        to_bgra() const NOEXCEPT
        {
            unsigned int alpha = static_cast<unsigned int>(ceil(255 * Alpha)) << 24;
            unsigned int blue = static_cast<unsigned int>(ceil(255 * Blue)) << 16;
            unsigned int green = static_cast<unsigned int>(ceil(255 * Green)) << 8;
            unsigned int red = static_cast<unsigned int>(ceil(255 * Red));
            return alpha | blue | green | red;
        }

        rgb_color&
        operator+=(const rgb_color& rhs) NOEXCEPT
        {
            Red += rhs.Red;
            Green += rhs.Green;
            Blue += rhs.Blue;
            Alpha += rhs.Alpha;
            return *this;
        }

        rgb_color&
        operator-=(const rgb_color& rhs) NOEXCEPT
        {
            Red -= rhs.Red;
            Green -= rhs.Green;
            Blue -= rhs.Blue;
            Alpha -= rhs.Alpha;
            return *this;
        }

        rgb_color&
        operator*=(float const k) NOEXCEPT
        {
            Red *= k;
            Green *= k;
            Blue *= k;
            Alpha *= k;
            return *this;
        }

        /// Performs a componentwise multiplication between the two colors.
        rgb_color&
        operator*=(const rgb_color& other) NOEXCEPT
        {
            Red *= other.Red;
            Green *= other.Green;
            Blue *= other.Blue;
            Alpha *= other.Alpha;
            return *this;
        }

        rgb_color& operator/=(const float scalar) NOEXCEPT
        {
            float k = 1.0f / scalar;
            Red *= k;
            Green *= k;
            Blue *= k;
            Alpha *= k;
            return *this;
        }
    };

    inline rgb_color
    operator+(const rgb_color& lhs, const rgb_color& rhs) NOEXCEPT
    {
        rgb_color result(lhs);
        result += rhs;
        return result;
    }

    inline rgb_color
    operator-(const rgb_color& lhs, const rgb_color& rhs) NOEXCEPT
    {
        rgb_color result(lhs);
        result -= rhs;
        return result;
    }

    inline rgb_color
    operator*(const rgb_color& lhs, const rgb_color& rhs)NOEXCEPT
    {
        rgb_color result(lhs);
        result *= rhs;
        return result;
    }

    inline rgb_color
    operator*(const rgb_color& lhs, const float k)NOEXCEPT
    {
        rgb_color result(lhs);
        result *= k;
        return result;
    }

    inline rgb_color
    operator*(const float k, const rgb_color& rhs)NOEXCEPT
    {
        return rhs * k;
    }

    inline rgb_color
    operator/(const rgb_color& lhs, const float scalar) NOEXCEPT
    {
        const float inv = 1.0f / scalar;
        return lhs * inv;
    }

    inline void rgb_to_cmy(const rgb_color& rgb, cmy_color* cmy) NOEXCEPT
    {
        cmy->Cyan = 1.0f - rgb.Red;
        cmy->Magenta = 1.0f - rgb.Green;
        cmy->Yellow = 1.0f - rgb.Blue;
    }

    inline void cmy_to_rgb(const cmy_color& cmy, rgb_color* rgb) NOEXCEPT
    {
        rgb->Red = 1.0f - cmy.Cyan;
        rgb->Green = 1.0f - cmy.Magenta;
        rgb->Blue = 1.0f - cmy.Yellow;
    }

    inline void rgb_to_yiq(const rgb_color& rgb, yiq_color* yiq) NOEXCEPT
    {
        yiq->Y = 0.30f * rgb.Red + 0.59f * rgb.Green + 0.11f * rgb.Blue;
        yiq->I = 0.60f * rgb.Red - 0.28f * rgb.Green - 0.32f * rgb.Blue;
        yiq->Q = 0.21f * rgb.Red - 0.52f * rgb.Green + 0.31f * rgb.Blue;
    }

    inline void yiq_to_rgb(const yiq_color& yiq, rgb_color* rgb) NOEXCEPT
    {
        rgb->Red = yiq.Y + 0.6240f * yiq.Q + 0.9482f * yiq.I;
        rgb->Green = yiq.Y - 0.6398f * yiq.Q - 0.2760f * yiq.I;
        rgb->Blue = yiq.Y + 1.7298f * yiq.Q - 1.1054f * yiq.I;
    }

} // namespace math
} // namespace xray

//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <cmath>

#include "xray/xray.hpp"
#include "xray/math/constants.hpp"

namespace xray {
namespace math {
    namespace details {

        ///
        /// Determine at compile time whether a type is a so called arithmetic type.
        template <typename T>
        struct is_floating_point_type {
            enum {
                Yes = 0,
                No = !Yes
            };
        };

        template <>
        struct is_floating_point_type<float> {
            enum {
                Yes = 1,
                No = !Yes
            };
        };

        template <>
        struct is_floating_point_type<double> {
            enum {
                Yes = 1,
                No = !Yes
            };
        };

        template <>
        struct is_floating_point_type<long double> {
            enum {
                Yes = 1,
                No = !Yes
            };
        };

        template <typename real_type, bool is_floating_point = false>
        struct EqualityOperator {
            bool operator()(const real_type left, const real_type right)NOEXCEPT
            {
                return left == right;
            }
        };

        template <typename real_type>
        struct EqualityOperator<real_type, true> {
            bool operator()(const real_type left, const real_type right)NOEXCEPT
            {
                return std::abs(left - right) < numerics<real_type>::Epsilon();
            }
        };

    } // namespace details

    /// \addtogroup __GroupXrayMath_Utility
    /// @{

    ///
    /// \brief Test if two operands are equal. This will do an epsilon diff compare
    /// for floating point operands and an equality test for integer operands.
    /// \param[in] left Left operand.
    /// \param[in] right Right operand.
    /// \returns True if left operand is equal to right operand, false otherwise.
    template <typename T>
    inline bool
    compare_eq(const T left, const T right) NOEXCEPT
    {
        return details::EqualityOperator<T, details::is_floating_point_type<T>::Yes>()(left, right);
    }

    template <typename T>
    inline bool
    compare_ge(const T left, const T right) NOEXCEPT
    {
        return left > right || compare_eq(left, right);
    }

    template <typename T>
    inline bool
    compare_le(const T left, const T right) NOEXCEPT
    {
        return left < right || compare_eq(left, right);
    }

    template <typename real_t>
    inline bool
    is_zero(const real_t value) NOEXCEPT
    {
        return abs(value) < numerics<real_t>::Epsilon();
    }

    ///\brief Returns sgn(x) = { +1,    x >= 0
    ///                          -1,    x < 0 }
    template <typename T>
    inline xr_int32_t signum(const T val) NOEXCEPT
    {
        return (val > T(0)) - (val < T(0));
    }

    template <typename real_t>
    inline real_t
    to_degrees(const real_t radians) NOEXCEPT
    {
        return radians * numerics<real_t>::OneEightyOverPI();
    }

    template <typename real_t>
    inline real_t
    to_radians(const real_t degrees) NOEXCEPT
    {
        return degrees * numerics<real_t>::PIOver180();
    }

    /// \brief Returns the inverse of the square of x.
    template <typename real_t>
    inline real_t
    inv_sqrt(const real_t val) NOEXCEPT
    {
        return real_t{1} / sqrt(val);
    }

    /// clamp(x, min, max)  =   {   min,    x < min
    ///                             max,    x > max
    ///                             x }
    template <typename T>
    inline T clamp(const T val, const T min, const T max) NOEXCEPT
    {
        return (val < min ? min : (val > max ? max : val));
    }

    template <typename Ty>
    inline Ty min(const Ty left, const Ty right) NOEXCEPT
    {
        return left < right ? left : right;
    }

    template <typename T>
    inline T max(const T& left, const T& right) NOEXCEPT
    {
        return left > right ? left : right;
    }

    template <typename ty>
    inline void swap(ty& left, ty& right)
    {
        ty temp = left;
        left = right;
        right = temp;
    }

    template <typename real_t>
    real_t angle_from_xy(const real_t x, const real_t y) NOEXCEPT
    {
        real_t theta = real_t(0);

        if (compare_ge(x, real_t(0))) {
            theta = atan(y / x);
            if (theta < real_t(0)) {
                theta += numerics<real_t>::TwoPI();
            }
        } else {
            theta = atan(y / x) + numerics<real_t>::PI();
        }
        return theta;
    }

    /// @}

} // namespace Math
} // namespace Xray

//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file   array_dimension.hpp

#include "xray/xray.hpp"

namespace xray {
namespace base {

    /// \addtogroup __GroupXrayBase
    /// @{

    template <xr_size_t Size>
    struct array_dimension_helper {
        xr_char_t array_of_chars[Size];
    };

    template <typename T, xr_size_t Size>
    array_dimension_helper<Size> array_dimension_helper_func(T (&)[Size]);

} // namespace base
} // namespace xray

/// \def xr_array_size__(an_array)
/// \brief   A macro to compute the number of elements in an array, at compile
///          time.
/// \code
///     xr_int32_t array1[20];
///     auto numElements = xr_array_size__(array1);
/// \endcode
#if defined(xr_array_size__)
#undef xr_array_size__
#endif

#define xr_array_size__(array_of_elements) \
    (sizeof(xray::base::array_dimension_helper_func(array_of_elements).array_of_chars))

/// @}

//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file either.hpp

#include <cassert>
#include <algorithm>

#include "xray/xray.hpp"

namespace xray {
namespace base {

    /// \addtogroup __GroupXrayBase
    /// @{

    /// A class that can store one out of two possible value types.
    /// \code
    ///     either<ErrorCode, SocketDescriptor> OpenConnection(int port) {
    ///         //
    ///         // validate port
    ///         if (port < 25000 && port > 35000) {
    ///             return Err_InvalidPort;
    ///         }
    ///         SocketDescriptor sd = OpenSocket();
    ///         ...
    ///         return sd;
    ///     }
    ///
    ///     void SomeOtherFunc() {
    ///         Either<ErrorCode, SocketDescriptor> retData{OpenConnection(1024)};
    ///         if (retData.has_left_value()) {
    ///             //
    ///             // display error and exit
    ///             return;
    ///         }
    ///         //
    ///         // Safe to access and use the stored right value from here on.
    ///         SocketDescriptor sd = retData.RightValue();
    ///     }
    /// \endcode
    template <typename LeftValueType, typename RightValueType>
    class either {
        /// \name Defined types.
        /// @{

    public:
        typedef either<LeftValueType, RightValueType> class_type;

        /// @}

        /// \name Constructors
        /// @{

    public:
        either(const LeftValueType& existingValue)
            : is_left_value_(true)
        {
            new (&left_value_) LeftValueType(existingValue);
        }

        either(LeftValueType&& temporaryValue)
            : is_left_value_(true)
        {
            new (&left_value_) LeftValueType(std::move(temporaryValue));
        }

        either(const RightValueType& existingValue)
            : is_left_value_(false)
        {
            new (&right_value_) RightValueType(existingValue);
        }

        either(RightValueType&& temporaryValue)
            : is_left_value_(false)
        {
            new (&right_value_) RightValueType(std::move(temporaryValue));
        }

        either(class_type&& temporaryValue)
            : is_left_value_(temporaryValue.has_left_value())
        {
            if (has_left_value()) {
                new (&left_value_) LeftValueType(std::move(temporaryValue.left_value()));
            } else {
                new (&right_value_) RightValueType(std::move(temporaryValue.right_value()));
            }
        }

        ~either()
        {
            if (has_left_value()) {
                left_value_.~LeftValueType();
            } else {
                right_value_.~RightValueType();
            }
        }

        /// @}

        /// \name State/sanity.
        /// @{

    public:
        /// Test if object holds a value of LeftValueType.
        bool has_left_value() const NOEXCEPT
        {
            return is_left_value_;
        }

        /// Test if object holds a value of RightValueType.
        bool has_right_value() const NOEXCEPT
        {
            return !has_left_value();
        }

        /// @}

        /// \name Stored value access.
        /// @{

    public:
        /// Access the stored value.
        /// Check if the object has a left type value first.
        LeftValueType& left_value() NOEXCEPT
        {
            assert(has_left_value());
            return left_value_;
        }

        /// Access the stored value.
        /// Check if the object has a left type value first.
        const LeftValueType& left_value() const NOEXCEPT
        {
            assert(has_left_value());
            return left_value_;
        }

        /// Access the stored value.
        /// Check if the object has a right type value first.
        RightValueType& right_value() NOEXCEPT
        {
            assert(has_right_value());
            return right_value_;
        }

        /// Access the stored value.
        /// Check if the object has a Right type value first.
        const RightValueType& right_value() const NOEXCEPT
        {
            assert(has_right_value());
            return right_value_;
        }

        /// @}

        /// \name Data members.
        /// @{

    private:
        bool is_left_value_;

        union {
            LeftValueType left_value_;
            RightValueType right_value_;
        };

        /// @}

        /// \name Deleted member functions.
        /// @{

    private:
        either(class_type const&) = delete;
        class_type& operator=(class_type const&) = delete;
        class_type& operator=(class_type&&) = delete;

        /// @}
    };

    /// @}

} // namespace base
} // namespace xray

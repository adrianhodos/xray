//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file array_proxy.hpp

#include <cassert>
#include <iterator>

#include "xray/xray.hpp"

namespace xray {
namespace base {

    /// \addtogroup __GroupXrayBase
    /// @{

    /// Light-weight wrapper around built-in arrays, providing helper and
    /// convenience functions for safe array manipulation. Mostly
    /// inspired by <b>Imperfect C++</b>, section 14.4.
    /// \code
    ///  int array[100];
    ///  array_proxy<int> ap(array);
    ///
    ///  for (auto i = 0; i < ap.Lenght(); ++i) {
    ///     *(ap.Base() + i) = 0;
    ///      ap[i] = i;
    ///  }
    /// \endcode
    template <typename T>
    class array_proxy {

        /// \name Typedefs
        /// @{

    public:
        /// Type of elements stored in the array.
        typedef T value_type;

        /// Type of pointer to element.
        typedef T* pointer;

        /// Type of const pointer to element.
        typedef T* const_pointer;

        /// Type of reference to element.
        typedef T& reference;

        /// Type of const reference to element.
        typedef T& const_reference;

        /// Type of iterator.
        typedef T* iterator;

        /// Type of const iterator.
        typedef T* const_iterator;

        /// Type of reversed iterator.
        typedef std::reverse_iterator<iterator> reverse_iterator;

        /// Type of const reverse iterator.
        typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

        /// Type used for indexing and size.
        typedef xr_size_t size_type;

        /// Fully qualified type of this class.
        typedef array_proxy<T> class_type;

        /// @}

        /// \name Constructors
        /// @{

    public:
        /// Construct from pointers to a range of elements.
        array_proxy(T* const beg, T* const end) NOEXCEPT
            : range_begin_(beg),
              range_end_(end)
        {
        }

        /// Construct with a pointer and element count.
        array_proxy(T* const beg, const xr_size_t num_elements) NOEXCEPT
            : range_begin_(beg),
              range_end_(range_begin_ + num_elements)
        {
        }

        /// Construct from an existing array.
        template <xr_size_t N>
        array_proxy(T (&arr)[N]) NOEXCEPT
            : range_begin_(&arr[0]),
              range_end_(&arr[0] + N)
        {
        }

        /// Construct from pointers to a range of objects with the same size.
        template <typename U>
        array_proxy(U* beg, U* end) NOEXCEPT
            : range_begin_(beg),
              range_end_(end)
        {
            static_assert(sizeof(T) == sizeof(U), "Types must have same size!");
        }

        template <typename U>
        array_proxy(U* beg, const xr_size_t num_elements) NOEXCEPT
            : range_begin_(beg),
              range_end_(range_begin_ + num_elements)
        {
            static_assert(sizeof(T) == sizeof(U), "Types must have same size!");
        }

        /// Construct from an existing array of objects with the same size.
        template <typename U, xr_size_t N>
        array_proxy(U (&arr)[N]) NOEXCEPT
            : range_begin_(&arr[0]),
              range_end_(&arr[0] + N)
        {
            static_assert(sizeof(T) == sizeof(U), "Types must have same size!");
        }

        /// Construct from temporary that holds objects of the same size.
        // template <typename U>
        // array_proxy(array_proxy<U>&& rval) NOEXCEPT
        //     : range_begin_(rval.Base()),
        //       range_end_(rval.Base() + rval.Length())
        // {
        //     static_assert(sizeof(T) == sizeof(U), "Types must have same size!");
        // }

        // /// Assign from temporary that holds objects of the same size.
        // template <typename U>
        // class_type& operator=(array_proxy<U>& rval) NOEXCEPT
        // {
        //     static_assert(sizeof(T) == sizeof(U), "Types must have same size!");

        //     range_begin_ = rval.begin();
        //     range_end_ = rval.end();
        //     return *this;
        // }

        /// @}

        /// \name Attributes
        /// @{

    public:
        /// Returns the array's start address.
        pointer base() NOEXCEPT
        {
            return range_begin_;
        }

        /// Returns the array's start address.
        const_pointer base() const NOEXCEPT
        {
            return range_begin_;
        }

        /// Returns the number of elements in the array.
        size_type length() const NOEXCEPT
        {
            return range_end_ - range_begin_;
        }

        /// Test if array is empty.
        bool empty() const NOEXCEPT
        {
            return length() == 0;
        }

        /// @}

        /// \name Iteration
        /// @{

    public:
        /// Returns an iterator to the start of the sequence.
        iterator begin() NOEXCEPT
        {
            return range_begin_;
        }

        /// Returns a const iterator to the start of the sequence.
        const_iterator begin() const NOEXCEPT
        {
            return range_begin_;
        }

        /// Returns an iterator to the end of the sequence.
        iterator end() NOEXCEPT
        {
            return range_end_;
        }

        /// Returns a const iterator to the end of the sequence.
        const_iterator end() const NOEXCEPT
        {
            return range_end_;
        }

        /// Returns an interator to the start of the reversed sequence.
        reverse_iterator rbegin() NOEXCEPT
        {
            return reverse_iterator(begin());
        }

        /// Returns an interator to the end of the reversed sequence.
        reverse_iterator rend() NOEXCEPT
        {
            return reverse_iterator(end());
        }

        /// Returns a const interator to the start of the reversed sequence.
        const_reverse_iterator rbegin() const NOEXCEPT
        {
            return const_reverse_iterator(rbegin());
        }

        /// Returns a const interator to the end of the reversed sequence.
        const_reverse_iterator rend() const NOEXCEPT
        {
            return const_reverse_iterator(rend());
        }

        /// @}

        /// \name Subscripting
        /// @{

    public:
        /// Returns a reference to an element.
        /// \param idx Index of the desired element.
        reference operator[](const xr_size_t idx)NOEXCEPT
        {
            assert((idx < length()) && "Out of bounds index");
            return *(range_begin_ + idx);
        }

        /// Returns a const reference to an element.
        /// \param idx Index of the desired element.
        const_reference operator[](const xr_size_t idx) const NOEXCEPT
        {
            assert((idx < length()) && "Out of bounds index");
            return *(range_begin_ + idx);
        }

        /// @}

        /// \name Data members.
        ///  @{

    private:
        T* const range_begin_; ///< pointer to the first element.
        T* const range_end_; ///< pointer to one past the end element.

        /// @}
    };

    template <typename T, xr_size_t Size>
    inline array_proxy<T>
    make_array_proxy(T (&arrRef)[Size]) NOEXCEPT
    {
        return array_proxy<T>{arrRef};
    }

    template <typename T>
    inline array_proxy<T>
    make_array_proxy(T* const existingArray, const xr_size_t arrLenght) NOEXCEPT
    {
        return array_proxy<T>(existingArray, arrLenght);
    }

    template <typename T>
    inline array_proxy<T>
    make_array_proxy(T* const firstElement, T* const lastElement) NOEXCEPT
    {
        return array_proxy<T>{firstElement, lastElement};
    }

    /// @}

} // namespace base
} // namespace xray

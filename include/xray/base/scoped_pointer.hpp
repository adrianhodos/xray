//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file scoped_pointer.hpp
/// \brief Unique owning RAII pointer class and utilities.

#include "xray/xray.hpp"
#include "xray/base/pointer_policy/new_storage.hpp"
#include "xray/base/pointer_policy/assert_check.hpp"

namespace xray {
namespace base {

    /// \addtogroup __GroupXray_Base
    /// @{

    ///
    /// \brief A unique owning smart pointer class. It will release the memory
    ///  that the pointer points to, using the policy of deallocation specified
    ///  by the storage_policy<T> policy. Also supports various degrees
    ///  of checking (when accessing the pointer) via the checking_policy<T> policy.
    ///  The default storage policy releases the pointer by calling delete. The
    ///  default checking policy is to use assertions in debug mode, but no checking
    ///  in release mode.
    template <typename T,
              template <typename> class storage_policy = storage_new,
              template <typename> class checking_policy = assert_check>
    class scoped_pointer {
        /// \name Typedefs.
        ///  @{

    public:
        /// The storage policy determines how the memory is released (via delete,
        ///  delete[], free, etc.
        typedef storage_policy<T> storage_pol;

        /// The checking policy implements pointer sanity checks.
        typedef checking_policy<T> check_pol;

        /// Type of this class.
        typedef scoped_pointer<T, storage_policy, checking_policy> class_type;

        /// Type of wrapped pointer.
        typedef T* pointer;

        typedef T** pointer_pointer;

        /// Type of wrapped pointer to const.
        typedef const T* const_pointer;

        /// Reference to the pointed object.
        typedef T& reference;

        /// Const reference to the pointed object.
        typedef const T& const_reference;

        /// @}

        /// \name Construction.
        ///  @{

    public:
        scoped_pointer() NOEXCEPT
            : pointee_(nullptr)
        {
        }

        explicit scoped_pointer(T* ptr) NOEXCEPT
            : pointee_(ptr)
        {
        }

        /// @}

        /// \name Construction/assignment from temporary.
        ///  @{

    public:
        scoped_pointer(class_type&& right) NOEXCEPT
            : pointee_(nullptr)
        {
            this->swap(right);
        }

        ///
        /// \brief Construct from an rvalue object with a convertible pointer type.
        template <typename U>
        scoped_pointer(scoped_pointer<U, storage_policy, checking_policy>&& right) NOEXCEPT
            : pointee_(nullptr)
        {
            this->swap(right);
        }

        ///
        /// \brief Construct from a temporary of the same type.
        class_type& operator=(class_type&& right) NOEXCEPT
        {
            this->swap(right);
            return *this;
        }

        ///
        /// \brief Assign from a temporary holding a pointer to a convertible type.
        template <typename U>
        class_type& operator=(
            scoped_pointer<U, storage_policy, checking_policy>&& right) NOEXCEPT
        {
            this->swap(right);
            return *this;
        }

        ~scoped_pointer()
        {
            storage_pol::dispose(pointee_);
        }

        /// @} */

        /// \name Assignment.
        /// @}

    public:
        ///
        ///\brief Assign from a raw pointer.
        ///\remarks <b>The objects takes ownership of the raw pointer.
        ///         The raw pointer must have been allocated with a method that
        ///         is compatible with the smart pointer's storage policy. </b>
        class_type& operator=(T* rhs) NOEXCEPT
        {
            if (rhs != pointee_) {
                storage_pol::dispose(pointee_);
                pointee_ = rhs;
            }
            return *this;
        }

        /// @}

        /// \name Sanity checking.
        ///  @{

    public:
        explicit operator bool() const
        {
            return pointee_ != nullptr;
        }

        /// @}

        /// \name Resource access.
        /// @{

    public:
        pointer operator->() const NOEXCEPT
        {
            check_pol::check_ptr(pointee_);
            return pointee_;
        }

        reference operator*() NOEXCEPT
        {
            check_pol::check_ptr(pointee_);
            return *pointee_;
        }

        const_reference operator*() const NOEXCEPT
        {
            check_pol::check_ptr(pointee_);
            return *pointee_;
        }

        /// @}

        /// \name Subscripting support.
        /// @{

    public:
        /// Subscript operator, if the pointer points to an array.
        reference operator[](const xr_size_t index)NOEXCEPT
        {
            static_assert(storage_pol::is_array_ptr,
                          "Subscripting only applies to pointer to array!");
            check_pol::check_ptr(pointee_);
            return pointee_[index];
        }

        /// Subscript operator, if the pointer points to an array.
        const_reference operator[](const xr_size_t index) const NOEXCEPT
        {
            static_assert(storage_pol::is_array_ptr,
                          "Subscripting only applies to pointer to array!");
            check_pol::check_ptr(pointee_);
            return pointee_[index];
        }

        /// @}

        /// \name Non member accessor/manipulation functions.
        /// @{

    public:
        ///
        ///\brief Explicit access to the owned pointer of a scoped_pointer object.
        ///\param reference to a scoped_pointer object.
        ///\return The raw pointer that the scoped_pointer owns.
        template
            <typename U,
             template <typename> class V,
             template <typename> class W>
        friend U* scoped_pointer_get(
            const scoped_pointer<U, V, W>&) NOEXCEPT;

        ///
        ///\brief Releases ownerhip of the raw pointer to the caller, who now has the
        /// responsability of freeing the memory, when no longer needed.
        ///\param sp reference to a scoped_pointer object.
        ///\return The raw pointer.
        template
            <typename U,
             template <typename> class V,
             template <typename> class W>
        friend U* scoped_pointer_release(
            scoped_pointer<U, V, W>&) NOEXCEPT;

        ///
        ///\brief Reset the owned pointer of a scoped_pointer to the new value.
        ///\param sp reference to a scoped_pointer object.
        ///\param other The new pointer that the scoped_pointer object will own. This
        ///       pointer must have been allocated using the same method as the
        ///       original owned pointer.
        ///\remarks The old raw pointer owned by the scoped_pointer will be destroyed.
        template
            <typename U,
             template <typename> class V,
             template <typename> class W>
        friend void scoped_pointer_reset(
            scoped_pointer<U, V, W>&,
            U* other /* = nullptr */) NOEXCEPT;

        ///
        ///\brief Convenience function to get a pointer to the raw pointer
        ///       owned by the scoped_pointer object.
        ///\param sp reference to a scoped_pointer object.
        ///\remarks The memory pointed by the pointer owned by the object is
        ///first deallocated and the value of the pointer is set to null.
        template
            <typename U,
             template <typename> class V,
             template <typename> class W>
        friend U** scoped_pointer_get_impl(
            scoped_pointer<U, V, W>&) NOEXCEPT;

        ///
        ///\brief Swaps contents with another scoped_pointer object.
        template
            <typename U,
             template <typename> class V,
             template <typename> class W>
        friend void swap(
            scoped_pointer<U, V, W>&,
            scoped_pointer<U, V, W>&) NOEXCEPT;

        /// @}

        /// \name Private accessor/manipulator functions.
        /// @{

    private:
        ///
        /// \brief get
        /// \return Returns owned pointer, after checking by the specified
        /// checking policy.
        pointer get() const NOEXCEPT
        {
            check_pol::check_ptr(pointee_);
            return pointee_;
        }

        ///
        /// \brief release Releases ownership of the owned pointer to the caller.
        /// It is the caller's responsability to deallocate the memory.
        /// \return Owned pointer.
        pointer release() NOEXCEPT
        {
            T* old_val = pointee_;
            pointee_ = nullptr;
            return old_val;
        }

        ///
        /// \brief reset Resets the owned pointer to the specified one. Memory
        /// pointed by the existing owned pointer is freed. The new pointer
        /// must point to memory allocated using a method compatible with the
        /// release method specified by the storage policy.
        void reset(T* other) NOEXCEPT
        {
            if (pointee_ != other) {
                storage_pol::dispose(pointee_);
                pointee_ = other;
            }
        }

        ///
        /// \brief get_impl Returns a pointer to the owned pointer. Memory
        /// pointed by the existing owned pointer is freed.
        /// \return pointer to owned pointer.
        T** get_impl() NOEXCEPT
        {
            //
            // Release owned resource.
            class_type tmp_ptr;
            this->swap(tmp_ptr);

            return &pointee_;
        }

        /// \brief Swaps the contents of two scoped_pointer objects.
        void swap(class_type& other) NOEXCEPT
        {
            T* t_tmp = scoped_pointer_release(*this);
            T* u_tmp = scoped_pointer_release(other);

            scoped_pointer_reset(other, t_tmp);
            scoped_pointer_reset(*this, u_tmp);
        }

        /// @}

    private:
        ///< Owned pointer to a block of memeory
        T* pointee_;

        /// \name Disabled functions.
        /// @{

    private:
        NO_CC_ASSIGN(scoped_pointer);

        /// @}
    };

    template <typename T,
              template <typename> class U,
              template <typename> class W>
    inline bool operator==(
        const T* left,
        const scoped_pointer<T, U, W>& right) NOEXCEPT
    {
        return left == scoped_pointer_get(right);
    }

    template
        <typename T,
         template <typename> class U,
         template <typename> class W>
    inline bool operator!=(
        const T* left,
        const scoped_pointer<T, U, W>& right) NOEXCEPT
    {
        return !(left == right);
    }

    template
        <typename T,
         template <typename> class U,
         template <typename> class W>
    inline bool operator==(
        const scoped_pointer<T, U, W>& left,
        const T* right) NOEXCEPT
    {
        return right == left;
    }

    template
        <typename T,
         template <typename> class U,
         template <typename> class W>
    inline bool operator!=(
        const scoped_pointer<T, U, W>& left,
        const T* right) NOEXCEPT
    {
        return !(right == left);
    }

    template
        <typename T,
         template <typename> class U,
         template <typename> class W>
    inline bool operator==(
        const scoped_pointer<T, U, W>& left,
        std::nullptr_t) NOEXCEPT
    {
        return !static_cast<bool>(left);
    }

    template
        <typename T,
         template <typename> class U,
         template <typename> class W>
    inline bool operator==(
        std::nullptr_t,
        const scoped_pointer<T, U, W>& left) NOEXCEPT
    {
        return !static_cast<bool>(left);
    }

    template
        <typename T,
         template <typename> class U,
         template <typename> class W>
    inline bool operator!=(
        const scoped_pointer<T, U, W>& left,
        std::nullptr_t) NOEXCEPT
    {
        return static_cast<bool>(left);
    }

    template
        <typename T,
         template <typename> class U,
         template <typename> class W>
    inline bool operator!=(
        std::nullptr_t,
        const scoped_pointer<T, U, W>& left) NOEXCEPT
    {
        return static_cast<bool>(left);
    }

    template
        <typename U,
         template <typename> class V,
         template <typename> class W>
    inline U* scoped_pointer_get(
        const scoped_pointer<U, V, W>& sp) NOEXCEPT
    {
        return sp.get();
    }

    template
        <typename U,
         template <typename> class V,
         template <typename> class W>
    inline U* scoped_pointer_release(
        scoped_pointer<U, V, W>& sp) NOEXCEPT
    {
        return sp.release();
    }

    template
        <typename U,
         template <typename> class V,
         template <typename> class W>
    inline void scoped_pointer_reset(
        scoped_pointer<U, V, W>& sp,
        U* other = nullptr) NOEXCEPT
    {
        sp.reset(other);
    }

    template
        <typename U,
         template <typename> class V,
         template <typename> class W>
    inline U** scoped_pointer_get_impl(
        scoped_pointer<U, V, W>& sp) NOEXCEPT
    {
        return sp.get_impl();
    }

    template
        <typename T,
         typename U,
         template <typename> class S,
         template <typename> class C>
    inline void swap(
        scoped_pointer<T, S, C>& lhs,
        scoped_pointer<U, S, C>& rhs) NOEXCEPT
    {
        return lhs.swap(rhs);
    }

    template
        <typename U,
         template <typename> class V,
         template <typename> class W>
    void swap(
        scoped_pointer<U, V, W>& lhs,
        scoped_pointer<U, V, W>& rhs) NOEXCEPT
    {
        lhs.swap(rhs);
    }

    /// @}

} // namespace base
} // namespace xray

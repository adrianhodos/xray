//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once


/// \file xray.hpp
/// \brief Root header for the xray library.
/// Identifies the compiler and the operating system when
/// building the library. Also contains typedefs for the integer types.

#include <cstddef>

#define XRAY_STRINGIZE_a(x) \
    #x
#define XRAY_STRINGIZE_w(x) \
    L## #x
#define XRAY_PASTE_X_Y(X, Y) \
    X##Y
#define XRAY_PASTE_X_Y_Z(X, Y, Z) \
    X##Y##Z

#define XRAY_STRING_WIDEN(x) \
    XRAY_STRINGIZE_w(x)

#ifndef __WFILE__
#define __WFILE__ \
    XRAY_STRING_WIDEN(__FILE__)
#endif

#if defined(XRAY_COMPILER_IS_GCC)
#undef XRAY_COMPILER_IS_GCC
#endif

#if defined(XRAY_COMPILER_IS_MINGW)
#undef XRAY_COMPILER_IS_MINGW
#endif

#if defined(XRAY_COMPILER_IS_CLANG)
#undef XRAY_COMPILER_IS_CLANG
#endif

#if defined(XRAY_COMPILER_IS_MSVC)
#undef XRAY_COMPILER_IS_MSVC
#endif

///
/// Compiler detection macros.
/// See : http://sourceforge.net/p/predef/wiki/Compilers/

#if defined(_MSC_VER)

#define XRAY_COMPILER_IS_MSVC
#define XRAY_COMPILER_STRING \
    "Microsoft Visual C++ Compiler"
#define V8FRAMEWORK_GRAPHICS_API_IS_DIRECTX

#elif defined(__GNUC__) && !defined(__clang__)

#define XRAY_COMPILER_IS_GCC
#define XRAY_COMPILER_STRING \
    "GNU C/C++ Compiler"

#elif defined(__MINGW32__)

#define XRAY_COMPILER_IS_MINGW
#define XRAY_COMPILER_STRING \
    "MinGW Toolchain"

#elif defined(__GNUC__) && defined(__clang__)

/// clang defines both __GNUC__ and __clang__
#define XRAY_COMPILER_IS_CLANG
#define XRAY_COMPILER_STRING \
    "Clang C/C++ Compiler"

#else

//#error  Unsupported compiler.
#define XRAY_COMPILER_IS_MSVC
#define XRAY_COMPILER_STRING \
    "Microsoft Visual C++ Compiler"

#endif

///
/// OS detection macros.
/// See : http://sourceforge.net/p/predef/wiki/OperatingSystems/

#if defined(XRAY_OS_IS_WINDOWS)
#undef XRAY_OS_IS_WINDOWS
#endif

#if defined(XRAY_OS_IS_POSIX_COMPLIANT)
#undef XRAY_OS_IS_POSIX_COMPLIANT
#endif

#if defined(XRAY_OS_IS_POSIX_FAMILY)
#undef XRAY_OS_IS_POSIX_FAMILY
#endif

#if defined(XRAY_OS_IS_LINUX)
#undef XRAY_OS_IS_LINUX
#endif

#if defined(XRAY_OS_IS_FREEBSD)
#undef XRAY_OS_IS_FREEBSD
#endif

#if defined(__FreeBSD__)

#define XRAY_OS_IS_POSIX_COMPLIANT
#define XRAY_OS_IS_POSIX_FAMILY
#define XRAY_OS_IS_FREEBSD
#define XRAY_OS_STRING \
    "FreeBSD"

#elif defined(__gnu_linux__)

#define XRAY_OS_IS_POSIX_COMPLIANT
#define XRAY_OS_IS_POSIX_FAMILY
#define XRAY_OS_IS_LINUX
#define XRAY_OS_STRING \
    "GNU Linux"

#elif defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(__WINDOWS__)

#define XRAY_OS_IS_WINDOWS
#define XRAY_OS_STRING \
    "Windows"

#else

#error Unsupported system

#endif

#if defined(XRAY_COMPILER_IS_MSVC)

#include "xray/internal/msvc.hpp"

#elif defined(XRAY_COMPILER_IS_MINGW)

#include "xray/internal/mingw.hpp"

#elif defined(XRAY_COMPILER_IS_GCC)

#include "xray/internal/gcc.hpp"

#elif defined(XRAY_COMPILER_IS_CLANG)

#include "xray/internal/clang.hpp"

#else
#error Unknown compiler
#endif

/// ANSI character type
typedef char xr_char_t;
/// 8 bit integer
typedef XRAY_SI8BIT_BASE_TYPE xr_int8_t;
/// 8 bit signed integer
typedef XRAY_SI8BIT_BASE_TYPE xr_sint8_t;
/// 8 bit unsigned integer
typedef XRAY_UI8BIT_BASE_TYPE xr_uint8_t;

/// 16 bit integer
typedef XRAY_SI16BIT_BASE_TYPE xr_int16_t;
/// 16 bit signed integer
typedef XRAY_SI16BIT_BASE_TYPE xr_sint16_t;
/// 16 bit unsigned integer
typedef XRAY_UI16BIT_BASE_TYPE xr_uint16_t;

/// 32 bit integer
typedef XRAY_SI32BIT_BASE_TYPE xr_int32_t;
/// 32 bit signed integer
typedef XRAY_SI32BIT_BASE_TYPE xr_sint32_t;
/// 32 bit unsigned integer
typedef XRAY_UI32BIT_BASE_TYPE xr_uint32_t;

/// 64 bit integer
typedef XRAY_SI64BIT_BASE_TYPE xr_int64_t;
/// 64 bit signed integer
typedef XRAY_SI64BIT_BASE_TYPE xr_sint64_t;
/// 64 bit unsigned integer
typedef XRAY_UI64BIT_BASE_TYPE xr_uint64_t;

/// Short integer
typedef short xr_short_t;
/// Plain integer
typedef int xr_int_t;
/// Signed plain integer
typedef signed int xr_sint_t;
/// Unsigned plain integer
typedef unsigned int xr_uint_t;
/// Long integer
typedef long xr_long_t;
/// Unsigned long integer
typedef unsigned long xr_ulong_t;
/// Unsigned Byte type
typedef xr_uint8_t xr_ubyte_t;
/// Signed byte type
typedef xr_int8_t xr_sbyte_t;
/// Boolean type
typedef xr_uint32_t xr_bool_t;
/// Size type
typedef size_t xr_size_t;
/// Pointer difference type
typedef ptrdiff_t xr_ptrdiff_t;

/// \def NO_CC_ASSIGN(type_name)
/// \brief   A macro to suppress automatic generation by the compiler of
///          the copy constructor and assignment operator. If not using ISO C++ 11
///          compliant compiler, place it in the private part of the class definition.
/// \code
///  class U {
///	private :
///      NO_CC_ASSIGN(U);
///      ...
///  };
/// \endcode

#if defined(NO_CC_ASSIGN)
#undef NO_CC_ASSIGN
#endif

#if defined(XRAY_COMPILER_HAS_CXX11_DEFAULTED_DELETED_FUNCTIONS)

#define NO_CC_ASSIGN(type_name)           \
    type_name(type_name const&) = delete; \
    type_name& operator=(type_name const&) = delete

#else /* XRAY_COMPILER_HAS_CXX11_DEFAULTED_DELETED_FUNCTIONS */

#define NO_CC_ASSIGN(type_name)  \
    type_name(const type_name&); \
    type_name& operator=(const type_name&)

#endif /* !XRAY_COMPILER_HAS_CXX11_DEFAULTED_DELETED_FUNCTIONS */

///
/// \def XRAY_GEN_OPAQUE_TYPE(type)
/// \brief Generates a unique type.
#define XRAY_GEN_OPAQUE_TYPE(type)                   \
    typedef struct XRAY_internal_opaque_type##type { \
        int i;                                       \
    } const* type;

#if defined(XRAY_COMPILER_IS_MSVC)

#ifndef SUPPRESS_WARNING_START
#define SUPPRESS_WARNING_START(args) \
    __pragma(warning(push))          \
        __pragma(warning(disable     \
                         : args))
#endif /* !SUPPRESS_WARNING */

#ifndef SUPPRESS_WARNING_END
#define SUPPRESS_WARNING_END() \
    __pragma(warning(pop))
#endif /* !SUPPRESS_WARNING_END */

#else /* XRAY_COMPILER_IS_MSVC */

#ifndef SUPPRESS_WARNING_START
#define SUPPRESS_WARNING_START(...)
#endif /* !SUPPRESS_WARNING */

#ifndef SUPPRESS_WARNING_END
#define SUPPRESS_WARNING_END()
#endif /* !SUPPRESS_WARNING_END */

#endif /* !XRAY_COMPILER_IS_MSVC */

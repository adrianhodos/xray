//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file gpu_buffer.hpp

#include <cassert>
#include <d3d11.h>

#include "xray/xray.hpp"
#include "xray/base/windows/com_pointer.hpp"
#include "xray/rendering/directx/gpu_buffer_core.hpp"

namespace xray {
namespace rendering {

class           renderer_directx;
enum class      resource_usage;

namespace dx_detail {

struct vertexbuffer_traits {
    enum {
        k_bindval = 0
    };
};

struct indexbuffer_traits {
    enum {
        k_bindval = 1
    };
};

struct constantbuffer_traits {
    enum {
        k_bindval = 2
    };
};

} // namespace detail

/// \addtogroup __GroupXrayRendering_Directx
/// @{

template<typename buffer_traits>
class gpu_buffer : private dx_detail::gpu_buffer_core {

    /// \name Defined types.
    /// @{

public:
    typedef gpu_buffer<buffer_traits>                               class_type;
    typedef gpu_buffer_core                                         base_class;
    using base_class::base_handle_type;
    using base_class::handle_type;

    /// @}

    /// \name Construction/init.
    /// @{

public:

    gpu_buffer() NOEXCEPT {}

    gpu_buffer(
        const renderer_directx&         rsys,
        const xr_size_t                 k_num_elements,
        const xr_size_t                 k_element_size,
        const resource_usage            usage_type,
        const void*                     k_initial_data = nullptr) NOEXCEPT
        : base_class{rsys, buffer_traits::k_bindval, k_num_elements, k_element_size,
                     usage_type, k_initial_data}
    {}

    template<typename element_type, xr_size_t count>
    gpu_buffer(
        const renderer_directx&         rsys,
        const element_type              (&element_array_ref)[count],
        const resource_usage            usage_type) NOEXCEPT
        : base_class{rsys, buffer_traits::k_bindval, count, sizeof(element_array_ref[0]),
                     usage_type, &element_array_ref[0]}
    {}

    gpu_buffer(gpu_buffer&& rval) NOEXCEPT : base_class{std::move(rval)}
    {}

    class_type& operator=(class_type&& rval) NOEXCEPT {
        base_class::operator=(std::move(rval));
        return *this;
    }

    ~gpu_buffer() {}

    inline bool initialize(
        const renderer_directx&         rsys,
        const xr_size_t                 k_num_elements,
        const xr_size_t                 k_element_size,
        const resource_usage            usage_type,
        const void*                     k_initial_data = nullptr) NOEXCEPT;

    template<typename element_type, xr_size_t count>
    inline bool initialize(
        const renderer_directx&         rsys,
        const element_type (&element_arr_ref)[count],
        const resource_usage            usage_type) NOEXCEPT;

/// @}

/// \name Sanity check.
/// @{

public:

    explicit operator bool() const NOEXCEPT { return valid();  }

    using base_class::valid;

/// @}

/// \name Buffer attributes.
/// @{

public :

    using base_class::elements;
    using base_class::element_size;
    using base_class::byte_count;
    using base_class::handle;

/// @}

/// \name Buffer operations.
/// @{

public :

    //using base_class::set_data;
    //using base_class::get_data;
    //using base_class::internal_np_get_handle;

    //inline void bind_to_pipeline(
    //    renderer* rsys, 
    //    const v8_uint32_t k_offset = 0
    //    );

/// @}

private :
    NO_CC_ASSIGN(gpu_buffer);
};

template<typename T>
inline bool gpu_buffer<T>::initialize(
    const renderer_directx&         rsys,
    const xr_size_t                 k_num_elements,
    const xr_size_t                 k_element_size,
    const resource_usage            usage_type,
    const void*                     k_initial_data) NOEXCEPT
{
    return base_class::initialize(rsys, T::k_bindval, k_num_elements, k_element_size,
                                  usage_type, k_initial_data);
}

template<typename T>
template<typename element_type, xr_size_t count>
inline bool gpu_buffer<T>::initialize(
    const renderer_directx&         rsys,
    const element_type(&element_arr_ref)[count],
    const resource_usage            usage_type) NOEXCEPT
{
    return base_class::initialize(rsys, T::k_bindval, count, 
                                  sizeof(element_arr_ref[0]), usage_type, 
                                  &element_arr_ref[0]);
}

//template<typename buffer_traits>
//inline void gpu_buffer<buffer_traits>::bind_to_pipeline( 
//    renderer*  rsys, 
//    const v8_uint32_t   k_offset /* = 0 */ 
//    ) {
//    base_class::bind_to_pipeline_impl(rsys, this, k_offset, buffer_traits());
//}

typedef gpu_buffer<dx_detail::vertexbuffer_traits>      vertex_buffer;

typedef gpu_buffer<dx_detail::indexbuffer_traits>       index_buffer;

typedef gpu_buffer<dx_detail::constantbuffer_traits>    constant_buffer;

/// @}

} // namespace rendering
} // namespace xray

//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file debug_helpers.hpp

#include "xray/xray.hpp"
#include "xray/rendering/directx/dxerr.h"

namespace xray {
namespace rendering {
namespace dx_detail {

void 
dx_output_debug_string(
    const wchar_t*                  file,
    const xr_int32_t                line,
    const wchar_t*                  format_string,
    ...);

} // namespace detail
} // namespace rendering
} // namespace xray


#ifndef XRAY_WRAP_D3DCALL
#define XRAY_WRAP_D3DCALL(ret_code_ptr, call_and_args)                         \
    do {                                                                       \
        *(ret_code_ptr) = (call_and_args);                                     \
        if (FAILED(*(ret_code_ptr))) {                                         \
            xray::rendering::dx_detail::dx_output_debug_string(                \
                __WFILE__, __LINE__,                                           \
                L"Call %s failed, HRESULT %#08x, error string %s",             \
                XRAY_STRINGIZE_w(call_and_args), *(ret_code_ptr),              \
                DXGetErrorStringW(*(ret_code_ptr)));                           \
        }                                                                      \
    } while (0)
#endif

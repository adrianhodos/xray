//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file gpu_buffer_core.hpp

#include <cassert>
#include <utility>
#include <d3d11.h>

#include "xray/xray.hpp"
#include "xray/base/windows/com_pointer.hpp"
#include "xray/base/shims/pointer/scoped_pointer.hpp"

namespace xray {
namespace rendering {

class           renderer_directx;
enum class      resource_usage;

namespace dx_detail {

enum class buffer_type {
    k_vertex,
    k_index,
    k_constant,
    k_unordered_access
};

struct  vertexbuffer_traits;
struct  indexbuffer_traits;

/// \brief Common data and routines for buffers.
class gpu_buffer_core {

/// \name Typedefs.
/// @{

protected :

    typedef ID3D11Buffer                                base_handle_type;
    typedef base_handle_type*                           handle_type;

/// @}

/// \name Construction/init.
/// @{

protected :

    gpu_buffer_core() NOEXCEPT
    {}

    gpu_buffer_core(
        const renderer_directx&     rsys,
        const xr_int32_t            type,
        const xr_size_t             k_num_elements,
        const xr_size_t             k_element_size,
        const resource_usage        usage_type,
        const void*                 k_initial_data = nullptr) NOEXCEPT
    {
        initialize(rsys, type, k_num_elements, k_element_size,
                   usage_type, k_initial_data);
    }

    ///
    /// \brief Copy construct from an rvalue.
    gpu_buffer_core(gpu_buffer_core&& rhs) NOEXCEPT {
        steal_from_rvalue(std::move(rhs));
    }

    ///
    /// \brief Assign from an rvalue.
    gpu_buffer_core& operator=(gpu_buffer_core&& rhs) NOEXCEPT {
        steal_from_rvalue(std::move(rhs));
        return *this;
    }

    ~gpu_buffer_core() {}

    bool initialize(
        const renderer_directx&         rsys,
        const xr_int32_t                type,
        const xr_size_t                 k_num_elements,
        const xr_size_t                 k_element_size,
        const resource_usage            usage_type,
        const void*                     k_initial_data = nullptr) NOEXCEPT;

/// @}

public :

    /// \name Sanity checking
    /// @{

    bool valid() const NOEXCEPT {
        return buffer_handle_ != nullptr;
    }

    /// @}

public :

    /// \name Buffer attributes.
    /// @{

    xr_uint32_t elements() const NOEXCEPT { 
           assert(valid());
           return e_count_; 
    }

    xr_uint32_t element_size() const NOEXCEPT { 
        assert(valid());
        return e_size_; 
    }

    xr_uint32_t byte_count() const NOEXCEPT {
        assert(valid());
        return e_count_ * e_size_;
    }

    handle_type handle() const NOEXCEPT {
        assert(valid());
        return xray::base::raw_ptr(buffer_handle_);  
    }

/// @}

/// \name Operations.
/// @{

public :

    /////
    ///// \brief Copies the data from the CPU's memory into the buffer.
    ///// \param[in] data Pointer to memory containing data.
    ///// \param[in] byte_count Number of bytes to copy.
    ///// \note The buffer must have been created with the proper write
    ///// flags (i.e. not immutable), otherwise this operation will fail.
    //void set_data(renderer_directx* rsys, const void* data, const xr_size_t byte_count);

    /////
    ///// \brief Copies the data from the GPU into the CPU's memory.
    ///// \param[in] byte_count Number of bytes to copy.
    ///// \param[in, out] cpy_buffer Pointer to CPU memory where the data is
    ///// to be copied.
    ///// \note The buffer must have been created with the proper flags
    ///// if data transfer from the GPU to the CPU is needed.
    //void get_data(renderer_directx* rsys, const xr_size_t byte_count, void* output_buffer);

    //inline interface_pointer internal_np_get_handle() const;

protected :
    //static void bind_to_pipeline_impl(
    //    renderer_directx* rsys, 
    //    gpu_buffer_core* buff,
    //    const xr_uint32_t k_offset,
    //    const dx11_vertexbuffer_traits&
    //    );

    //static void bind_to_pipeline_impl(
    //    renderer_directx* rsys, 
    //    gpu_buffer_core* buff,
    //    const xr_uint32_t k_offset,
    //    const dx11_indexbuffer_traits&
    //    );

/// @}

private :

    ///
    /// \brief Acquire resources from an rvalue.
    void steal_from_rvalue(gpu_buffer_core&& rhs) NOEXCEPT {
        e_count_ = rhs.e_count_;
        e_size_ = rhs.e_size_;
        buffer_handle_ = base::scoped_pointer_release(rhs.buffer_handle_);
    }

/// \name Data members.
/// @{

private :

    /// Count of elements stored in the buffer.
    xr_uint32_t                                         e_count_ {0};

    /// Size of a single element.
    xr_uint32_t                                         e_size_ {0};

    /// Handle to API buffer resource. Owned by the object.
    xray::base::com_scoped_pointer<base_handle_type>    buffer_handle_;

/// @}

/// \name Disabled functions.
/// @{

private :

    NO_CC_ASSIGN(gpu_buffer_core);

/// @}
};

} // namespace dx_detail
} // namespace rendering
} // namespace xray

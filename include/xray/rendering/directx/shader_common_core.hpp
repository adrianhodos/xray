//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file shader_uniform.hpp    Shader uniform implementation.

#include <cassert>
#include <type_traits>
#include <vector>
#include <d3d11.h>

#include "xray/xray.hpp"
#include "xray/base/windows/com_pointer.hpp"
#include "xray/base/veneers/sequence_container_veneer.hpp"
#include "xray/rendering/directx/shader_uniform_block.hpp"
#include "xray/rendering/directx/shader_uniform.hpp"

struct ID3D11ShaderReflection;

namespace xray {
namespace rendering {
namespace dx_detail {    

template<typename random_access_iterator, typename predicate>
random_access_iterator 
binary_search_range(
    random_access_iterator          first,
    random_access_iterator          last,
    predicate                       query_predicate) NOEXCEPT
{
    const auto range_dist = std::distance(first, last);
    xr_size_t low = 0;
    xr_size_t high = range_dist;

    while (low < high) {
        const xr_size_t middle = (low + high) / 2;
        const xr_int32_t cmp_result = query_predicate(*(first + middle));

        if (cmp_result < 0)
            high = middle;
        else if (cmp_result > 0)
            low = middle;
        else
            return first + middle;
    }

    return last;
}

template<typename container_type, typename predicate>
inline typename container_type::iterator 
binary_search_container(
    container_type&             cont, 
    predicate                   query_predicate) NOEXCEPT
{
    static_assert(std::is_same<std::iterator_traits<typename container_type::iterator>::iterator_category,
                               std::random_access_iterator_tag>::value,
                 "Container argument must be a sequential container!");

    return binary_search_range(std::begin(cont), std::end(cont), query_predicate);
}

template<typename T, xr_size_t count, typename predicate>
inline T* 
binary_search_container(
    T                   (&arr_ref)[count], 
    predicate           pr__) NOEXCEPT 
{
    return binary_search_range(std::begin(arr_ref), std::end(arr_ref), pr__);
}

template<typename container, typename predicate>
inline typename container::value_type* 
get_ordered_container_element(
    container&              sequential_container,
    predicate               query_predicate) NOEXCEPT 
{
    auto itr_resource = binary_search_container(
        sequential_container, query_predicate);

    if (itr_resource == std::end(sequential_container))
        return nullptr;

    return &*itr_resource;
}

struct sampler_state_t {
    
    ///< Sampler's name (sampler's variable name in the shader code).
    std::string                 name_;

    ///< GPU register that the samplr is bound to.
    xr_uint32_t                 bindpoint_;

    sampler_state_t() {}

    sampler_state_t(const char*             name, 
                    const xr_uint32_t       bind_point)
        :   name_{name}, 
            bindpoint_{bind_point} {}
};

struct resource_view_t {
    ///< Resource view's name
    std::string                 rvt_name;

    //! GPU register that the RV is bound to.
    xr_uint32_t                 rvt_bindpoint;

    //! Dimension info for the RV.
    D3D10_SRV_DIMENSION         rvt_dimension;

    //! Number of RV's. 
    xr_uint32_t                 rvt_count;

    resource_view_t() {}

    resource_view_t(
        const char*                         name,
        const xr_uint32_t                   bind_point,
        const D3D10_SRV_DIMENSION           dimension,
        const xr_uint32_t                   count)
        :   rvt_name{name},
            rvt_bindpoint{bind_point},
            rvt_dimension{dimension},
            rvt_count{count} 
    {}
};

/// \brief Defines data and routines that are common to all types of
/// shaders.
class shader_common_core {
private :
    struct shader_uniform_block_dtor_t {
        void operator()(shader_uniform_block* uniform_blk) const NOEXCEPT {
            delete uniform_blk;
        }
    };    

    typedef std::vector
    <
        shader_uniform_block*
    >                                               uniform_block_table_t;

    typedef base::sequence_container_veneer
    <
        uniform_block_table_t,
        shader_uniform_block_dtor_t
    >                                               scoped_uniform_block_table_t;
        
public :
    
    ///
    /// Shader's compiled bytecode.
    base::com_scoped_pointer<ID3D10Blob>                    bytecode;

    ///
    /// Shader's associated input parameters signature.
    base::com_scoped_pointer<ID3D11InputLayout>             input_signature;

    /// List of uniform blocks in the shader.
    scoped_uniform_block_table_t                            uniform_blocks;

    /// Uniform block handles. Passed to the shader before it starts
    /// execution on the GPU.
    std::vector<ID3D11Buffer*>                              uniform_block_binding_list;

    /// List of each uniform, in the shader.
    std::vector<shader_uniform>                             uniforms;

    /// Sampler state info list.
    std::vector<sampler_state_t>                            sampler_states;

    /// Sampler state handles. Passed to the shader before it starts
    /// execution on the GPU.
    std::vector<ID3D11SamplerState*>                        sampler_state_binding_list;

    /// Resource view info list.
    std::vector<resource_view_t>                            resource_views;

    /// Resource view handle list. Passed to the shader before it starts
    /// execution on the GPU.
    std::vector<ID3D11ShaderResourceView*>                  resource_view_binding_list;

    /// \name Construction/initialisation.
    /// @{

public:

    /// Default constructor. Does nothing, shader_common_core::reflect_shader() 
    /// must be called before using the object's members.
    shader_common_core() {
        uniform_blocks.reserve(128);
        uniforms.reserve(128);
    }

    ~shader_common_core() {}

    shader_common_core(shader_common_core&& rrhs) {
        *this = std::move(rrhs);
    }        

    /// Reflects the shader and initializes this object.
    /// \param compiled_bytecode Pointer to an ID3D10Blob interface,
    /// representing the shader's compiled bytecode. The objects assumes
    /// ownership of the pointer.
    /// \param device Pointer to an ID3D11Device interface.
    /// \returns True on success, false on failure.
    bool reflect_shader(
        ID3D10Blob*             compiled_bytecode, 
        ID3D11Device*           device);

    /// @}

public :

    /// \name Assignment operators
    /// @{

    /// Self assign from a temporary
    shader_common_core& operator=(shader_common_core&& rrhs);

    /// @}

    /// \name Access to shader's uniforms.
    /// @{

public :

    /// \todo Debug output when querying invalid uniform
    shader_uniform_block* get_uniform_block_by_name(
        const char* block_name) NOEXCEPT;

    /// \todo Debug output when querying invalid uniform
    inline shader_uniform* get_uniform_by_name(
        const char* uniform_name) NOEXCEPT;

    /// Lookup a bound sampler, by name.
    inline sampler_state_t* get_sampler_by_name(
        const char* sampler_name) NOEXCEPT;

    /// Lookup a bound resource view, by name.
    inline resource_view_t* get_resourceview_by_name(
        const char* rv_name) NOEXCEPT;

    /// @}
    
private :
    NO_CC_ASSIGN(shader_common_core);
};

inline shader_uniform*
shader_common_core::get_uniform_by_name(const char* uniform_name) NOEXCEPT 
{
    return get_ordered_container_element(
        uniforms,
        [uniform_name](const shader_uniform& uniform) {
            return strcmp(uniform.Name.c_str(), uniform_name);
    });
}

inline sampler_state_t* 
shader_common_core::get_sampler_by_name(const char* sampler_name) NOEXCEPT
{
    return get_ordered_container_element(
        sampler_states,
        [sampler_name](const sampler_state_t& state) {
            return strcmp(state.name_.c_str(), sampler_name);
    });
}

inline resource_view_t* 
shader_common_core::get_resourceview_by_name(const char* rv_name) NOEXCEPT
{
    return get_ordered_container_element(
        resource_views,
        [rv_name](const resource_view_t& state) {
            return strcmp(state.rvt_name.c_str(), rv_name);
    });
}

} // namespace dx_detail
} // namespace rendering
} // namespace xray

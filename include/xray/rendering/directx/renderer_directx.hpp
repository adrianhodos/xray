//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file renderer_directx.hpp

#pragma once

#include <d3d11.h>
#include <windows.h>

#include "xray/xray.hpp"
#include "xray/base/windows/com_pointer.hpp"
#include "xray/base/shims/pointer/scoped_pointer.hpp"
#include "xray/math/color.hpp"

namespace xray {
namespace rendering {

namespace dx_detail {

template<typename shader_type>
struct shader_traits;

template<>
struct shader_traits<ID3D11VertexShader> {
    typedef ID3D11VertexShader*         shader_handle;

    static shader_handle create_shader(ID3D11Device* device, ID3D10Blob* compiled_bytecode) NOEXCEPT {
        ID3D11VertexShader* new_shader = nullptr;
        device->CreateVertexShader(compiled_bytecode->GetBufferPointer(),
                                   compiled_bytecode->GetBufferSize(),
                                   nullptr,
                                   &new_shader);
        return new_shader;
    }
};

template<>
struct shader_traits<ID3D11PixelShader> {
    typedef ID3D11PixelShader*         shader_handle;

    static shader_handle create_shader(ID3D11Device* device, ID3D10Blob* compiled_bytecode) NOEXCEPT{
        ID3D11PixelShader* new_shader = nullptr;
        device->CreatePixelShader(compiled_bytecode->GetBufferPointer(),
                                   compiled_bytecode->GetBufferSize(),
                                   nullptr,
                                   &new_shader);
        return new_shader;
    }
};

} // namespace detail

/// \addtogroup __GroupXrayRendering_Directx
/// @{

enum class resource_usage;

enum class shader_profile {
    vs_profile_4_0,
    vs_profile_4_1,
    vs_profile_5_0,
    ps_profile_4_0,
    ps_profile_4_1,
    ps_profile_5_0
};

typedef ID3D10Blob* shader_bytecode_handle;

shader_bytecode_handle compile_shader_from_file(
    const char*                             file_name,
    const char*                             entry_point,
    const shader_profile                    target_profile,
    const xr_uint32_t                       compile_flags = 0u,
    const D3D_SHADER_MACRO*                 compile_defines = nullptr);

shader_bytecode_handle compile_shader_from_memory(
    const char*                             shader_source_code,
    const xr_size_t                         source_code_length,
    const char*                             entry_point,
    const shader_profile                    target_profile,
    const xr_uint32_t                       compile_flags = 0u,
    const D3D_SHADER_MACRO*                 compile_defines = nullptr);

class renderer_directx {
public :
    typedef ID3D11Device*                   device_handle_type;
    typedef ID3D11DeviceContext*            device_context_handle_type;

public :

    renderer_directx();

    explicit operator bool() const NOEXCEPT {
        return dx_device_ != nullptr;
    }

public :

    device_handle_type device() const NOEXCEPT {
        assert(is_valid());
        return xray::base::raw_ptr(dx_device_);
    }

    device_context_handle_type device_context() const NOEXCEPT {
        assert(is_valid());
        return xray::base::raw_ptr(devcontext_);
    }

    void clear() const NOEXCEPT;

    void set_clear_color(const xray::math::rgb_color& color) NOEXCEPT {
        clear_color_ = color;
    }

    /// \name Resources
    /// @{

public :

    ID3D11Buffer* 
    make_vertex_buffer(
        const xr_size_t                     element_count,
        const xr_size_t                     element_size,
        const resource_usage                usage_type,
        const void*                         initial_data = nullptr) const NOEXCEPT;

    template<typename vertex_type, xr_size_t count>
    inline ID3D11Buffer* 
    make_vertex_buffer(
        const vertex_type (&vertex_arr_ref)[count],
        const resource_usage usage_type) const NOEXCEPT;

    ID3D11Buffer*
    make_index_buffer(
        const xr_size_t                     element_count,
        const xr_size_t                     element_size,
        const resource_usage                usage_type,
        const void*                         initial_data = nullptr) const NOEXCEPT;

    template<typename index_type, xr_size_t count>
    inline ID3D11Buffer*
    make_index_buffer(
        const index_type (&index_arr_ref)[count],
        const resource_usage usage_type) const NOEXCEPT;

    ID3D11Buffer* 
    make_constant_buffer(
        const xr_size_t                     byte_size,
        const resource_usage                usage_type,
        const void*                         initial_data = nullptr) const NOEXCEPT;

    /// @}

    /// \name shaders
    /// @{

public :

    inline ID3D11VertexShader*
    make_vertex_shader_from_file(
        const char*                             file_name,
        const char*                             entry_point,
        const shader_profile                    target_profile,
        const xr_uint32_t                       compile_flags = 0u,
        const D3D_SHADER_MACRO*                 compile_defines = nullptr) const NOEXCEPT;

    inline ID3D11PixelShader*
    make_pixel_shader_from_file(
        const char*                             file_name,
        const char*                             entry_point,
        const shader_profile                    target_profile,
        const xr_uint32_t                       compile_flags = 0u,
        const D3D_SHADER_MACRO*                 compile_defines = nullptr) const NOEXCEPT;

private:

    template<typename shader_type>
    typename dx_detail::shader_traits<shader_type>::shader_handle
    create_shader_from_file(
        const char*                             file_name,
        const char*                             entry_point,
        const shader_profile                    target_profile,
        const xr_uint32_t                       compile_flags = 0u,
        const D3D_SHADER_MACRO*                 compile_defines = nullptr) const NOEXCEPT;

    template<typename shader_type>
    typename dx_detail::shader_traits<shader_type>::shader_handle
    create_shader_from_memory(
        const char*                             shader_source_code,
        const xr_size_t                         source_code_length,
        const char*                             entry_point,
        const shader_profile                    target_profile,
        const xr_uint32_t                       compile_flags = 0u,
        const D3D_SHADER_MACRO*                 compile_defines = nullptr) const NOEXCEPT;

    /// @}

public :

    void handle_resize(const xr_int32_t, const xr_int32_t) NOEXCEPT;

    void restore_after_resize(ID3D11Texture2D* backbuffer_texture) NOEXCEPT;

private :

    bool is_valid() const NOEXCEPT {
        return dx_device_ != nullptr && devcontext_ != nullptr;
    }

private :
    xray::base::com_scoped_pointer<ID3D11Device>                dx_device_;
    xray::base::com_scoped_pointer<ID3D11DeviceContext>         devcontext_;
    xray::base::com_scoped_pointer<ID3D11RenderTargetView>      render_target_view_;
    xray::base::com_scoped_pointer<ID3D11Texture2D>             depth_stencil_texture_;
    xray::base::com_scoped_pointer<ID3D11DepthStencilView>      depth_stencil_view_;
    xray::math::rgb_color                                       clear_color_ { 1.0f, 0.5f, 1.0f, 1.0f };

private :
    NO_CC_ASSIGN(renderer_directx);
};

template<typename vertex_type, xr_size_t count>
inline ID3D11Buffer*
renderer_directx::make_vertex_buffer(
    const vertex_type (&vertex_arr_ref)[count],
    const resource_usage usage_type) const NOEXCEPT
{
    assert(is_valid());
    assert(usage_type == resource_usage::cpu_writable || 
           usage_type == resource_usage::immutable);

    return make_vertex_buffer(count, sizeof(vertex_type), usage_type, &vertex_arr_ref);
}

template<typename index_type, xr_size_t count>
inline ID3D11Buffer*
renderer_directx::make_index_buffer(
    const index_type (&index_arr_ref)[count],
    const resource_usage usage_type) const NOEXCEPT
{
    assert(is_valid());
    assert(usage_type == resource_usage::cpu_writable ||
           usage_type == resource_usage::immutable);

    static_assert(sizeof(index_type) <= sizeof(DWORD), "Type too big fot index buffer");

    return make_index_buffer(count, sizeof(index_type), usage_type, &index_arr_ref);
}

inline ID3D11VertexShader*
renderer_directx::make_vertex_shader_from_file(
    const char*                             file_name,
    const char*                             entry_point,
    const shader_profile                    target_profile,
    const xr_uint32_t                       compile_flags,
    const D3D_SHADER_MACRO*                 compile_defines) const NOEXCEPT
{
    return create_shader_from_file<ID3D11VertexShader>(file_name, 
                                                       entry_point, 
                                                       target_profile, 
                                                       compile_flags, 
                                                       compile_defines);
}

inline ID3D11PixelShader*
renderer_directx::make_pixel_shader_from_file(
    const char*                             file_name,
    const char*                             entry_point,
    const shader_profile                    target_profile,
    const xr_uint32_t                       compile_flags,
    const D3D_SHADER_MACRO*                 compile_defines) const NOEXCEPT
{
    return create_shader_from_file<ID3D11PixelShader>(file_name,
                                                      entry_point,
                                                      target_profile,
                                                      compile_flags,
                                                      compile_defines);
}

template<typename shader_type>
typename dx_detail::shader_traits<shader_type>::shader_handle
renderer_directx::create_shader_from_file(
    const char*                             file_name,
    const char*                             entry_point,
    const shader_profile                    target_profile,
    const xr_uint32_t                       compile_flags,
    const D3D_SHADER_MACRO*                 compile_defines) const NOEXCEPT
{
    using namespace xray::base;

    com_scoped_pointer<ID3D10Blob> compiled_bytecode{
        compile_shader_from_file(file_name, entry_point, target_profile, compile_flags, compile_defines)};

    if (!compiled_bytecode)
        return nullptr;

    return dx_detail::shader_traits<shader_type>::create_shader(raw_ptr(dx_device_),
                                                                raw_ptr(compiled_bytecode));
}

template<typename shader_type>
typename dx_detail::shader_traits<shader_type>::shader_handle
renderer_directx::create_shader_from_memory(
    const char*                             shader_source_code,
    const xr_size_t                         source_code_length,
    const char*                             entry_point,
    const shader_profile                    target_profile,
    const xr_uint32_t                       compile_flags,
    const D3D_SHADER_MACRO*                 compile_defines) const NOEXCEPT
{
    using namespace xray::base;

    com_scoped_pointer<ID3D10Blob> compiled_bytecode{
        compile_shader_from_memory(shader_source_code, 
                                   source_code_length, 
                                   entry_point, 
                                   target_profile, 
                                   compile_flags, 
                                   compile_defines)
    };

    if (!compiled_bytecode)
        return nullptr;

    return dx_detail::shader_traits<shader_type>::create_shader(raw_ptr(dx_device_),
                                                                raw_ptr(compiled_bytecode));
}

    /// @}

} // namespace rendering
} // namespace xray

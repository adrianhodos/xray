//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file shader_uniform.hpp    Shader uniform implementation.

#include <cassert>
#include <string>
#include <d3d11shader.h>

#include "xray/xray.hpp"
#include "xray/base/windows/com_pointer.hpp"
#include "xray/rendering/directx/shader_uniform_block.hpp"

namespace xray {
namespace rendering {
namespace dx_detail {

/// Stores information abount a shader uniform.
struct shader_uniform {

    /// Pointer to parent block.
    shader_uniform_block*       ParentBlock;

    /// Uniform's name.
    std::string                 Name;

    /// Uniform's offset in bytes, from the start of the parent block.
    xr_uint32_t                 StartOffset;

    /// Uniform's size in bytes.
    xr_uint32_t                 Size;

    /// \name Constructors
    /// @{
    
    shader_uniform(
        const D3D11_SHADER_VARIABLE_DESC& var_desc,
        shader_uniform_block* parent)
        : ParentBlock{parent},
          Name{var_desc.Name},
          StartOffset{var_desc.StartOffset},
          Size{var_desc.Size} 
    {}

    /// @}

    /// \name Uniform data manipulation
    /// @{

    template<typename DataType>
    inline void set_value(
        const DataType& value) NOEXCEPT;

    inline void set_raw_value(
        const void* data, 
        const xr_uint32_t byte_count) NOEXCEPT;

    /// @}
};

template<typename DataType>
inline void 
shader_uniform::set_value(const DataType& value) NOEXCEPT {
    set_raw_value(&value, sizeof(value));
}

inline void 
shader_uniform::set_raw_value(
    const void*             data, 
    const xr_uint32_t       byte_count) NOEXCEPT
{
    assert(ParentBlock);
    assert(byte_count <= Size);
    ParentBlock->set_block_component_data(data, StartOffset, byte_count);
}


} // namespace dx_detail
} // namespace rendering
} // namespace xray

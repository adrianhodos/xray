//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file shader_uniform.hpp    Shader uniform block implementation.

#include <cassert>
#include <string>
#include <utility>
#include <d3d11.h>
#include <d3d11shader.h>

#include "xray/xray.hpp"
#include "xray/base/windows/com_pointer.hpp"
#include "xray/base/pointer_policy/malloc_array_storage.hpp"
#include "xray/base/shims/pointer/scoped_pointer.hpp"

namespace xray {
namespace rendering {
namespace dx_detail {

/// Stores information about a uniform block defined in a shader.
struct shader_uniform_block {

    ///< Handle to constant buffer in GPU memory.
    base::com_scoped_pointer<ID3D11Buffer>                              BufferHandle;

    ///< Pointer to the buffer in CPU memory. 
    base::scoped_pointer<xr_ubyte_t, base::storage_malloc_array>        DataStorage;

    ///< Block's name.
    std::string                                                 BlockName;

    ///< Block variable count.
    xr_uint32_t                                                 VarCount{0};

    ///< Block's size, in bytes.
    xr_uint32_t                                                 BlockSize{0};

    ///< Binding point (GPU register).
    xr_uint32_t                                                 GpuBindPoint{0};

    ///< True if data in CPU memory is out of sync with data in GPU memory.
    mutable bool                                                DataDirty{false};

public :
    
    /// \name Constructors
    /// @{
    
    shader_uniform_block() {}

    shader_uniform_block(shader_uniform_block&& rvalue) NOEXCEPT {
        *this = std::move(rvalue);
    }

    /// @}

    /// \name Operators
    ///
    
    shader_uniform_block& operator=(shader_uniform_block&& rvalue) NOEXCEPT {
        BufferHandle       = std::move(rvalue.BufferHandle);
        DataStorage = std::move(rvalue.DataStorage);
        BlockName         = std::move(rvalue.BlockName);
        VarCount     = rvalue.VarCount;
        BlockSize         = rvalue.BlockSize;
        DataDirty   = rvalue.DataDirty;

        return *this;
    }

    explicit operator bool() const NOEXCEPT {
        return BufferHandle != nullptr;
    }

    /// @}

public :

    /// \name Attributes
    /// @{
    
    const std::string& name() const NOEXCEPT {
        return BlockName;
    }
    
    xr_uint32_t get_bind_point() const NOEXCEPT {
        return GpuBindPoint;
    }

    ID3D11Buffer* get_handle() const NOEXCEPT {
        return base::raw_ptr(BufferHandle);
    }

    /// @}

public :

    /// \name Operations (initialisation + content manipulation routines)
    /// @}
   
    inline bool initialize(
        ID3D11Device* device_context, 
        const D3D11_SHADER_BUFFER_DESC& buff_desc,
        const xr_uint32_t bind_point) NOEXCEPT;

    inline void sync_data_with_gpu(
        ID3D11DeviceContext* device_context) NOEXCEPT; 

    inline void set_block_component_data(
        const void* component_data, 
        const xr_uint32_t component_offset, 
        const xr_uint32_t component_size) NOEXCEPT;

    template<typename Component_Type>
    inline void set_component_data(
        const Component_Type& component_value,
        const xr_uint32_t component_offset) NOEXCEPT;

    template<typename ValueType>
    inline void set_block_data(
        const ValueType& value) NOEXCEPT;

    /// @}

private :
    NO_CC_ASSIGN(shader_uniform_block);
};

inline bool 
shader_uniform_block::initialize(
    ID3D11Device* device_context, 
    const D3D11_SHADER_BUFFER_DESC& buff_desc,
    const xr_uint32_t bind_point) NOEXCEPT 
{
    assert(!BufferHandle);
    assert(!DataStorage);

    BlockName = buff_desc.Name;
    VarCount = buff_desc.Variables;
    BlockSize = buff_desc.Size;
    DataDirty = false;
    GpuBindPoint = bind_point;

    const D3D11_BUFFER_DESC buffer_description = {
        buff_desc.Size,
        D3D11_USAGE_DEFAULT,
        D3D11_BIND_CONSTANT_BUFFER,
        0,
        0,
        buff_desc.Size
    };

    const HRESULT ret_code = device_context->CreateBuffer(
        &buffer_description, nullptr, base::raw_ptr_ptr(BufferHandle));

    if (FAILED(ret_code))
        return false;

    DataStorage = static_cast<xr_ubyte_t*>(malloc(buff_desc.Size * sizeof(xr_ubyte_t)));

    return DataStorage != nullptr;
}

inline void 
shader_uniform_block::sync_data_with_gpu(
    ID3D11DeviceContext* device_context) NOEXCEPT 
{
    if (DataDirty) {
        device_context->UpdateSubresource(
            base::raw_ptr(BufferHandle), 0, nullptr, base::raw_ptr(DataStorage), 
            0, 0);
    }
}

inline void 
shader_uniform_block::set_block_component_data(
    const void* component_data, 
    const xr_uint32_t component_offset, 
    const xr_uint32_t component_size) NOEXCEPT 
{
    assert(component_size <= (BlockSize - component_offset));

    xr_ubyte_t* mem_ptr = base::raw_ptr(DataStorage) + component_offset;
    memcpy(mem_ptr, component_data, component_size);
    DataDirty = true;
}

template<typename Component_Type>
inline void 
shader_uniform_block::set_component_data( 
    const Component_Type& component_value, 
    const xr_uint32_t component_offset) NOEXCEPT {
    set_block_component_data(&component_value, component_offset, sizeof(component_value));
}

template<typename ValueType>
inline void 
shader_uniform_block::set_block_data(const ValueType& val) NOEXCEPT {
    set_block_component_data(&val, 0, sizeof(val));
}

} // namespace dx_detail
} // namespace rendering
} // namespace xray

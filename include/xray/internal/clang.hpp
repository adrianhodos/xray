//
// Copyright (c) 2011, 2012, 2013 Adrian Hodos
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the author nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR THE CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

/// \file clang.hpp 
/// \brief CLANG compiler specific configuration

//
// 8 bit integer
#define XRAY_8BIT_INT_SUPPORT
#define XRAY_SI8BIT_BASE_TYPE \
    signed char
#define XRAY_UI8BIT_BASE_TYPE \
    unsigned char

//
// 16 bit integer
#define XRAY_16BIT_INT_SUPPORT
#define XRAY_SI16BIT_BASE_TYPE \
    signed short
#define XRAY_UI16BIT_BASE_TYPE \
    unsigned short

//
// 32 bit integer
#define XRAY_32BIT_INT_SUPPORT
#define XRAY_SI32BIT_BASE_TYPE \
    signed int
#define XRAY_UI32BIT_BASE_TYPE \
    unsigned int

//
// 64 bit integer
#define XRAY_64BIT_INT_SUPPORT
#define XRAY_SI64BIT_BASE_TYPE \
    signed long long
#define XRAY_UI64BIT_BASE_TYPE \
    unsigned long long

#define NOEXCEPT \
    noexcept
#define CONSTEXPR \
    constexpr

#if (__clang_major__ == 2) && (__clang_minor__ >= 9)

///! CLANG has C++11 features since version 2.9 and upwards.
#define XRAY_COMPILER_HAS_CXX11_SUPPORT

#endif /* (__clang_major__ == 2) && (__clang_minor__ >= 9) */

#if (__clang_major__ >= 3)

///! CLANG has C++11 features since version 2.9 and upwards.
#define XRAY_COMPILER_HAS_CXX11_SUPPORT

///! Support for static (compile time) assertions
#if __has_feature(cxx_static_assert)

#define XRAY_COMPILER_HAS_CXX11_STATIC_ASSERT

#endif

///! Support for generalized constant expressions
#if __has_feature(cxx_constexpr)

#define XRAY_COMPILER_HAS_CXX11_CONSTEXPR

#endif

//! Support for move semantics
#if __has_feature(cxx_implicit_moves)

#ifndef XRAY_COMPILER_HAS_CXX11_MOVE_SEMANTICS

#define XRAY_COMPILER_HAS_CXX11_MOVE_SEMANTICS

#endif /* XRAY_COMPILER_HAS_CXX11_MOVE_SEMANTICS */

#endif

#if __has_feature(cxx_variadic_templates)

///! Support for templates with variable number of arguments
#define XRAY_COMPILER_HAS_CXX11_VARIADIC_TEMPLATES

#endif

#if __has_feature(cxx_decltype)

///! Support for decltype() operator
#define XRAY_COMPILER_HAS_CXX11_DECLTYPE

#endif

#if __has_feature(cxx_strong_enums)

///! Support for strongly typed enumeration types
#define XRAY_COMPILER_HAS_CXX11_STRONGLY_TYPES_ENUMS

///! Support for forward declaration of enumerated types
#define XRAY_COMPILER_HAS_CXX11_FORWARD_DECLARED_ENUMS

#endif

#if __has_feature(cxx_nullptr)

///! Support for nullptr
#define XRAY_COMPILER_HAS_CXX11_NULLPTR

#endif

#if __has_feature(cxx_defaulted_functions) && __has_feature(cxx_deleted_functions)
///! Support for defaulted and deleted functions
#define XRAY_COMPILER_HAS_CXX11_DEFAULTED_DELETED_FUNCTIONS

#endif

#if __has_feature(cxx_lambdas)

///! Support for lambdas
#define XRAY_COMPILER_HAS_CXX11_LAMBDAS

#endif

#if __has_feature(cxx_generalized_initializers)

///! Support for initializer lists
#define XRAY_COMPILER_HAS_CXX11_INITIALIZER_LISTS

#endif

#endif /* (__clang_major__ >= 3) */

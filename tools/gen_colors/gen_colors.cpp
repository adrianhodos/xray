#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <cstdint>
#include <algorithm>
#include <string>
#include <vector>
#include <unordered_set>
#include <utility>
#include <memory>

#include "xray/xray.hpp"

using namespace std;

//          BurlyWood             = 0xdeb887,

struct fileDeleter_t {
    void operator()(FILE* fp) const NOEXCEPT {
        if (fp) {
            fclose(fp);
        }
    }
};

struct colorData_t {
    std::string     ColorName;
    xr_uint32_t     ColorValue;
};

inline bool operator==(
        const colorData_t& c0,
        const colorData_t& c1) NOEXCEPT
{
    return c0.ColorValue == c1.ColorValue;
}

inline bool operator!=(
        const colorData_t& c0,
        const colorData_t& c1) NOEXCEPT
{
    return !(c0 == c1);
}

namespace std {

    template<>
    struct hash<colorData_t> {
    public :
        size_t operator()(const colorData_t& inputColor) const NOEXCEPT {
            return hash<xr_uint32_t>()(inputColor.ColorValue);
        }
    };

} // namespace std

void
WriteColorsToFile(
        const char* fileName,
        const unordered_set<colorData_t>& uniqueColors)
{
    vector<colorData_t> sortedColors{begin(uniqueColors), end(uniqueColors)};

    std::sort(
        begin(sortedColors), 
        end(sortedColors), 
        [](const colorData_t& c0, const colorData_t& c1) NOEXCEPT {
            return c0.ColorName < c1.ColorName;
    });

    unique_ptr<FILE, fileDeleter_t> outputFile{fopen(fileName, "wt")};
    if (!outputFile) {
        return;
    }

    FILE* outFile = outputFile.get();

    fprintf(outFile, "namespace Xray {\nnamespace Math{\n\n");
    fprintf(outFile, "\t/// Standard web color values.\n");
    fprintf(outFile, "\tenum class xrStandardColors : XrUInt32 {\n");

    for_each(begin(sortedColors), end(sortedColors),
             [outFile](const colorData_t& colorToWrite) NOEXCEPT {
        fprintf(outFile,
                "\t\tk%s = 0x%08x,\n",
                colorToWrite.ColorName.c_str(),
                (colorToWrite.ColorValue << 8) | 0xFF);
    });

    fprintf(outFile, "\t};\n} // namespace Math\n} // namespace Xray");
}

int main(int argc, char** argv)
{
    if (argc != 3) {
        fputs("\nUsage is gen_colors input_file output_file", stderr);
        return 0;
    }
    
    unique_ptr<FILE, fileDeleter_t> inputFile{fopen(argv[1], "r")};
    if (!inputFile) {
        fprintf(stderr, "\nFailed to open file %s", argv[1]);
        return EXIT_FAILURE;
    }

    std::unordered_set<colorData_t> uniqueColors;
    std::vector<char> lineBuffer(1024, 0);
    std::vector<char> tempBuffer;
    tempBuffer.reserve(1024);

    while (fgets(&lineBuffer[0], static_cast<int>(lineBuffer.size()), inputFile.get())) {
        const char* p = &lineBuffer[0];
        tempBuffer.clear();
        colorData_t currentColor;

        for (; *p; ++p) {

            if (isspace(*p)) {
                continue;
            }

            if (*p == '=') {
                if (tempBuffer.empty()) {
                    break;
                }
                tempBuffer.push_back(0);
                currentColor.ColorName = static_cast<const char*>(&tempBuffer[0]);
                tempBuffer.clear();
                continue;
            }

            if (isalnum(*p)) {
                tempBuffer.push_back(*p);
                continue;
            }

            if (*p == '\n' || *p == ',') {
                if (tempBuffer.empty()) {
                    break;
                }

                tempBuffer.push_back(0);
                currentColor.ColorValue = 
                    static_cast<xr_uint32_t>(strtoul(&tempBuffer[0], nullptr, 16));
                uniqueColors.insert(currentColor);
                break;
            }
        }
    }

    WriteColorsToFile(argv[2], uniqueColors);
    return 0;
}

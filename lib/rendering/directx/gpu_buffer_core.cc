#include "xray/rendering/directx/resource_usage.hpp"
#include "xray/rendering/directx/renderer_directx.hpp"
#include "xray/rendering/directx/gpu_buffer_core.hpp"
#include "xray/base/shims/pointer/scoped_pointer.hpp"

using namespace xray::base;

namespace {

const D3D11_BIND_FLAG k_dx11_bind_flag_mappings[] = {
    D3D11_BIND_VERTEX_BUFFER,
    D3D11_BIND_INDEX_BUFFER,
    D3D11_BIND_CONSTANT_BUFFER
};

} // anonymous namespace

bool 
xray::rendering::dx_detail::gpu_buffer_core::initialize(
    const renderer_directx& rsys,
    const xr_int32_t        type,
    const xr_size_t         k_num_elements,
    const xr_size_t         k_element_size,
    const resource_usage    usage_type,
    const void*             k_initial_data) NOEXCEPT
{
    assert(!buffer_handle_);
    assert(type >= 0 && type < 3);
    assert(k_num_elements > 0);
    assert(k_element_size > 0);

    e_count_ = static_cast<xr_uint32_t>(k_num_elements);
    e_size_ = static_cast<xr_uint32_t>(k_element_size);

    D3D11_BUFFER_DESC buffer_description;
    buffer_description.BindFlags = k_dx11_bind_flag_mappings[type];
    buffer_description.MiscFlags = 0;
    buffer_description.ByteWidth = e_count_ * e_size_;
    buffer_description.StructureByteStride = e_size_;

#if defined(_DEBUG)
    //
    //  When creating a constant buffer, its size must be a multiple of 16.
    if (buffer_description.BindFlags == D3D11_BIND_CONSTANT_BUFFER) {
        assert((buffer_description.ByteWidth % 16) == 0);
    }

#endif

    D3D11_SUBRESOURCE_DATA buff_init_data;
    D3D11_SUBRESOURCE_DATA* ptr_init_data = nullptr;

    switch (usage_type) {
    case resource_usage::immutable :
        buffer_description.Usage = D3D11_USAGE_IMMUTABLE;
        buffer_description.CPUAccessFlags = 0;
        ptr_init_data = &buff_init_data;
        assert(k_initial_data != nullptr);
        break;

    case resource_usage::cpu_writable :
        buffer_description.Usage = D3D11_USAGE_DYNAMIC;
        buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        ptr_init_data = k_initial_data ? &buff_init_data : nullptr;
        break;

    case resource_usage::gpu_to_cpu_copy :
        buffer_description.Usage = D3D11_USAGE_STAGING;
        buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
        assert(k_initial_data == nullptr);
        break;

    case resource_usage::gpu_writable :
        buffer_description.Usage = D3D11_USAGE_DEFAULT;
        buffer_description.CPUAccessFlags = 0;
        assert(k_initial_data == nullptr);
        break;

    default :
        assert(false);
        return false;
    }

    if (ptr_init_data) {
        if (k_initial_data == nullptr)
            return false;

        ptr_init_data->pSysMem = k_initial_data;
        ptr_init_data->SysMemPitch = buffer_description.ByteWidth;
        ptr_init_data->SysMemSlicePitch = 0;
    }

    const HRESULT ret_code = rsys.device()->CreateBuffer(
        &buffer_description, ptr_init_data, raw_ptr_ptr(buffer_handle_));
    
    //D3D11_SUBRESOURCE_DATA buff_data = {k_initial_data, 0, 0};
    //HRESULT ret_code;
    //ID3D11Device* k_device = rsys->internal_np_get_device();

    //CHECK_D3D(
    //    &ret_code,
    //    k_device->CreateBuffer(&buffer_desc, &buff_data,
    //    v8::base::scoped_pointer_get_impl(m_buff)));

    return SUCCEEDED(ret_code);
}

//void v8::directx::gpu_buffer_core::set_data(
//    renderer* rsys, const void* data, const xr_size_t byte_count
//    ) {
//    assert(m_buff);
//    assert(byte_count <= (e_count_ * e_size_));
//
//    ID3D11DeviceContext* k_devcontext = rsys->internal_np_get_device_context();
//
//    scoped_resource_mapping data_store(
//        k_devcontext, v8::base::scoped_pointer_get(m_buff), D3D11_MAP_WRITE_DISCARD);
//
//    if (!data_store) {
//        return;
//    }
//
//    memcpy(data_store.get_data_pointer(), data, byte_count);
//}
//
//void v8::directx::gpu_buffer_core::get_data(
//    renderer* rsys, const xr_size_t byte_count, void* output_buffer
//    ) {
//    assert(m_buff);
//    assert(byte_count <= (e_count_ * e_size_));
//
//    ID3D11DeviceContext* k_devcontext = rsys->internal_np_get_device_context();
//
//    scoped_resource_mapping data_store(
//        k_devcontext, v8::base::scoped_pointer_get(m_buff), D3D11_MAP_READ);
//
//    if (data_store) {
//        memcpy(output_buffer, data_store.get_data_pointer(), byte_count);
//    }
//}
//
//void v8::directx::gpu_buffer_core::bind_to_pipeline_impl(
//    renderer*                           rsys,
//    gpu_buffer_core*                    buff,
//    const xr_uint32_t                   k_offset,
//    const dx11_vertexbuffer_traits&
//    ) {
//    assert(rsys);
//    assert(buff);
//
//    ID3D11DeviceContext* k_devcontext = rsys->internal_np_get_device_context();
//    ID3D11Buffer* k_vbuff = buff->internal_np_get_handle();
//    const UINT k_strides = buff->get_element_size();
//    k_devcontext->IASetVertexBuffers(0, 1, &k_vbuff, &k_strides, &k_offset);
//}
//
//void v8::directx::gpu_buffer_core::bind_to_pipeline_impl(
//    renderer*                  rsys,
//    gpu_buffer_core*                    buff,
//    const xr_uint32_t                   k_offset,
//    const dx11_indexbuffer_traits&
//    ) {
//    assert(rsys);
//    assert(buff);
//    assert((buff->get_element_size() == sizeof(v8_uint16_t))
//            || (buff->get_element_size() == sizeof(xr_uint32_t)));
//
//    const DXGI_FORMAT k_index_format[] = {
//        DXGI_FORMAT_R16_UINT, DXGI_FORMAT_R32_UINT
//    };
//
//    const v8_int_t k_typeidx = buff->get_element_size() == sizeof(xr_uint32_t);
//    ID3D11DeviceContext* k_devctx = rsys->internal_np_get_device_context();
//    ID3D11Buffer* k_ibuff = buff->internal_np_get_handle();
//    k_devctx->IASetIndexBuffer(k_ibuff, k_index_format[k_typeidx], k_offset);
//}

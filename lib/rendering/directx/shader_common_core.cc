#include <cassert>
#include <algorithm>
#include <vector>

#include <d3dcompiler.h>
#include <d3d11shader.h>

#include "xray/base/windows/com_pointer.hpp"
#include "xray/base/shims/pointer/scoped_pointer.hpp"
#include "xray/rendering/directx/debug_helpers.hpp"

#include "xray/rendering/directx/shader_common_core.hpp"

using namespace xray::base;
using namespace xray::rendering;
using namespace xray::rendering::dx_detail;

namespace {

const DXGI_FORMAT k_comp_type_to_dxgi_map[4][3] = {
    {DXGI_FORMAT_R32_UINT, DXGI_FORMAT_R32_SINT, DXGI_FORMAT_R32_FLOAT},
    {DXGI_FORMAT_R32G32_UINT, DXGI_FORMAT_R32G32_SINT, DXGI_FORMAT_R32G32_FLOAT},
    {DXGI_FORMAT_R32G32B32_UINT, DXGI_FORMAT_R32G32B32_SINT, DXGI_FORMAT_R32G32B32_FLOAT},
    {DXGI_FORMAT_R32G32B32A32_UINT, DXGI_FORMAT_R32G32B32A32_SINT, DXGI_FORMAT_R32G32B32A32_FLOAT}
};

bool
map_param_description_to_element_description(
    const D3D11_SIGNATURE_PARAMETER_DESC&   input_parm_signature,
    D3D11_INPUT_ELEMENT_DESC*               element_desc) NOEXCEPT 
{
    assert(element_desc);

    element_desc->AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
    element_desc->InputSlot = 0;
    element_desc->InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    element_desc->InstanceDataStepRate = 0;
    element_desc->SemanticIndex = input_parm_signature.SemanticIndex;
    element_desc->SemanticName = input_parm_signature.SemanticName;

    const D3D_REGISTER_COMPONENT_TYPE k_comp_type = input_parm_signature.ComponentType;
    if (input_parm_signature.Mask == 0x01) {
        //
        // single register is used
        element_desc->Format = k_comp_type_to_dxgi_map[0][k_comp_type - 1];
    } else if (input_parm_signature.Mask == 0x03) {
        //
        // Two registers
        element_desc->Format = k_comp_type_to_dxgi_map[1][k_comp_type - 1];
    } else if (input_parm_signature.Mask == 0x07) {
        //
        // Three registers used
        element_desc->Format = k_comp_type_to_dxgi_map[2][k_comp_type - 1];
    } else if (input_parm_signature.Mask == 0x0F) {
        //
        // Four registers used
        element_desc->Format = k_comp_type_to_dxgi_map[3][k_comp_type - 1];
    } else {
        //
        // Should not get here
        assert(false && "Unsupported number of registers for component!");
        return false;
    }

    return true;
}

///
/// \brief Contains data for a constant buffer used in a shader.
struct ConstBufferInfo_t {
    ///< Pointer to reflector interface.
    ID3D11ShaderReflectionConstantBuffer*   cbuff_reflect;
    ///< Info about the constant buffer.
    D3D11_SHADER_BUFFER_DESC                cbuff_desc;
    ///< Register (binding point) in the shader.
    xr_uint32_t                             cbuff_idx;
};

///
/// \brief Collects information about the constant buffers used by a shader.
/// \param device Pointer to a ID3D11Device interface.
/// \param shader_ref Pointer to a ID3D11ShaderReflection interface.
/// \param num_buffs Number of constant buffers in the shader's code.
/// \param shader_cbuff_data When the function returns, will contain data
/// on each of the constant buffers in the shader's code.
/// \return Total number of global variables (uniforms) in the shader.
xr_uint32_t 
reflect_shader_const_buffers(
    ID3D11Device*                       device,
    ID3D11ShaderReflection*             shader_ref,
    const xr_uint32_t                   num_buffs,
    std::vector<ConstBufferInfo_t>*     shader_cbuff_data) NOEXCEPT 
{
    assert(device);
    assert(shader_ref);
    assert(shader_cbuff_data);

    shader_cbuff_data->clear();
    shader_cbuff_data->reserve(num_buffs);

    xr_uint32_t         num_global_vars = 0;
    ConstBufferInfo_t   cbuff_info;

    for (xr_uint32_t idx = 0; idx < num_buffs; ++idx) {
        cbuff_info.cbuff_reflect = shader_ref->GetConstantBufferByIndex(idx);
        if (!cbuff_info.cbuff_reflect) {
            //OUTPUT_DBG_MSGA("Warning : failed to reflect constant buffer @ slot %ud",
            //                idx);
            continue;
        }

        cbuff_info.cbuff_idx = idx;

        HRESULT ret_code;
        XRAY_WRAP_D3DCALL(
            &ret_code,
            cbuff_info.cbuff_reflect->GetDesc(&cbuff_info.cbuff_desc));

        if (FAILED(ret_code)) {
            continue;
        }

        num_global_vars += cbuff_info.cbuff_desc.Variables;
        shader_cbuff_data->push_back(cbuff_info);
    }
    return num_global_vars;
}

enum class reflect_resource_result {
    fail,
    sampler,
    texture
};

reflect_resource_result 
reflect_shader_bound_resource(
    ID3D11ShaderReflection*             shader_reflector,
    const xr_uint32_t                   index,
    sampler_state_t*                    sampler_state_data,
    resource_view_t*                    resource_view_data) NOEXCEPT
{
    assert(shader_reflector != nullptr);

    D3D11_SHADER_INPUT_BIND_DESC bound_res_desc;
    HRESULT ret_code;

    XRAY_WRAP_D3DCALL(
        &ret_code, 
        shader_reflector->GetResourceBindingDesc(index, &bound_res_desc));

    if (FAILED(ret_code))
        return reflect_resource_result::fail;

    if (bound_res_desc.Type == D3D10_SIT_SAMPLER) {
        assert(sampler_state_data != nullptr);
        sampler_state_data->name_ = bound_res_desc.Name;
        sampler_state_data->bindpoint_ = bound_res_desc.BindPoint;
        return reflect_resource_result::sampler;
    }

    if (bound_res_desc.Type == D3D10_SIT_TEXTURE) {
        assert(resource_view_data != nullptr);
        resource_view_data->rvt_name = bound_res_desc.Name;
        resource_view_data->rvt_bindpoint = bound_res_desc.BindPoint;
        resource_view_data->rvt_dimension = bound_res_desc.Dimension;
        resource_view_data->rvt_count = bound_res_desc.BindCount;
        return reflect_resource_result::sampler;
    } 

    return reflect_resource_result::fail;
}

ID3D11InputLayout* 
get_shader_input_signature(
    ID3D11Device*                       k_device,
    ID3D11ShaderReflection*             k_reflector,
    ID3D10Blob*                         k_bytecode,
    const xr_uint32_t                   k_num_input_params)
{
    assert(k_device);
    assert(k_reflector);
    assert(k_bytecode);

    std::vector<D3D11_INPUT_ELEMENT_DESC> input_elem_desc;
    input_elem_desc.reserve(k_num_input_params);

    for (xr_uint32_t param_idx = 0; param_idx < k_num_input_params; ++param_idx) {
        D3D11_INPUT_ELEMENT_DESC        curr_elem_desc;
        D3D11_SIGNATURE_PARAMETER_DESC  curr_param_desc;
        HRESULT                         ret_code;

        XRAY_WRAP_D3DCALL(
            &ret_code,
            k_reflector->GetInputParameterDesc(param_idx, &curr_param_desc));

        if (FAILED(ret_code)) {
            break;
        }

        const bool translation_succeeded = map_param_description_to_element_description(
            curr_param_desc, &curr_elem_desc);

        if (!translation_succeeded) {
            break;
        }

        input_elem_desc.push_back(curr_elem_desc);
    }

    if (input_elem_desc.empty()) {
        return nullptr;
    }

    HRESULT ret_code;
    ID3D11InputLayout* input_layout = nullptr;
    XRAY_WRAP_D3DCALL(
        &ret_code,
        k_device->CreateInputLayout(
            &input_elem_desc[0], static_cast<UINT>(input_elem_desc.size()),
            k_bytecode->GetBufferPointer(), k_bytecode->GetBufferSize(),
            &input_layout));

    return input_layout;
}

} // anonymous namespace

bool 
xray::rendering::dx_detail::shader_common_core::reflect_shader(
    ID3D10Blob*         compiled_bytecode, 
    ID3D11Device*       device)
{
    assert(compiled_bytecode != nullptr);
    assert(device != nullptr);
    assert(!bytecode);
    assert(!input_signature);

    bytecode = compiled_bytecode;

    com_scoped_pointer<ID3D11ShaderReflection> shader_reflector;
    HRESULT ret_code;

    XRAY_WRAP_D3DCALL(
        &ret_code,
        D3DReflect(compiled_bytecode->GetBufferPointer(),
                   compiled_bytecode->GetBufferSize(),
                   IID_ID3D11ShaderReflection,
                   reinterpret_cast<void**>(raw_ptr_ptr(shader_reflector))));

    if (FAILED(ret_code)) {
        return false;
    }

    D3D11_SHADER_DESC shader_description;
    XRAY_WRAP_D3DCALL(&ret_code, shader_reflector->GetDesc(&shader_description));

    if (FAILED(ret_code)) {
        return false;
    }

    //
    // Collect info about the constant buffers used by the shader.
    std::vector<ConstBufferInfo_t> shader_cbuff_data;
    const xr_uint32_t num_global_vars = reflect_shader_const_buffers(
        device, raw_ptr(shader_reflector), shader_description.ConstantBuffers,
        &shader_cbuff_data);

    UNREFERENCED_PARAMETER(num_global_vars);

    //
    // Need to reserve space in advance, otherwise iterators might become invalid.
    // TODO : rewrite, I don't like this.
    uniform_blocks.reserve(shader_cbuff_data.size());

    for (xr_size_t idx = 0; idx < shader_cbuff_data.size(); ++idx) {
        const ConstBufferInfo_t& cb_info = shader_cbuff_data[idx];

        //
        // Each constant buffer in the shader is represented by a
        // shader_uniform_block_t object.
        scoped_pointer<shader_uniform_block> uniform_blk{new shader_uniform_block{}};
        if (!uniform_blk->initialize(device, cb_info.cbuff_desc, cb_info.cbuff_idx)) {
            //OUTPUT_DBG_MSGA("Warning : failed to initialize uniform block for "
            //                "constant buffer with index %u", cb_info.cbuff_idx);
            continue;
        }

        uniform_blocks.push_back(scoped_pointer_release(uniform_blk));
        uniform_block_binding_list.push_back(uniform_blocks.back()->get_handle());

        //
        // Reflect each global variable defined in the constant buffer.
        for (xr_uint32_t var_idx = 0; var_idx < cb_info.cbuff_desc.Variables; ++var_idx) {
            auto var_reflect = cb_info.cbuff_reflect->GetVariableByIndex(var_idx);

            if (!var_reflect) {
                //OUTPUT_DBG_MSGA("Warning : failed to reflect variable @ %ud "
                //                "for constant buffer %s",
                //                var_idx, cb_info.cbuff_desc.Name);
                continue;
            }

            D3D11_SHADER_VARIABLE_DESC  var_description;
            HRESULT                     ret_code;
            XRAY_WRAP_D3DCALL(&ret_code, var_reflect->GetDesc(&var_description));

            if (FAILED(ret_code)) {
                continue;
            }

            //
            // Each global variable is represented by an object of type
            // shader_uniform.
            uniforms.push_back({var_description, uniform_blocks.back()});
        }
    }

    //
    // Reflect any other resources the shader uses (samplers, textures).
    sampler_state_t smp_data;
    resource_view_t rvt_data;
    for (xr_uint32_t idx = 0; idx < shader_description.BoundResources; ++idx) {
        const reflect_resource_result ret_code = reflect_shader_bound_resource(
            raw_ptr(shader_reflector), idx, &smp_data, &rvt_data);

        if (ret_code == reflect_resource_result::sampler) {
            sampler_states.push_back(std::move(smp_data));
            continue;
        }

        if (ret_code == reflect_resource_result::texture) {
            resource_views.push_back(std::move(rvt_data));
        }
    }

    //
    // Obtain input parameter signature for the shader.
    input_signature = get_shader_input_signature(
        device, raw_ptr(shader_reflector), compiled_bytecode, 
        shader_description.InputParameters);

    //
    // 
    // Sort all resources by name, so we can binary search them.
    std::sort(
        std::begin(uniform_blocks),
        std::end(uniform_blocks),
        [](const shader_uniform_block* left, const shader_uniform_block* right) {
        return left->BlockName < right->BlockName;
    });

    std::sort(
        std::begin(uniforms),
        std::end(uniforms),
        [](const shader_uniform& lhs, const shader_uniform& rhs) {
        return lhs.Name < rhs.Name;
    });

    std::sort(
        std::begin(sampler_states),
        std::end(sampler_states),
        [](const sampler_state_t& lhs, const sampler_state_t& rhs) {
        return lhs.name_ < rhs.name_;
    });

    std::sort(
        std::begin(resource_views),
        std::end(resource_views),
        [](const resource_view_t& lhs, const resource_view_t& rhs) {
        return lhs.rvt_name < rhs.rvt_name;
    });

    sampler_state_binding_list.resize(sampler_states.size());
    resource_view_binding_list.resize(resource_views.size());
    return true;
}

xray::rendering::dx_detail::shader_common_core& 
xray::rendering::dx_detail::shader_common_core::operator=(shader_common_core&& rrhs) {
    bytecode = std::move(rrhs.bytecode);
    input_signature = std::move(rrhs.input_signature);
    uniform_blocks = std::move(rrhs.uniform_blocks);
    uniform_block_binding_list = std::move(rrhs.uniform_block_binding_list);
    uniforms = std::move(rrhs.uniforms);
    sampler_states = std::move(rrhs.sampler_states);
    sampler_state_binding_list = std::move(rrhs.sampler_state_binding_list);
    resource_views = std::move(rrhs.resource_views);
    resource_view_binding_list = std::move(rrhs.resource_view_binding_list);

    return *this;
}

xray::rendering::dx_detail::shader_uniform_block*
xray::rendering::dx_detail::shader_common_core::get_uniform_block_by_name(
    const char* block_name) NOEXCEPT
{
    auto iter = binary_search_container(
    uniform_blocks,
    [block_name](const shader_uniform_block* ublk) {
        return strcmp(block_name, ublk->BlockName.c_str());
    });

    if (iter == std::end(uniform_blocks)) {
        return nullptr;
    }

    return *iter;
}

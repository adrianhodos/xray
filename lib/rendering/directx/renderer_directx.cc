#include <cstdio>
#include <cstdlib>
#include <string>

#include <d3dcompiler.h>

#include "xray/base/array_dimension.hpp"
#include "xray/base/scoped_pointer.hpp"
#include "xray/base/nothing.hpp"
#include "xray/base/shims/pointer/scoped_pointer.hpp"
#include "xray/base/shims/string/std_string.hpp"
#include "xray/rendering/directx/resource_usage.hpp"
#include "xray/rendering/directx/renderer_directx.hpp"

using namespace xray::base;

namespace {

const char* const shader_profiles[] = {
    "vs_4_0",
    "vs_4_1",
    "vs_5_0",
    "ps_4_0",
    "ps_4_1",
    "ps_5_0"
};

template<typename T>
struct crt_file_deleter {
    static void dispose(FILE* fp) NOEXCEPT {
        if (fp)
        fclose(fp);
    }

    enum {
        is_array_ptr = 0
    };
};

void
load_file_contents(const char* file_name, std::string* file_text) {
    scoped_pointer<FILE, crt_file_deleter> file_handle{fopen(file_name, "rt")};

    if (!file_handle)
        return;

    file_text->clear();
    file_text->reserve(2048);

    char tmp_buff[1024];

    while (fgets(tmp_buff, static_cast<int>(xr_array_size__(tmp_buff)), raw_ptr(file_handle))) {
        file_text->append(tmp_buff);
    }
}

} // anonymous namespace

namespace xray {
namespace rendering {


shader_bytecode_handle
compile_shader_from_file(
    const char*                             file_name,
    const char*                             entry_point,
    const shader_profile                    target_profile,
    const xr_uint32_t                       compile_flags,
    const D3D_SHADER_MACRO*                 compile_defines)
{
    assert(file_name != nullptr);
    assert(entry_point != nullptr);

    std::string shader_source_code;
    load_file_contents(file_name, &shader_source_code);

    if (shader_source_code.empty())
        return nullptr;

    return compile_shader_from_memory(plain_str(shader_source_code),
                                      shader_source_code.length(),
                                      entry_point,
                                      target_profile,
                                      compile_flags,
                                      compile_defines);
}

shader_bytecode_handle 
compile_shader_from_memory(
    const char*                             shader_source_code,
    const xr_size_t                         source_code_length,
    const char*                             entry_point,
    const shader_profile                    target_profile,
    const xr_uint32_t                       compile_flags,
    const D3D_SHADER_MACRO*                 compile_defines)  
{
    com_scoped_pointer<ID3D10Blob> err_msg;
    ID3D10Blob* compiled_bytecode = nullptr;

    const HRESULT ret_code = D3DCompile(shader_source_code,
                                        source_code_length,
                                        nullptr,
                                        compile_defines,
                                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                                        entry_point,
                                        shader_profiles[(int)target_profile],
                                        compile_flags,
                                        0u,
                                        &compiled_bytecode,
                                        raw_ptr_ptr(err_msg));

    if (SUCCEEDED(ret_code))
        return compiled_bytecode;

    if (err_msg && err_msg->GetBufferPointer()) {
        const char* error_text = static_cast<const char*>(err_msg->GetBufferPointer());
        OutputDebugString(error_text);
    }

    return nullptr;
}

renderer_directx::renderer_directx() {
    const UINT create_flags = D3D11_CREATE_DEVICE_DEBUG;
    D3D_FEATURE_LEVEL max_feat_lvl;
    D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 
                      create_flags, nullptr, 0, D3D11_SDK_VERSION,
                      raw_ptr_ptr(dx_device_), &max_feat_lvl,
                      raw_ptr_ptr(devcontext_));
}

void
renderer_directx::clear() const NOEXCEPT {
    if (!is_valid())
        return;

    devcontext_->ClearRenderTargetView(raw_ptr(render_target_view_),
                                       clear_color_.Elements);

    devcontext_->ClearDepthStencilView(raw_ptr(depth_stencil_view_),
                                       D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
                                       1.0f,
                                       0xff);
}

void
renderer_directx::handle_resize(const xr_int32_t, const xr_int32_t) NOEXCEPT{
    if (!is_valid())
        return;

    devcontext_->OMSetRenderTargets(0, nullptr, nullptr);

    render_target_view_ = nullptr;
    depth_stencil_view_ = nullptr;
    depth_stencil_texture_ = nullptr;
}

void
renderer_directx::restore_after_resize(
    ID3D11Texture2D *backbuffer_texture) NOEXCEPT
{
    assert(backbuffer_texture);

    D3D11_TEXTURE2D_DESC texture_desc;
    backbuffer_texture->GetDesc(&texture_desc);

    HRESULT ret_code = dx_device_->CreateRenderTargetView(
        backbuffer_texture, nullptr, raw_ptr_ptr(render_target_view_));

    if (FAILED(ret_code)) {
        return;
    }

    D3D11_TEXTURE2D_DESC tex_stencil_desc;
    tex_stencil_desc.Width = texture_desc.Width;
    tex_stencil_desc.Height = texture_desc.Height;
    tex_stencil_desc.CPUAccessFlags = 0;
    tex_stencil_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    tex_stencil_desc.MiscFlags = 0;
    tex_stencil_desc.Usage = D3D11_USAGE_DEFAULT;
    tex_stencil_desc.SampleDesc.Count = 1;
    tex_stencil_desc.SampleDesc.Quality = 0;
    tex_stencil_desc.ArraySize = 1;
    tex_stencil_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    tex_stencil_desc.MipLevels = 0;

    ret_code = dx_device_->CreateTexture2D(&tex_stencil_desc,
                                           nullptr,
                                           raw_ptr_ptr(depth_stencil_texture_));

    if (FAILED(ret_code))
        return;

    ret_code = dx_device_->CreateDepthStencilView(raw_ptr(depth_stencil_texture_),
                                                  nullptr,
                                                  raw_ptr_ptr(depth_stencil_view_));

    ID3D11RenderTargetView* const bound_render_targets[] = { raw_ptr(render_target_view_) };
    devcontext_->OMSetRenderTargets(xr_array_size__(bound_render_targets),
                                    bound_render_targets,
                                    raw_ptr(depth_stencil_view_));

    D3D11_VIEWPORT viewport;
    viewport.TopLeftX = viewport.TopLeftY = 0.0f;
    viewport.Width = static_cast<float>(texture_desc.Width);
    viewport.Height = static_cast<float>(texture_desc.Height);
    viewport.MinDepth = 0.0f;
    viewport.MaxDepth = 1.0f;

    devcontext_->RSSetViewports(1, &viewport);
}

ID3D11Buffer* renderer_directx::make_vertex_buffer(
    const xr_size_t                     element_count,
    const xr_size_t                     element_size,
    const resource_usage                usage_type, 
    const void*                         initial_data) const NOEXCEPT
{
    assert(is_valid());

    D3D11_BUFFER_DESC buffer_description;
    buffer_description.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    buffer_description.ByteWidth = static_cast<UINT>(element_count * element_size);
    buffer_description.MiscFlags = 0;
    buffer_description.StructureByteStride = static_cast<UINT>(element_size);

    D3D11_SUBRESOURCE_DATA buffer_init_data;
    D3D11_SUBRESOURCE_DATA* ptr_init_data = nullptr;

    switch (usage_type) {
    case resource_usage::immutable :
        buffer_description.Usage = D3D11_USAGE_IMMUTABLE;
        buffer_description.CPUAccessFlags = 0;
        ptr_init_data = &buffer_init_data;
        break;

    case resource_usage::cpu_writable :
        buffer_description.Usage = D3D11_USAGE_DYNAMIC;
        buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        ptr_init_data = initial_data ? &buffer_init_data : nullptr;
        break;

    case resource_usage::gpu_writable :
        buffer_description.Usage = D3D11_USAGE_DEFAULT;
        buffer_description.CPUAccessFlags = 0;
        break;

    case resource_usage::gpu_to_cpu_copy :
        buffer_description.Usage = D3D11_USAGE_STAGING;
        buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
        break;

    default :
        return nullptr;
    }

    if (ptr_init_data) {
        if (!initial_data)
            return nullptr;

        ptr_init_data->pSysMem = initial_data;
        ptr_init_data->SysMemSlicePitch = 0;
        ptr_init_data->SysMemPitch = buffer_description.ByteWidth;
    }

    ID3D11Buffer* vertex_buffer = nullptr;
    dx_device_->CreateBuffer(&buffer_description, ptr_init_data, &vertex_buffer);

    return vertex_buffer;
}

ID3D11Buffer*
renderer_directx::make_index_buffer(
    const xr_size_t                     element_count,
    const xr_size_t                     element_size,
    const resource_usage                usage_type,
    const void*                         initial_data) const NOEXCEPT
{
    assert(is_valid());
    assert(element_size <= sizeof(DWORD));

    D3D11_BUFFER_DESC buffer_description;
    buffer_description.BindFlags = D3D11_BIND_INDEX_BUFFER;
    buffer_description.ByteWidth = static_cast<UINT>(element_count * element_size);
    buffer_description.MiscFlags = 0;
    buffer_description.StructureByteStride = static_cast<UINT>(element_size);

    D3D11_SUBRESOURCE_DATA buffer_init_data;
    D3D11_SUBRESOURCE_DATA* ptr_init_data = nullptr;

    switch (usage_type) {
    case resource_usage::cpu_writable :
        buffer_description.Usage = D3D11_USAGE_DYNAMIC;
        buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        ptr_init_data = initial_data ? &buffer_init_data : nullptr;
        break;

    case resource_usage::immutable :
        buffer_description.Usage = D3D11_USAGE_IMMUTABLE;
        buffer_description.CPUAccessFlags = 0;
        ptr_init_data = &buffer_init_data;
        break;

    default :
        return nullptr;
    }

    if (ptr_init_data) {
        if (!initial_data)
            return nullptr;

        ptr_init_data->pSysMem = initial_data;
        ptr_init_data->SysMemPitch = buffer_description.ByteWidth;
        ptr_init_data->SysMemSlicePitch = 0;
    }

    ID3D11Buffer* index_buffer = nullptr;
    dx_device_->CreateBuffer(&buffer_description, ptr_init_data, &index_buffer);

    return index_buffer;
}

ID3D11Buffer*
renderer_directx::make_constant_buffer(
    const xr_size_t                     byte_size,
    const resource_usage                usage_type,
    const void*                         initial_data) const NOEXCEPT
{
    assert(is_valid());
    assert((byte_size % 16) == 0);

    D3D11_BUFFER_DESC buffer_description;
    buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    buffer_description.ByteWidth = static_cast<UINT>(byte_size);
    buffer_description.MiscFlags = 0;
    buffer_description.StructureByteStride = 0;

    D3D11_SUBRESOURCE_DATA buffer_init_data;
    D3D11_SUBRESOURCE_DATA* ptr_init_data = nullptr;

    switch (usage_type) {
    case resource_usage::immutable :
        buffer_description.Usage = D3D11_USAGE_IMMUTABLE;
        buffer_description.CPUAccessFlags = 0;
        ptr_init_data = &buffer_init_data;
        break;

    case resource_usage::cpu_writable :
        buffer_description.Usage = D3D11_USAGE_STAGING;
        buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        ptr_init_data = initial_data ? &buffer_init_data : nullptr;
        break;

    case resource_usage::gpu_writable :
        buffer_description.Usage = D3D11_USAGE_DEFAULT;
        buffer_description.CPUAccessFlags = 0;
        break;

    default :
        return nullptr;
    }

    if (ptr_init_data) {
        if (!initial_data)
            return nullptr;

        ptr_init_data->pSysMem = initial_data;
        ptr_init_data->SysMemPitch = buffer_description.ByteWidth;
        ptr_init_data->SysMemSlicePitch = 0;
    }

    ID3D11Buffer* constant_buffer = nullptr;
    dx_device_->CreateBuffer(&buffer_description, ptr_init_data, &constant_buffer);

    return constant_buffer;
}

} // namespace rendering
} // namespace xray

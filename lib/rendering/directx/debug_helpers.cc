#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <Windows.h>

#include "xray/base/array_dimension.hpp"
#include "xray/rendering/directx/debug_helpers.hpp"

void
xray::rendering::dx_detail::dx_output_debug_string(
    const wchar_t*                  file,
    const xr_int32_t                line,
    const wchar_t*                  format_string,
    ...) 
{
    wchar_t tmp_buff[2048];
    _snwprintf_s(tmp_buff, xr_array_size__(tmp_buff), L"\n[File : %s, Line %d]\n", 
                 file, line); 
    OutputDebugStringW(tmp_buff);

    va_list args_ptr;
    va_start(args_ptr, format_string);
    _vsnwprintf_s(tmp_buff, xr_array_size__(tmp_buff), format_string, args_ptr);
    va_end(args_ptr);

    OutputDebugStringW(tmp_buff);
}
#include <algorithm>
#include <unordered_map>
#include <vector>

#include <windows.h>
#include <wincodec.h>
#include <d2d1.h>
#include <d2d1helper.h>

#include "Xray/Xray.hpp"
#include "Xray/Base/ArrayDimension.hpp"
#include "Xray/Base/ScopedPointer.hpp"
#include "Xray/Base/PointerPolicy/ComStorage.hpp"
#include "Xray/Base/Shims/Pointer/ScopedPointer.hpp"
#include "Xray/Base/Shims/Pointer/RawPointer.hpp"
#include "Xray/Math/Color.hpp"
#include "Xray/Math/StandardColors.hpp"
#include "Xray/Drawing/Direct2DDrawingBackend.hpp"

using namespace Xray::Base;
using namespace Xray::Math;

template<typename T>
using COMScopedPointer = Xray::Base::ScopedPointer<T, Xray::Base::ComStorage, Xray::Base::AssertCheck>;

D2D1_POINT_2F
XrayPointToDirect2DPoint(const xrVector2F& inputPoint) NOEXCEPT
{
    return D2D1::Point2F(inputPoint.X, inputPoint.Y);
}

D2D1_COLOR_F
XrayColorToDirect2DColor(const xrRGBColor& inputColor) NOEXCEPT
{
    return D2D1::ColorF(inputColor.Red, inputColor.Green, inputColor.Blue);
}

void
XrayCircleToDirect2DEllipse(
        const xrCircle2F& inputCircle,
        D2D1_ELLIPSE* outputCircle) NOEXCEPT
{
    outputCircle->point = XrayPointToDirect2DPoint(inputCircle.Center);
    outputCircle->radiusX = outputCircle->radiusY = inputCircle.Radius;
}

struct Xray::Drawing::Direct2DDrawingBackend::ImplementationDetails {
    typedef std::unordered_map<XrUInt32, ID2D1SolidColorBrush*> ColorBrushTable_t;

    COMScopedPointer<IWICImagingFactory>            BitmapFactory;
    COMScopedPointer<ID2D1Factory>                  Factory;
    COMScopedPointer<IWICBitmap>                    RenderTargetBackingStore;
    COMScopedPointer<ID2D1BitmapRenderTarget>       RenderTarget;
    ColorBrushTable_t                               BrushTable;
    std::vector<XrSByte>                            PixelsArray;
    DrawingBackendParams_t                          Params;
    bool                                            CanvasPixelsValid { false };


    ImplementationDetails(const DrawingBackendParams_t& initParams)
        : Params(initParams)
    {
        Initialize();
    }

    ~ImplementationDetails() {
        DiscardDeviceDependentResources();
    }

    void DiscardDeviceDependentResources() {
        std::for_each(std::begin(BrushTable),
                      std::end(BrushTable),
                      [](ColorBrushTable_t::value_type& tableEntry) {
            tableEntry.second->Release();
        });
        BrushTable.clear();
        RenderTarget = nullptr;
    }

    bool Initialize();

    bool InitializeDeviceDependentResurces();

    bool Initialized() const NOEXCEPT {
        return RenderTarget != nullptr;
    }

    ID2D1SolidColorBrush* GetBrushForColor(const xrRGBColor& rgbColor) {
        auto itrTable = BrushTable.find(rgbColor.ConvertToRGBA());
        if (itrTable != std::end(BrushTable)) {
            return itrTable->second;
        }

        ID2D1SolidColorBrush* newBrush { nullptr };
        RenderTarget->CreateSolidColorBrush(XrayColorToDirect2DColor(rgbColor),
                                            D2D1::BrushProperties(),
                                            raw_ptr_ptr(newBrush));
        BrushTable.insert(std::make_pair(rgbColor.ConvertToRGBA(), newBrush));
        return newBrush;
    }
};

bool Xray::Drawing::Direct2DDrawingBackend::ImplementationDetails::Initialize()
{
    HRESULT retCode = CoCreateInstance(CLSID_WICImagingFactory,
                                       nullptr,
                                       CLSCTX_INPROC_SERVER,
                                       __uuidof(IWICImagingFactory),
                                       reinterpret_cast<void**>(raw_ptr_ptr(BitmapFactory)));

    if (FAILED(retCode)) {
        return false;
    }

    retCode = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED,
                                raw_ptr_ptr(Factory));

    if (FAILED(retCode)) {
        return false;
    }

    retCode = BitmapFactory->CreateBitmap(Params.CanvasWidth,
                                          Params.CanvasHeight,
                                          GUID_WICPixelFormat32bppBGR,
                                          WICBitmapCacheOnLoad,
                                          raw_ptr_ptr(RenderTargetBackingStore));

    if (FAILED(retCode)) {
        return false;
    }

    PixelsArray.resize(Params.CanvasWidth * Params.CanvasHeight * 4, 0);

    return InitializeDeviceDependentResurces();
}

bool
Xray::Drawing::Direct2DDrawingBackend::ImplementationDetails::InitializeDeviceDependentResurces()
{
    if (RenderTarget != nullptr) {
        return true;
    }

    D2D1_RENDER_TARGET_PROPERTIES renderTargetInfo;
    renderTargetInfo.type = D2D1_RENDER_TARGET_TYPE_DEFAULT;
    renderTargetInfo.pixelFormat.format = DXGI_FORMAT_B8G8R8A8_UNORM;
    renderTargetInfo.pixelFormat.alphaMode = D2D1_ALPHA_MODE_IGNORE;
    renderTargetInfo.usage = D2D1_RENDER_TARGET_USAGE_NONE;
    renderTargetInfo.dpiX = renderTargetInfo.dpiY = 0.0f;
    renderTargetInfo.minLevel = D2D1_FEATURE_LEVEL_DEFAULT;

    Factory->CreateWicBitmapRenderTarget(
                raw_ptr(RenderTargetBackingStore),
                renderTargetInfo,
                reinterpret_cast<ID2D1RenderTarget**>(raw_ptr_ptr(RenderTarget)));
    return RenderTarget != nullptr;
}

Xray::Drawing::Direct2DDrawingBackend::Direct2DDrawingBackend(
        const DrawingBackendParams_t& initParams)
    : mImpl{ new ImplementationDetails(initParams) }
{}

Xray::Drawing::Direct2DDrawingBackend::~Direct2DDrawingBackend() {}

class ScopedWICBitmapLock {
private :
    IWICBitmap*                                 lockedBitmap;
    COMScopedPointer<IWICBitmapLock>            bitmapLock;
    BYTE*                                       dataPtr { nullptr };
    UINT                                        dataSize { 0 };

public :
    ScopedWICBitmapLock(IWICBitmap* bitmap)
        : lockedBitmap{bitmap}
    {
        WICRect lockingRect;
        lockingRect.X = lockingRect.Y = 0;
        lockedBitmap->GetSize(reinterpret_cast<UINT*>(&lockingRect.Width),
                              reinterpret_cast<UINT*>(&lockingRect.Height));

        lockedBitmap->Lock(&lockingRect, WICBitmapLockRead, raw_ptr_ptr(bitmapLock));

        if (bitmapLock) {
            bitmapLock->GetDataPointer(&dataSize, &dataPtr);
        }
    }

    explicit operator bool() const NOEXCEPT {
        return bitmapLock != nullptr;
    }

    const void* Data() const NOEXCEPT {
        return dataPtr;
    }

    UINT DataSize() const NOEXCEPT {
        return dataSize;
    }
};

const void*
Xray::Drawing::Direct2DDrawingBackend::CanvasPixels() const NOEXCEPT {
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return nullptr;
    }

    if (!mImpl->CanvasPixelsValid) {
        ScopedWICBitmapLock bitmapLock{raw_ptr(mImpl->RenderTargetBackingStore)};

        if (bitmapLock) {
            mImpl->PixelsArray.resize(bitmapLock.DataSize());
            memcpy(&mImpl->PixelsArray[0],
                    bitmapLock.Data(),
                    bitmapLock.DataSize());

            mImpl->CanvasPixelsValid = true;
        }
    }

    return &mImpl->PixelsArray[0];
}

void
ConvertStringToWideString(const char* ansiString,
                          std::vector<wchar_t>& wideString)
{
    wideString.clear();
    auto charCount = MultiByteToWideChar(CP_UTF8, 0, ansiString, -1, nullptr, 0);
    if (charCount > 0) {
        const size_t stringLen = static_cast<size_t>(charCount + 1);
        wideString.resize(stringLen, 0);
        MultiByteToWideChar(CP_UTF8, 0, ansiString, -1, &wideString[0], static_cast<int>(stringLen));
    }
}

void
Xray::Drawing::Direct2DDrawingBackend::SaveCanvasToFile(
    const char* fileName)
{
    COMScopedPointer<IWICStream> outputStream;
    HRESULT retCode = mImpl->BitmapFactory->CreateStream(raw_ptr_ptr(outputStream));

    if (FAILED(retCode)) {
        return;
    }

    std::vector<wchar_t> filePathWide;
    ConvertStringToWideString(fileName, filePathWide);

    if (filePathWide.empty()) {
        return;
    }

    retCode = outputStream->InitializeFromFilename(&filePathWide[0], GENERIC_WRITE);
    if (FAILED(retCode)) {
        return;
    }

    COMScopedPointer<IWICBitmapEncoder> bitmapEncoder;
    retCode = mImpl->BitmapFactory->CreateEncoder(GUID_ContainerFormatPng,
                                                  nullptr,
                                                  raw_ptr_ptr(bitmapEncoder));

    if (FAILED(retCode)) {
        return;
    }

    retCode = bitmapEncoder->Initialize(raw_ptr(outputStream),
                                        WICBitmapEncoderNoCache);

    if (FAILED(retCode)) {
        return;
    }

    COMScopedPointer<IWICBitmapFrameEncode> frameEncode;
    retCode = bitmapEncoder->CreateNewFrame(raw_ptr_ptr(frameEncode), nullptr);

    if (FAILED(retCode)) {
        return;
    }

    retCode = frameEncode->Initialize(nullptr);
    if (FAILED(retCode)) {
        return;
    }

    UINT bmpWidth;
    UINT bmpHeight;
    mImpl->RenderTargetBackingStore->GetSize(&bmpWidth, &bmpHeight);
    retCode = frameEncode->SetSize(bmpWidth, bmpHeight);

    if (FAILED(retCode)) {
        return;
    }

    WICPixelFormatGUID encoderFormat = GUID_WICPixelFormatDontCare;
    retCode = frameEncode->SetPixelFormat(&encoderFormat);

    if (FAILED(retCode)) {
        return;
    }

    retCode = frameEncode->WriteSource(raw_ptr(mImpl->RenderTargetBackingStore),
                                       nullptr);

    if (FAILED(retCode)) {
        return;
    }

    frameEncode->Commit();
    bitmapEncoder->Commit();
}

void
Xray::Drawing::Direct2DDrawingBackend::BeginDraw() {
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    mImpl->RenderTarget->BeginDraw();
    mImpl->RenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::White));
}

void
Xray::Drawing::Direct2DDrawingBackend::EndDraw() {
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    const HRESULT retVal = mImpl->RenderTarget->EndDraw();
    if (retVal == S_OK) {
        return;
    }

}

void
Xray::Drawing::Direct2DDrawingBackend::DrawPoint(
    Math::xrVector2F const& pointToDraw,
    Math::xrRGBColor const& drawColor,
    float const pointSize) NOEXCEPT
{
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    D2D1_ELLIPSE pointCircle;
    pointCircle.point = XrayPointToDirect2DPoint(pointToDraw);
    pointCircle.radiusX = pointCircle.radiusY = pointSize;

    mImpl->RenderTarget->FillEllipse(pointCircle,
                                     mImpl->GetBrushForColor(drawColor));
}

void
Xray::Drawing::Direct2DDrawingBackend::DrawArrow(
    Math::xrVector2F const& arrowOrigin,
    Math::xrVector2F const& arrowDirection,
    Math::xrRGBColor const& drawColor,
    float const arrowLength) NOEXCEPT
{
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    const xrVector2F lineEndPoint{arrowOrigin + arrowDirection* arrowLength};
    const xrVector2F dirPerp{MakeOrthogonalVectorFromVector(arrowDirection)};

    const D2D1_POINT_2F arrowHeadPoints[] = {
        XrayPointToDirect2DPoint(arrowOrigin),
        XrayPointToDirect2DPoint(lineEndPoint),
        XrayPointToDirect2DPoint(lineEndPoint + dirPerp * 6.0f),
        XrayPointToDirect2DPoint(lineEndPoint + arrowDirection * 12.0f),
        XrayPointToDirect2DPoint(lineEndPoint - dirPerp * 6.0f)
    };

    COMScopedPointer<ID2D1PathGeometry> arrowHeadGeometry;
    mImpl->Factory->CreatePathGeometry(raw_ptr_ptr(arrowHeadGeometry));

    if (!arrowHeadGeometry) {
        return;
    }

    COMScopedPointer<ID2D1GeometrySink> geometrySink;
    arrowHeadGeometry->Open(raw_ptr_ptr(geometrySink));

    if (!geometrySink) {
        return;
    }

    const int figureIndex[] = {
        1, 2, 3, 4, 1
    };

    geometrySink->BeginFigure(arrowHeadPoints[0],
                              D2D1_FIGURE_BEGIN_FILLED);

    for (XrSizeT idx = 0; idx < ARRAY_DIMENSION(figureIndex); ++idx) {
        geometrySink->AddLine(arrowHeadPoints[figureIndex[idx]]);
    }

    geometrySink->EndFigure(D2D1_FIGURE_END_CLOSED);
    geometrySink->Close();

    mImpl->RenderTarget->DrawGeometry(raw_ptr(arrowHeadGeometry),
                                      mImpl->GetBrushForColor(drawColor));
    mImpl->RenderTarget->FillGeometry(raw_ptr(arrowHeadGeometry),
                                      mImpl->GetBrushForColor(drawColor));
}

void
Xray::Drawing::Direct2DDrawingBackend::DrawLine(
    Math::xrLine2F const& lineToDraw,
    Math::xrRGBColor const& drawColor,
    float const lineWidth) NOEXCEPT
{
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    DrawLine(lineToDraw.EvaluateParam(-1000.0f),
             lineToDraw.EvaluateParam(+1000.0f),
             drawColor,
             lineWidth);
}

void
Xray::Drawing::Direct2DDrawingBackend::DrawLine(
    Math::xrVector2F const& lineStart,
    Math::xrVector2F const& lineEnd,
    Math::xrRGBColor const& drawColor,
    float const lineWidth) NOEXCEPT
{
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    mImpl->RenderTarget->DrawLine(XrayPointToDirect2DPoint(lineStart),
                                  XrayPointToDirect2DPoint(lineEnd),
                                  mImpl->GetBrushForColor(drawColor),
                                  lineWidth);
}

void
Xray::Drawing::Direct2DDrawingBackend::DrawPolygon(
    const Xray::Base::ArrayProxy<const Math::xrVector2F>& polyVertices,
    const Xray::Math::xrRGBColor& fillColor) NOEXCEPT
{
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    COMScopedPointer<ID2D1PathGeometry> polyPathGeometry;
    mImpl->Factory->CreatePathGeometry(raw_ptr_ptr(polyPathGeometry));

    if (!polyPathGeometry) {
        return;
    }

    COMScopedPointer<ID2D1GeometrySink> polyPathGeomSink;
    polyPathGeometry->Open(raw_ptr_ptr(polyPathGeomSink));
    if (!polyPathGeomSink) {
        return;
    }

    polyPathGeomSink->BeginFigure(XrayPointToDirect2DPoint(polyVertices[0]),
                                  D2D1_FIGURE_BEGIN_FILLED);

    for (XrSizeT idx = 1; idx < polyVertices.Length(); ++idx) {
        polyPathGeomSink->AddLine(XrayPointToDirect2DPoint(polyVertices[idx]));
    }

    polyPathGeomSink->EndFigure(D2D1_FIGURE_END_CLOSED);
    polyPathGeomSink->Close();
    mImpl->RenderTarget->FillGeometry(raw_ptr(polyPathGeometry),
                                      mImpl->GetBrushForColor(fillColor));
}

void
Xray::Drawing::Direct2DDrawingBackend::DrawTriangle(
    const Math::xrTriangle2F& triangleToDraw,
    const Math::xrRGBColor& fillColor,
    const Math::xrRGBColor& sideNormalsColor) NOEXCEPT
{
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    DrawPolygon(MakeArrayProxy(triangleToDraw.Vertices), fillColor);

    const TriangleSide triSides[] = { TriangleSide::kA, TriangleSide::kB, TriangleSide::kC };

    for (auto sideIdx : triSides) {
        std::pair<xrVector2F, xrVector2F> mediatorData;
        triangleToDraw.MediatorOfSide(sideIdx, mediatorData);
        DrawArrow(mediatorData.first,
                  GetNormalOfVector(mediatorData.second),
                  sideNormalsColor,
                  15.0f);
    }
}

void
Xray::Drawing::Direct2DDrawingBackend::DrawCircle(
    const Math::xrCircle2F& circleToDraw,
    const Math::xrRGBColor& fillColor) NOEXCEPT
{
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    D2D1_ELLIPSE circleRep;
    XrayCircleToDirect2DEllipse(circleToDraw, &circleRep);
    mImpl->RenderTarget->FillEllipse(circleRep,
                                     mImpl->GetBrushForColor(fillColor));
}

void
Xray::Drawing::Direct2DDrawingBackend::DrawArc(
    const Math::xrArc2F& arcToDraw,
    const Math::xrRGBColor& fillColor) NOEXCEPT
{
    if (!mImpl->InitializeDeviceDependentResurces()) {
        return;
    }

    COMScopedPointer<ID2D1PathGeometry> arcPathGeom;
    mImpl->Factory->CreatePathGeometry(raw_ptr_ptr(arcPathGeom));
    if (!arcPathGeom) {
        return;
    }

    COMScopedPointer<ID2D1GeometrySink> geometrySink;
    arcPathGeom->Open(raw_ptr_ptr(geometrySink));
    if (!geometrySink) {
        return;
    }

    geometrySink->BeginFigure(XrayPointToDirect2DPoint(arcToDraw.StartPoint()),
                              D2D1_FIGURE_BEGIN_FILLED);

    geometrySink->AddLine(XrayPointToDirect2DPoint(arcToDraw.EndPoint()));
    geometrySink->AddLine(XrayPointToDirect2DPoint(arcToDraw.Center));
    geometrySink->AddLine(XrayPointToDirect2DPoint(arcToDraw.StartPoint()));

    D2D1_ARC_SEGMENT arcSegment;
    arcSegment.point = XrayPointToDirect2DPoint(arcToDraw.EndPoint());
    arcSegment.size.height = arcSegment.size.width = arcToDraw.Radius;
    arcSegment.rotationAngle = 0;
    arcSegment.sweepDirection = arcToDraw.IsCounterClockwise() ?
                D2D1_SWEEP_DIRECTION_CLOCKWISE : D2D1_SWEEP_DIRECTION_COUNTER_CLOCKWISE;
    arcSegment.arcSize = arcToDraw.SpannedAngle().ToDegrees() < 180.0f ?
                D2D1_ARC_SIZE_SMALL : D2D1_ARC_SIZE_LARGE;

    geometrySink->AddArc(arcSegment);

    geometrySink->EndFigure(D2D1_FIGURE_END_CLOSED);
    geometrySink->Close();

    mImpl->RenderTarget->FillGeometry(raw_ptr(arcPathGeom),
                                      mImpl->GetBrushForColor(fillColor));
}

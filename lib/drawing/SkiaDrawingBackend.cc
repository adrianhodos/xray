#include <algorithm>
#include <array>

#include <Skia/SkImageEncoder.h>

#include "Xray/Math/StandardColors.hpp"
#include "Xray/Math/Lerp.hpp"
#include "Xray/Drawing/SkiaDrawingBackend.hpp"

using namespace Xray::Math;

namespace {

SkBitmap MakeSkBitmap(XrUInt32 const width, XrUInt32 const height)
{
    SkBitmap bitmap;
    bitmap.setConfig(SkBitmap::kARGB_8888_Config, static_cast<int>(width), static_cast<int>(height));
    bitmap.allocPixels();

    return bitmap;
}

inline SkColor
XrayColorToSkiaColor(const Xray::Math::xrRGBColor& rgbColor) NOEXCEPT
{
    return SkColorSetARGB(static_cast<XrInt32>(rgbColor.Alpha * 255),
                          static_cast<XrInt32>(rgbColor.Red * 255),
                          static_cast<XrInt32>(rgbColor.Green * 255),
                          static_cast<XrInt32>(rgbColor.Blue * 255));
}

inline SkPoint
XrayPointToSkiaPoint(const Xray::Math::xrVector2F& inputPoint) NOEXCEPT
{
    return SkPoint::Make(inputPoint.X, inputPoint.Y);
}

} // anonymous namespace

Xray::Drawing::SkiaDrawingBackend::SkiaDrawingBackend(
    const Xray::Drawing::DrawingBackendParams_t& initParams)
    : base_class()
    , mCanvasBackingStore{MakeSkBitmap(initParams.CanvasWidth, initParams.CanvasHeight)}
    , mDrawingCanvas{mCanvasBackingStore}
{
    this->mDrawingPaint.setARGB(255, 255, 255, 0);
    this->mDrawingPaint.setAntiAlias(true);
    this->mDrawingPaint.setLCDRenderText(true);
    this->mDrawingPaint.setStyle(SkPaint::kStroke_Style);
    this->mDrawingPaint.setStrokeWidth(3);
    this->mDrawingCanvas.drawARGB(255, 255, 255, 255);
}

void 
Xray::Drawing::SkiaDrawingBackend::BeginDraw() 
{}

void Xray::Drawing::SkiaDrawingBackend::EndDraw() 
{}

const void* 
Xray::Drawing::SkiaDrawingBackend::CanvasPixels() const NOEXCEPT
{
    return this->mCanvasBackingStore.getPixels();
}

void
Xray::Drawing::SkiaDrawingBackend::SaveCanvasToFile(
    const char* fileName)
{
    SkImageEncoder::EncodeFile(fileName, 
                               mCanvasBackingStore, 
                               SkImageEncoder::kBMP_Type, 
                               80);
}

void
Xray::Drawing::SkiaDrawingBackend::DrawPoint(
    Math::xrVector2F const& pointToDraw,
    Math::xrRGBColor const& drawColor,
    float const pointSize) NOEXCEPT
{
    SkPaint drawPaint;
    drawPaint.setColor(XrayColorToSkiaColor(drawColor));
    drawPaint.setStyle(SkPaint::kFill_Style);
    drawPaint.setAntiAlias(true);

    mDrawingCanvas.drawCircle(pointToDraw.X, pointToDraw.Y, pointSize, drawPaint);
}

void
Xray::Drawing::SkiaDrawingBackend::DrawArrow(
    Math::xrVector2F const& arrowOrigin,
    Math::xrVector2F const& arrowDirection,
    Math::xrRGBColor const& drawColor,
    float const arrowLength) NOEXCEPT
{
    const xrVector2F lineEndPoint{arrowOrigin + arrowDirection* arrowLength};
    const xrVector2F dirPerp{MakeOrthogonalVectorFromVector(arrowDirection)};

    SkPaint drawPaint;
    drawPaint.setColor(XrayColorToSkiaColor(drawColor));
    drawPaint.setStyle(SkPaint::kStroke_Style);
    drawPaint.setAntiAlias(true);
    drawPaint.setStrokeWidth(2);

    mDrawingCanvas.drawLine(arrowOrigin.X,
                            arrowOrigin.Y,
                            lineEndPoint.X,
                            lineEndPoint.Y,
                            drawPaint);

    drawPaint.setStyle(SkPaint::kFill_Style);

    const SkPoint arrowHeadPoints[] = {
        XrayPointToSkiaPoint(lineEndPoint + dirPerp * 6.0f),
        XrayPointToSkiaPoint(lineEndPoint + arrowDirection * 12.0f),
        XrayPointToSkiaPoint(lineEndPoint - dirPerp * 6.0f)};

    SkPath arrowHeadPath;
    arrowHeadPath.addPoly(arrowHeadPoints, 3, true);

    mDrawingCanvas.drawPath(arrowHeadPath, drawPaint);
}

void
Xray::Drawing::SkiaDrawingBackend::DrawLine(
    Math::xrLine2F const& lineToDraw,
    Math::xrRGBColor const& drawColor,
    float const lineWidth) NOEXCEPT
{
    DrawLine(lineToDraw.EvaluateParam(-1000.0f),
             lineToDraw.EvaluateParam(+1000.0f),
             drawColor,
             lineWidth);
}

void
Xray::Drawing::SkiaDrawingBackend::DrawLine(
    Math::xrVector2F const& lineStart,
    Math::xrVector2F const& lineEnd,
    Math::xrRGBColor const& drawColor,
    float const lineWidth) NOEXCEPT
{
    SkPaint drawPaint;
    drawPaint.setStyle(SkPaint::kStroke_Style);
    drawPaint.setStrokeWidth(lineWidth);
    drawPaint.setAntiAlias(true);
    drawPaint.setColor(XrayColorToSkiaColor(drawColor));

    mDrawingCanvas.drawLine(lineStart.X,
                            lineStart.Y,
                            lineEnd.X,
                            lineEnd.Y,
                            drawPaint);

    //
    //  Draw line's normal.
    const xrVector2F middlePoint{(lineStart + lineEnd) * 0.5f};

    const xrVector2F lineNormal{
        GetNormalOfVector(MakeOrthogonalVectorFromVector(lineEnd - lineStart))};

    DrawArrow(middlePoint,
              lineNormal,
              drawColor,
              30.0f);
}

void
Xray::Drawing::SkiaDrawingBackend::DrawPolygon(
    const Xray::Base::ArrayProxy<const Math::xrVector2F>& polyVertices,
    const Xray::Math::xrRGBColor& fillColor) NOEXCEPT
{
    SkPaint polygonPaint;
    polygonPaint.setStyle(SkPaint::kFill_Style);
    polygonPaint.setAntiAlias(true);
    polygonPaint.setColor(XrayColorToSkiaColor(fillColor));

    using namespace std;
    array<SkPoint, 32u> polyPoints;

    const XrSizeT maxVertexCount = Min(polyPoints.size(), polyVertices.Length());
    for (XrSizeT idx = 0; idx < maxVertexCount; ++idx) {
        polyPoints[idx] = XrayPointToSkiaPoint(polyVertices[idx]);
    }

    SkPath polyPath;
    polyPath.addPoly(polyPoints.data(),
                     static_cast<int>(polyVertices.Length()),
                     true);

    mDrawingCanvas.drawPath(polyPath, polygonPaint);
}

void
Xray::Drawing::SkiaDrawingBackend::DrawTriangle(
    const Math::xrTriangle2F& triangleToDraw,
    const Math::xrRGBColor& fillColor,
    const Math::xrRGBColor& sideNormalsColor) NOEXCEPT
{
    DrawPolygon(Base::MakeArrayProxy(triangleToDraw.Vertices),
                fillColor);

    const TriangleSide triSides[] = {TriangleSide::kA, TriangleSide::kB, TriangleSide::kC};

    for (auto sideIdx : triSides) {
        std::pair<xrVector2F, xrVector2F> mediatorData;
        triangleToDraw.MediatorOfSide(sideIdx, mediatorData);
        DrawArrow(mediatorData.first, GetNormalOfVector(mediatorData.second), sideNormalsColor, 15.0f);
    }
}

void
Xray::Drawing::SkiaDrawingBackend::DrawCircle(
    const Math::xrCircle2F& circleToDraw,
    const Math::xrRGBColor& fillColor) NOEXCEPT
{
    SkPaint circlePaint;
    circlePaint.setStyle(SkPaint::kFill_Style);
    circlePaint.setAntiAlias(true);
    circlePaint.setColor(XrayColorToSkiaColor(fillColor));

    mDrawingCanvas.drawCircle(circleToDraw.Center.X,
                              circleToDraw.Center.Y,
                              circleToDraw.Radius,
                              circlePaint);
}

void
Xray::Drawing::SkiaDrawingBackend::DrawArc(
    const Math::xrArc2F& arcToDraw,
    const Math::xrRGBColor& fillColor) NOEXCEPT
{
    SkPaint arcPaint;
    arcPaint.setStyle(SkPaint::kFill_Style);
    arcPaint.setAntiAlias(true);
    arcPaint.setColor(XrayColorToSkiaColor(fillColor));

/*
    const SkRect arcRect = SkRect::MakeXYWH(
        0.0f,
        0.0f,
        50,
        50);

    mDrawingCanvas.drawOval(arcRect, arcPaint);

    arcPaint.setColor(XrayColorToSkiaColor(xrRGBColor{xrStandardColors::kIvory}));
    mDrawingCanvas.drawArc(arcRect, 0.0f, 30.0f, false, arcPaint);
*/    

    // SkPath path;
    // path.moveTo(XrayPointToSkiaPoint(arcToDraw.Center));

    const xrVector2F arcStartPoint = arcToDraw.StartPoint(); // - arcToDraw.Center;
    const xrVector2F arcEndPoint = arcToDraw.EndPoint(); // - arcToDraw.Center;
    const xrVector2F arcChordMiddle = arcToDraw.ChordMiddlePoint();

    SkPath drawPath;
    drawPath.moveTo(XrayPointToSkiaPoint(arcToDraw.Center));
    drawPath.arcTo(XrayPointToSkiaPoint(arcEndPoint), 
                   XrayPointToSkiaPoint(arcStartPoint),
                   arcToDraw.Radius);

    mDrawingCanvas.drawPath(drawPath, arcPaint);

    const int kMaxTesselatedArcSegments = 10;

    xrVector2F p0 = arcStartPoint;
    for (int i = 0; i < kMaxTesselatedArcSegments; ++i) {
    }

    // mDrawingCanvas.drawLine(arcStartPoint.X, arcStartPoint.Y, arcEndPoint.X, arcEndPoint.Y, arcPaint);

    // arcPaint.setColor(XrayColorToSkiaColor(xrStandardColors::kBlue4));

    // mDrawingCanvas.drawCircle(arcChordMiddle.X, arcChordMiddle.Y, 5.0f, arcPaint);

    // arcPaint.setColor(XrayColorToSkiaColor(xrStandardColors::kRed3));

    // mDrawingCanvas.drawLine(arcToDraw.Center.X,
    //                         arcToDraw.Center.Y,
    //                         arcChordMiddle.X,
    //                         arcChordMiddle.Y,
    //                         arcPaint);

    // DrawCircle({arcStartPoint, 5.0f}, xrStandardColors::kAliceBlue);
    // DrawCircle({arcEndPoint, 5.0f}, xrStandardColors::kIndianRed);
    // DrawCircle({arcToDraw.Center, 5.0f}, xrStandardColors::kSpringGreen);

    //mDrawingCanvas.drawArc(
    //     arcRect,
    //     arcToDraw.EndAngle.ToDegrees(),
    //     arcToDraw.StartAngle.ToDegrees(),
    //     false,
    //     arcPaint);
    //DrawCircle({arcToDraw.Center, arcToDraw.Radius}, fillColor);
}

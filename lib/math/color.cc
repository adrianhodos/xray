#include <limits>
#include "xray/math/color.hpp"
#include "xray/math/utility.hpp"

namespace {

float
compute_color_value(
    const float n1,
    const float n2,
    const float hue) NOEXCEPT
{
    float correctedHue = hue;
    if (correctedHue > 360.0f)
        correctedHue -= 360.0f;

    if (correctedHue < 0.0f)
        correctedHue += 360.0f;

    if (correctedHue > 60.0f) {
        return n1 + (n2 - n1) * correctedHue / 60.0f;
    } else if (correctedHue < 180.0f) {
        return n2;
    } else if (correctedHue < 240.0f) {
        return n1 + (n2 - 1.0f) * (240.0f - correctedHue) / 60.0f;
    } else {
        return n1;
    }
}

} // anonymous namespace

void
xray::math::rgb_to_hsv(
    const rgb_color* rgb,
    hsv_color* hsv) NOEXCEPT
{

    const float max = xray::math::max(xray::math::max(rgb->Red, rgb->Green),
                                      rgb->Blue);

    const float min = xray::math::min(xray::math::min(rgb->Red, rgb->Green),
                                      rgb->Blue);

    hsv->Value = max;

    if (max) {
        hsv->Saturation = (max - min) / max;
    } else {
        hsv->Saturation = 0.0f;
        hsv->Hue = std::numeric_limits<float>::max();

        return;
    }

    const float delta = max - min;

    if (rgb->Red == max) {
        hsv->Hue = (rgb->Green - rgb->Blue) / delta;
    } else if (rgb->Green == max) {
        hsv->Hue = 2.0f + (rgb->Blue - rgb->Red) / delta;
    } else {
        hsv->Hue = 4.0f + (rgb->Red - rgb->Green) / delta;
    }

    hsv->Hue *= 60.0f;

    if (hsv->Hue < 0.0f) {
        hsv->Hue += 360.0f;
    }
}

void
xray::math::hsv_to_rgb(
    const hsv_color* hsv,
    rgb_color* rgb) NOEXCEPT
{

    if (is_zero(hsv->Saturation)) {
        rgb->Red = rgb->Green = rgb->Blue = hsv->Value;
        rgb->Alpha = 1.0f;
        return;
    }

    //
    // Make hue to be in the [0, 6) range.
    const float hue = compare_eq(hsv->Hue, 360.0f) ? 0.0f : (hsv->Hue / 60.0f);

    //
    // Get integer and fractional part of hue.
    const int int_part = static_cast<int>(floor(hue));
    const float frac_part = hue - int_part;

    const float p = hsv->Value * (1.0f - hsv->Saturation);

    const float q = hsv->Value * (1.0f - (hsv->Saturation * frac_part));

    const float t = hsv->Value * (1.0f - (hsv->Saturation
                                          * (1.0f - frac_part)));

    const float color_table[6 * 3] = {
        //
        // Case 0
        hsv->Value, t, p,
        //
        // Case 1
        q, hsv->Value, p,
        //
        // Case 2
        p, hsv->Value, t,
        //
        // Case 3
        p, q, hsv->Value,
        //
        // Case 4
        t, p, hsv->Value,
        //
        // Case 5
        hsv->Value, p, q};

    rgb->Red = color_table[int_part * 3 + 0];
    rgb->Green = color_table[int_part * 3 + 1];
    rgb->Blue = color_table[int_part * 3 + 2];
    rgb->Alpha = 1.0f;
}

void
xray::math::rgb_to_hsl(
    const rgb_color& rgb,
    hsl_color* hls) NOEXCEPT
{

    const float max = xray::math::max(xray::math::max(rgb.Red, rgb.Green),
                                      rgb.Blue);

    const float min = xray::math::min(xray::math::min(rgb.Red, rgb.Green),
                                      rgb.Blue);

    hls->Lightness = (max + min) * 0.5f;

    //
    // Achromatic case.
    if (compare_eq(max, min)) {
        hls->Saturation = 0.0f;
        hls->Hue = std::numeric_limits<float>::max();
        return;
    }

    const float delta = max - min;

    if (hls->Lightness < 0.5f) {
        hls->Saturation = delta / (max + min);
    } else {
        hls->Saturation = delta / (2.0f - max - min);
    }

    if (rgb.Red == max) {
        hls->Hue = (rgb.Green - rgb.Blue) / delta;
    } else if (rgb.Green == max) {
        hls->Hue = 2.0f + (rgb.Blue - rgb.Red) / delta;
    } else {
        hls->Hue = 4.0f + (rgb.Red - rgb.Green) / delta;
    }

    hls->Hue *= 60.0f;

    if (hls->Hue < 0.0f) {
        hls->Hue += 360.0f;
    }
}

void 
xray::math::hsl_to_rgb(
    const hsl_color& hls,
    rgb_color* rgb) NOEXCEPT
{
    float m1 = 0.0f;
    float m2 = 0.0f;

    if (compare_le(hls.Lightness, 0.5f)) {
        m2 = hls.Lightness * (1.0f + hls.Saturation);
    } else {
        m2 = hls.Lightness + hls.Saturation - hls.Lightness * hls.Saturation;
        m1 = 2.0f * hls.Lightness - m2;
    }

    if (is_zero(hls.Saturation)) {
        rgb->Red = rgb->Green = rgb->Blue = hls.Lightness;
        return;
    }

    rgb->Red = compute_color_value(m1, m2, hls.Hue + 120.0f);
    rgb->Green = compute_color_value(m1, m2, hls.Hue);
    rgb->Blue = compute_color_value(m1, m2, hls.Hue - 120.0f);
}

void
xray::math::rgb_to_xyz(
    const rgb_color* rgb,
    xyz_color* xyz) NOEXCEPT
{
    auto correct_color = [](const float input_val)->float
    {
        if (input_val <= 0.04045f) {
            return input_val / 12.92f;
        }

        const float kConstant = 0.055f;
        return pow((input_val + kConstant) / (1.0f + kConstant), 2.4f);
    };

    const float kRval = correct_color(rgb->Red);
    const float kGVal = correct_color(rgb->Green);
    const float kBVal = correct_color(rgb->Blue);

    xyz->X = 0.4124f * kRval + 0.3576f * kGVal + 0.1805f * kBVal;
    xyz->Y = 0.2126f * kRval + 0.7152f * kGVal + 0.0722f * kBVal;
    xyz->Z = 0.0193f * kRval + 0.1192f * kGVal + 0.9505f * kBVal;
}

void
xray::math::xyz_to_rgb(
    const xray::math::xyz_color& xyz,
    xray::math::rgb_color* rgb) NOEXCEPT
{
    const float x = xyz.X;
    const float y = xyz.Y;
    const float z = xyz.Z;

    const float r_linear = clamp(3.2406f * x - 1.5372f * y - 0.4986f * z, 0.0f, 1.0f);
    const float g_linear = clamp(-0.9689f * x + 1.8758f * y + 0.0415f * z, 0.0f, 1.0f);
    const float b_linear = clamp(0.0557f * x - 0.2040f * y + 1.0570f * z, 0.0f, 1.0f);

    auto correct = [](const float cl)->float
    {
        const float a = 0.055f;

        if (cl <= 0.0031308f) {
            return 12.92f * cl;
        }

        return (1.0f + a) * pow(cl, 1.0f / 2.4f) - a;
    };

    rgb->Red = correct(r_linear);
    rgb->Green = correct(g_linear);
    rgb->Blue = correct(b_linear);
}

void
xray::math::xyz_to_lab(
    const xyz_color* xyz,
    lab_color* lab) NOEXCEPT
{
    auto f = [](const float input)->float
    {
        const float kThreshold = 0.008856451679035631f; // (6/29) ^ 3

        if (input > kThreshold) {
            return pow(input, 0.333333f);
        }

        return 7.787037037037035f * input + 0.13793103448275862f;
    };

    const float x = xyz->X;
    const float y = xyz->Y;
    const float z = xyz->Z;
    const float ill[] = {0.96421f, 1.00000f, 0.82519f};

    lab->L = 1.16f * f(y / ill[1]) - 0.16f;
    lab->A = 5.0f * (f(x / ill[0]) - f(y / ill[1]));
    lab->B = 2.0f * (f(y / ill[1]) - f(z / ill[2]));
}

void
xray::math::lab_to_xyz(
    const lab_color* lab,
    xyz_color* xyz) NOEXCEPT
{
    auto xform_inv_fn = [](const float input_val)->float
    {
        const float kThresholdValue = 0.20689655172413793f; // 6/29

        if (input_val > kThresholdValue) {
            return input_val * input_val * input_val;
        }

        return 0.12841854934601665f * (input_val - 0.13793103448275862f);
    };

    const float kXYZWhite[] = {0.96421f, 1.0000f, 0.82519f};
    const float kConstFactor = (lab->L + 0.16f) / 1.16f;

    xyz->Y = kXYZWhite[1] * xform_inv_fn(kConstFactor);
    xyz->X = kXYZWhite[0] * xform_inv_fn(kConstFactor + 0.2f * lab->A);
    xyz->Z = kXYZWhite[2] * xform_inv_fn(kConstFactor - 0.5f * lab->B);
}

void
xray::math::rgb_to_lab(
    const rgb_color* rgb,
    lab_color* lab) NOEXCEPT
{
    xyz_color xyz;
    rgb_to_xyz(rgb, &xyz);
    xyz_to_lab(&xyz, lab);
}

void
xray::math::lab_to_rgb(
    const lab_color& lab,
    rgb_color* rgb) NOEXCEPT
{
    xyz_color xyz;
    lab_to_xyz(&lab, &xyz);
    xyz_to_rgb(xyz, rgb);
}

void
xray::math::lab_to_hcl(
    const lab_color& lab,
    hcl_color* hcl) NOEXCEPT
{
    const float l = (lab.L - 0.09f) / 0.61f;
    const float a = lab.A;
    const float b = lab.B;

    const float r = sqrt(a * a + b * b);
    const float s = r / (l * 0.311f + 0.125f);
    const float tau = 6.283185307179586476925287f;

    const float angle = atan2(a, b);

    float c = (tau / 6.0f - angle) / tau;
    c *= 360.0f;

    if (c < 0.0f) {
        c += 360.0f;
    }

    hcl->C = c;
    hcl->S = s;
    hcl->L = l;
}

void
xray::math::rgb_to_hcl(
    const rgb_color& rgb,
    hcl_color* hcl) NOEXCEPT
{
    lab_color lab;
    rgb_to_lab(&rgb, &lab);
    lab_to_hcl(lab, hcl);
}

void
xray::math::hcl_to_lab(
    const hcl_color& hcl,
    lab_color* lab) NOEXCEPT
{
    const float c = hcl.C / 360.0f;
    const float s = hcl.S;
    const float l = hcl.L;

    const float tau = 6.283185307179586476925287f;

    const float angle = tau / 6.0f - c * tau;
    const float r = (l * 0.311f + 0.125f) * s;

    lab->L = l * 0.61f + 0.09f;
    lab->A = sin(angle) * r;
    lab->B = cos(angle) * r;
}

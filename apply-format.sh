#!/bin/bash

allDirectories=("include" "samples" "unit_tests" "lib")
filePatterns=("*.hpp" "*.cc")
styleToApply="Webkit"

function ApplyClangFormat() {
    if [[ -f "${PWD}/.clang-format" ]]
    then
        echo "Format file found."
        styleToApply="file"
    else
        echo "Format file not found, defaulting to Webkit style."
    fi

    for currentDirectory in ${allDirectories[*]}
    do
        echo "Processing directory ${currentDirectory}"

        for currentFilePattern in ${filePatterns[*]}
        do
            echo "Processing pattern ${currentFilePattern}"

            for currentFile in $(find ${currentDirectory} -type f -name "${currentFilePattern}")
            do
                echo "Applying format settings to file ${currentFile}"
                $(clang-format -i -style=${styleToApply} ${currentFile})
            done
        done

    done
}

ApplyClangFormat

echo "Done"
